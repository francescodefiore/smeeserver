package it.smee.webservices;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

import it.smee.acotel.dao.JdbcAcotelDAO;
import it.smee.jboxlib.JBoxDAO;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class AcotelController {
	
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO acotelDAO = (JBoxDAO)context.getBean("acotelDAO");
	
	public AcotelController()
	{
		System.out.println("==== /AcotelController ====");
	}
	
	@RequestMapping(value="/acotel/upload", method=RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    public void handleFileUpload(@RequestParam("file") MultipartFile file){
		System.out.println("==== /acotel/upload new request ====");
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Calendar c = Calendar.getInstance();
                String directory = "./imported/acotel";
                boolean success=(new File(directory)).mkdirs();		           
                System.out.println("DEBUG:: directory creata: " + success);
                
            	String name = directory + "/"+ c.getTimeInMillis()+"_acotel.csv";            
            	BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name)));
            	stream.write(bytes);
            	stream.close();
            	((JdbcAcotelDAO)acotelDAO).FileImport(name);
	            
                
            } catch (Exception e) { }            
        }
	}

}
