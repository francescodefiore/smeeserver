package it.smee.daticampo.models;

public class MisureCampo {
	private String idMeasure;
	private String idOsservazione;
	private String valore;
	private String giorno;
	private String system_id;
	private String peso_unita;
	
	
	public String getIdMeasure() {
		return idMeasure;
	}
	public void setIdMeasure(String idMeasure) {
		this.idMeasure = idMeasure;
	}
	public String getIdOsservazione() {
		return idOsservazione;
	}
	public void setIdOsservazione(String idOsservazione) {
		this.idOsservazione = idOsservazione;
	}
	public String getValore() {
		return valore;
	}
	public void setValore(String valore) {
		this.valore = valore;
	}
	public String getGiorno() {
		return giorno;
	}
	public void setGiorno(String giorno) {
		this.giorno = giorno;
	}
	public String getSystem_id() {
		return system_id;
	}
	public void setSystem_id(String system_id) {
		this.system_id = system_id;
	}
	public String getPeso_unita() {
		return peso_unita;
	}
	public void setPeso_unita(String peso_unita) {
		this.peso_unita = peso_unita;
	}
	
	
}
