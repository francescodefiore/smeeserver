package it.smee.webservices;

import it.smee.jboxlib.JWeather;
import it.smee.weather.dao.JdbcWeatherDAO;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class WeatherController {
    private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
    @SuppressWarnings("unchecked")
    private static JWeather weatherDAO = (JWeather) context.getBean("weatherDAO");

    public WeatherController() {
        System.out.println("==== /WeatherController ====");
    }


    //WebService weather
    @RequestMapping(value = "/weather", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> weather(
            @RequestParam(value="city", required=true) String city,
            @RequestParam(value="dateFrom", required=true) String dateFrom,
            @RequestParam(value="dateTo", required=true) String dateTo)
    {

        return weatherDAO.getData(city, dateFrom, dateTo);
    }
}
