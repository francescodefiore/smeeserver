package it.smee.epi.models;

public class Supesterneopache {
	private String descrizione;
	private String tiposuperficie;
	private String materiale;
	private String esposizione;
	private String superficieseo;
	private String spessore;
	private String trasmittanzaeop;
	private String ambienteconfinante;
	private String fattorecorrezione;
	private String massa;
	private String calorespecifico;
	
	public String getDescrizione() {
		return descrizione;
	}
	
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	public String getTiposuperficie() {
		return tiposuperficie;
	}
	
	public void setTiposuperficie(String tiposuperficie) {
		this.tiposuperficie = tiposuperficie;
	}
	
	public String getMateriale() {
		return materiale;
	}
	
	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}
	
	public String getEsposizione() {
		return esposizione;
	}
	
	public void setEsposizione(String esposizione) {
		this.esposizione = esposizione;
	}
	
	public String getSuperficieseo() {
		return superficieseo;
	}
	
	public void setSuperficieseo(String superficieseo) {
		this.superficieseo = superficieseo;
	}
	
	public String getSpessore() {
		return spessore;
	}
	
	public void setSpessore(String spessore) {
		this.spessore = spessore;
	}
	
	public String getTrasmittanzaeop() {
		return trasmittanzaeop;
	}
	
	public void setTrasmittanzaeop(String trasmittanzaeop) {
		this.trasmittanzaeop = trasmittanzaeop;
	}
	
	public String getAmbienteconfinante() {
		return ambienteconfinante;
	}
	
	public void setAmbienteconfinante(String ambienteconfinante) {
		this.ambienteconfinante = ambienteconfinante;
	}
	
	public String getFattorecorrezione() {
		return fattorecorrezione;
	}
	
	public void setFattorecorrezione(String fattorecorrezione) {
		this.fattorecorrezione = fattorecorrezione;
	}
	
	public String getMassa() {
		return massa;
	}
	
	public void setMassa(String massa) {
		this.massa = massa;
	}
	
	public String getCalorespecifico() {
		return calorespecifico;
	}
	
	public void setCalorespecifico(String calorespecifico) {
		this.calorespecifico = calorespecifico;
	}
}
