package it.smee.acotel.dao;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import it.smee.jboxlib.JBoxDAO;

public class JdbcAcotelDAO implements JBoxDAO{
	
	private JdbcTemplate jdbcTemplate;	
	private String filter_string = "";
	private String filter_columns = "*";
	private String aggregation_string = "";
	private String name = "";

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public void FileImport(String fileName) 
	{
		try {		
			
			String sql = createSqlImportFile(fileName);
					
			System.out.println("query " + sql);							
			this.jdbcTemplate.execute(sql);
		}
			
		catch (Exception e) {
			System.out.println(" FileImportAcotel::Query non eseguita");
			System.out.println(e);
			    
		}	
	}
	
	private String createSqlImportFile(String fileName)
	{
		return 
				"LOAD DATA LOCAL INFILE '" + fileName + "' INTO TABLE smee_datimultiorari_acotel " +
				"FIELDS TERMINATED BY ';' " + 
				"LINES TERMINATED BY '\r\n' " +
				"#IGNORE 1 LINES "+
				"(" +
				"data,"+
				"pod,"+
				"codiceImmobile,"+
				"totale,"+
				"01_am,"+
				"02_am,"+
				"03_am,"+
				"04_am,"+
				"05_am,"+
				"06_am,"+
				"07_am,"+
				"08_am,"+
				"09_am,"+
				"10_am,"+
				"11_am,"+
				"12_am,"+
				"01_pm,"+
				"02_pm,"+
				"03_pm,"+
				"04_pm,"+
				"05_pm,"+
				"06_pm,"+
				"07_pm,"+
				"08_pm,"+
				"09_pm,"+
				"10_pm,"+
				"11_pm,"+
				"12_pm "+
				") "+
				";";
	}


	@Override
	public void print() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resetFilters() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Map<String, Object>> getData() {
		// TODO Auto-generated method stub
		return null;
	}

}
