package it.smee.manutenzione.models;

import java.util.List;

public class Scenari {
	
	private int idScenario;	
	private float costoTotale;
	private float epiAttuale;	
	private float epiRisparmio;	
	private List<Simulazioni> interventi;
	
	
	public int getIdScenario() {
		return idScenario;
	}
	public void setIdScenario(int idScenario) {
		this.idScenario = idScenario;
	}
	public float getCostoTotale() {
		return costoTotale;
	}
	public void setCostoTotale(float costoTotale) {
		this.costoTotale = costoTotale;
	}
	public float getEpiAttuale() {
		return epiAttuale;
	}
	public void setEpiAttuale(float epiAttuale) {
		this.epiAttuale = epiAttuale;
	}
	public List<Simulazioni> getInterventi() {
		return interventi;
	}
	public void setInterventi(List<Simulazioni> interventi) {
		this.interventi = interventi;
	}
	public float getEpiRisparmio() {
		return epiRisparmio;
	}
	public void setEpiRisparmio(float epiRisparmio) {
		this.epiRisparmio = epiRisparmio;
	}

}
