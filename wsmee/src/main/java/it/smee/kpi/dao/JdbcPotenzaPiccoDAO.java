package it.smee.kpi.dao;

import it.smee.jboxlib.JBoxDAO;

import javax.sql.DataSource;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;


public class JdbcPotenzaPiccoDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;	
	
	
	private String filter_string = "";
	private String filter_columns;
	private String aggregation_string;
	private String name = "";
	
	
	@Override
	public void print() {
		System.out.println("--- JdbcPotenzaPiccoDAO");		
	}

	private String timeFilter(String[] interval)
	{	
		int i=0;
		String s = " && (data BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'";
		i = i+2;
		while(i<interval.length)
		{
			s = s.concat(" OR data BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'");
			i = i+2;
		}
		s = s.concat(")");
		return s;
	}


	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		switch(filter_name)
		{
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
			
			case "TIMEINTERVAL":	
				filter_string = filter_string.concat( timeFilter(filter_params) );				
				break;
				
			case "PROV":	
				filter_string = filter_string.concat( provFilter(filter_params) );				
				break;
				
			case "CLUSTERENERGY":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );				
				break;
				
		}
		

	}
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = "&& pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		
		return s;
	}
	
	private String provFilter(String[] prov)
	{			
		int i=0;
		String s = "&& provincia = '" + prov[i] + "'"; 
		while(++i<prov.length)
			s = s.concat(" OR provincia = '" + prov[i] + "'");
		
		return s;
	}
	
	private String clusterFilter(String[] cluster)
	{			
		int i=0;
		String s = "&& clusterEnergy = '" + cluster[i] + "'"; 
		while(++i<cluster.length)
			s = s.concat(" OR clusterEnergy = '" + cluster[i] + "'");
		
		return s;
	}

	
	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{
			case "POD":	
				aggregation_string = "GROUP BY pod";				
			break;
			
			case "PROV":	
				aggregation_string = "GROUP BY provincia";				
				break;
			
			case "CLUSTERENERGY":	
				aggregation_string = "GROUP BY clusterEnergy";	
				break;
				
		}
	}


	@Override
	public void resetFilters() {
		//filter_string = "";		
		filter_columns = "pod, comune, provincia, indirizzoPod, clusterEnergy, GREATEST( MAX(01_am), MAX(02_am), MAX(03_am), MAX(04_am), MAX(05_am), MAX(06_am), MAX(07_am), MAX(08_am), MAX(09_am), MAX(10_am), MAX(11_am), MAX(12_pm), MAX(01_pm), MAX(02_pm) , MAX(03_pm), MAX(04_pm), MAX(05_pm), MAX(06_pm), MAX(07_pm), MAX(08_pm), MAX(09_pm), MAX(10_pm), MAX(11_pm), MAX(12_am)) AS potenza_picco";
		aggregation_string = "";
	}

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Map<String, Object>> getData() {
		List<Map<String, Object>> multi = null;
		
		try {										
					String sql = "SELECT " + filter_columns + " " +
						     "FROM datimultiorari " +
						     "WHERE 1=1 "+ filter_string + " "
						     + aggregation_string;
						     

					System.out.println("query " + sql);
					
					multi = this.jdbcTemplate.queryForList(sql);
					
		}
			
		catch (Exception e) {
			    System.out.println(" JdbcPotenzaPiccoDAO::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return multi;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;		
	}

}