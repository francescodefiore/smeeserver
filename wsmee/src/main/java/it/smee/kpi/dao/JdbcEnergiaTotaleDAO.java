package it.smee.kpi.dao;

import it.smee.jboxlib.JBoxDAO;
import it.smee.kpi.models.EnergiaTotaleAnnua;
import it.smee.kpi.models.PiccoPotenzaAssorbita;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/*
public class JdbcEnergiaTotaleDAO implements JBoxDAO<EnergiaTotaleAnnua> {

	private JdbcTemplate jdbcTemplate;	
	private String filter_string = "";
	private String filter_columns;
	private String aggregation_string;
	
	
	@Override
	public void print() {
		System.out.println("--- JdbcEnergiaTotaleDAO");		
	}

	private String timeFilter(String[] interval)
	{	
		int i=0;
		String s = " && (data BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'";
		i = i+2;
		while(i<interval.length)
		{
			s = s.concat(" OR data BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'");
			i = i+2;
		}
		s = s.concat(")");
		return s;
	}


	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		switch(filter_name)
		{
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
			
			case "TIMEINTERVAL":	
				filter_string = filter_string.concat( timeFilter(filter_params) );				
				break;
				
			case "PROV":	
				filter_string = filter_string.concat( provFilter(filter_params) );				
				break;
				
			case "CLUSTERENERGY":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );				
				break;
				
		}
		

	}
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = "&& pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		
		return s;
	}
	
	private String provFilter(String[] prov)
	{			
		int i=0;
		String s = "&& provincia = '" + prov[i] + "'"; 
		while(++i<prov.length)
			s = s.concat(" OR provincia = '" + prov[i] + "'");
		
		return s;
	}
	
	private String clusterFilter(String[] cluster)
	{			
		int i=0;
		String s = "&& clusterEnergy = '" + cluster[i] + "'"; 
		while(++i<cluster.length)
			s = s.concat(" OR clusterEnergy = '" + cluster[i] + "'");
		
		return s;
	}

	
	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{
			case "POD":	
				aggregation_string = "GROUP BY pod";				
			break;
			
			case "PROV":	
				aggregation_string = "GROUP BY provincia";				
				break;
			
			case "CLUSTERENERGY":	
				aggregation_string = "GROUP BY clusterEnergy";	
				break;
				
		}
	}


	@Override
	public void resetFilters() {
		filter_string = "";		
		filter_columns = "podedificio, anno, indirizzo, comune, provincia, clusterEnergy,  (SUM(gen) + SUM(feb) + sum(mar) + sum(apr) + sum(mag) + sum(giu) + sum(lug) + sum(ago) + sum(sett) + sum(ott) + sum(nov) + sum(dic)) as energia_totale_anno ";
		aggregation_string = "";
	}

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<EnergiaTotaleAnnua> getData() {
		List<EnergiaTotaleAnnua> multi = null;
		
		try {										
					String sql = "SELECT " + filter_columns + " " +
						     "FROM consuntivi INNER JOIN immobile ON consuntivi.podedificio=immobile.pod " +
						     "WHERE 1=1 "+ filter_string + " "
						     + aggregation_string;
						     

					System.out.println("query " + sql);
					
					multi = this.jdbcTemplate.query(sql, 
							new RowMapper<EnergiaTotaleAnnua>() {
								public EnergiaTotaleAnnua mapRow(ResultSet rs, int rowNum) throws SQLException {
									EnergiaTotaleAnnua b = new EnergiaTotaleAnnua();
																		
									//b.setEnergia_totale_annua(rs.getFloat("energia_totale_anno"));
									b.setPod(rs.getString("podedificio"));					
									b.setIndirizzo(rs.getString("indirizzo"));
									b.setComune(rs.getString("comune"));
									b.setProvincia(rs.getString("provincia"));
									b.setClusterEnergy(rs.getString("clusterEnergy"));
									//b.setAnno(rs.getString("anno"));
									return b;
								}
							}
					);
					
		}
			
		catch (Exception e) {
			    System.out.println(" JdbcEnergiaTotaleDAO::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return multi;
	}

}
*/