CREEMapp.directive('consumptionsChart', 

function() 
{

	return {
    scope: {},
    controller: ['$rootScope', '$scope', 'CreemSettings', 'ConsumptionsFactory', 'WeatherFactory', 'LocalWeatherFactory',
	
		function($rootScope, $scope, CreemSettings, ConsumptionsFactory, WeatherFactory, LocalWeatherFactory)
		{			
			var weatherValue = '';
			var seriesCounter = 0;
			$scope.dataset = [];
			$scope.dataconsumptions = {};
						
			
			var createChart = function(){
				var histconsumptionschart = new Highcharts.Chart({
					chart: {
						renderTo: 'histconsumptions',
						type: 'spline',
						zoomType: 'x'
					},
					title: {
						text: 'Grafico consumi storici (kw/h)'
					},
					
					credits: {
						enabled: false
					},
					
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: { // don't display the dummy year
							month: '%e. %b',
							year: '%b'
						},
						title: {
							text: 'Date'
						}
					},
					yAxis: [{ // Primary yAxis
                        labels: {
                        	format: '{value} kW',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        title: {
                        	text: 'Potenza consumata (kW)',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }
                    },  { // Secondary yAxis
                        title: {
                       		text: weatherValue,
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        labels: {
                        	format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        opposite: true
                    }],

					plotOptions: {
						spline: {
							marker: {
								enabled: false
							}
						}
					},

					series: $scope.dataset
				});
			}

			var getdata = function (building, datefrom, dateto) {

				$scope.dataconsumptions[building.pod] = ConsumptionsFactory.getConsumptions(building.pod, datefrom, dateto).query(
					function() {
						if($scope.dataconsumptions[building.pod].length) {
							$scope.dataset.push({
								name: building.codice,
								data: prepareConsumptionsData($scope.dataconsumptions[building.pod]),
								yAxis: 0,
							});

							
							seriesCounter += 1;
                                if (seriesCounter === CreemSettings.selectedbuildings.length) {
                                	createChart();
                                }
						}
					}
				);
				
			}
			
			var prepareConsumptionsData = function (response, weather)
			{				
				var tot = 0;
				var chartData = [];

				for (var i = 0; i < response.length; i++) {
					var consumi = [response[i]['01_am'], response[i]['02_am'], response[i]['03_am'], response[i]['04_am'], response[i]['05_am'], response[i]['06_am'],
						response[i]['07_am'], response[i]['08_am'], response[i]['09_am'], response[i]['10_am'], response[i]['11_am'], response[i]['12_pm'],
						response[i]['01_pm'], response[i]['02_pm'], response[i]['03_pm'], response[i]['04_pm'], response[i]['05_pm'], response[i]['06_pm'],
						response[i]['07_pm'], response[i]['08_pm'], response[i]['09_pm'], response[i]['10_pm'], response[i]['11_pm'], response[i]['12_am']];
					for (var j = 0; j < 24; j++) {
						var newDate = new Date(response[i]['data']);						
						chartData.push([
							Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), j, 0, 0, 0),
							consumi[j]
						]);

					}
				}				
				return chartData;
			}
			
			
			
			var refreshData = function ()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);
					$scope.dataconsumptions = {}
					seriesCounter = 0;
					
					
					weatherValue = CreemSettings.selectedWeather;
                            var weatherData = [];
                            var data = [];

                            data = LocalWeatherFactory.getWeather('Palermo', CreemSettings.selectedDates.from, CreemSettings.selectedDates.to).query(function(){

                                var dataLen = data.length;
                                
								for (var j=0; j<dataLen; j++)
								{								
										var newDate = new Date(data[j]["date_w"]);
										weatherData.push([
											parseInt(Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), parseInt(data[j]["time_w"]), 0, 0, 0)), 
											parseInt(data[j][weatherValue])]);
								}
								
                                $scope.dataset.push({
                                    name: weatherValue,
                                    data: weatherData,
                                    yAxis: 1,
									lineWidth: 2,
									dashStyle: "Dash"
                                });

                            });
							
										
					for(var i=0; i<CreemSettings.selectedbuildings.length; i++)
					{							
							getdata(CreemSettings.selectedbuildings[i], CreemSettings.selectedDates.from, CreemSettings.selectedDates.to);
					}
					
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();


			
		}],
		
		templateUrl: 'directives/consumptions-chart.html'
  };
  
});
	
	