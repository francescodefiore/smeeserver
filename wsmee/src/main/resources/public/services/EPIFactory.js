
CREEMapp.factory('EPIFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/epi';
			var EPIFactory = {};
			
			EPIFactory.getEPI = function(idImmobile) 
			{
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/indici?filterBy=IMMOBILE&filterParams=" + idImmobile, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: false,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}				
									}							
								});
								
			}
			
			EPIFactory.getEpiForm = function(idimmobile) 
			{
					
					LoadingFactory.startLoading();
					
					return $resource(RESTservAddr + urlBase + "/moduloepi?idImmobile=" + idimmobile, {}, 
									{
										query: {
											method: 'GET',
											isArray : false,
											cache: false,										
											interceptor: {
												response: function (data) {										
													LoadingFactory.stopLoading();
												},
												responseError: function (data) {
													LoadingFactory.stopLoading();
												}
											}							
										}							
									});
									
			}
			
			EPIFactory.getEpiCOPForm = function(tipoPdC, idImmobile) 
			{
					
					LoadingFactory.startLoading();
					
					return $resource(RESTservAddr + urlBase + "/getCOP?filterBy=COP&filterParams=" + tipoPdC + "&filterBy=IMMOBILE&filterParams=" + idImmobile, [], 
									{
										query: {
											method: 'GET',
											isArray : true,
											cache: false,										
											interceptor: {
												response: function (data) {										
													LoadingFactory.stopLoading();
												},
												responseError: function (data) {
													LoadingFactory.stopLoading();
												}
											}							
										}							
									});
									
			}
			
			EPIFactory.getDatiGenerali = function() 
			{
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "/datigenerali", {}, 
								{
									query: {
										method: 'GET',
										isArray : false,
										cache: false,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}				
									}							
								});
								
			}
			
			return EPIFactory;
	}
						
]);

CREEMapp.factory("DatiGeneraliForm", function(RESTservAddr, $resource)
		{
			var urlBase = RESTservAddr + '/epi/datigenerali';
			return $resource(urlBase, {}, {
				update: {
					method: 'POST'
				}
			});
		}
);

CREEMapp.factory("EpiForm", function(RESTservAddr, $resource)
		{
			var urlBase = RESTservAddr + '/epi/moduloepi';
			return $resource(urlBase, {}, {
				update: {
					method: 'POST'
				}
			});
		}
);



