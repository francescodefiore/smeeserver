package it.smee.webservices;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import it.smee.buildings.dao.JdbcEdificiDAO;
import it.smee.buildings.models.Edificio;
import it.smee.daticampo.dao.JdbcDatiCampoDAO;
import it.smee.daticampo.models.DatiPesb;
import it.smee.daticampo.models.EnergyData;
import it.smee.daticampo.models.MisureCampo;
import it.smee.daticampo.models.Schede;
import it.smee.daticampo.models.SensorData;
import it.smee.daticampo.models.WeatherStation;
import it.smee.jboxlib.JBoxDAO;
import it.smee.manutenzione.dao.JdbcManutenzioneDAO;
import it.smee.manutenzione.models.Scenari;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DatiCampoController {

	private static final ApplicationContext context =	new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	private static final JBoxDAO daticampoDAO = (JBoxDAO) context.getBean("datiCampoDAO");
	
	public DatiCampoController() {
		System.out.println("==== /DatiCampoController ====");
	} 
	

	
	@RequestMapping(value = "/daticampo/thermal", method = RequestMethod.GET)
	public @ResponseBody List<MisureCampo> getDataThermal(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params)  {
		
		daticampoDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				daticampoDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
	
		
		return ((JdbcDatiCampoDAO)daticampoDAO).getDataThermal();
	}
	

	@RequestMapping(value = "/daticampo/weatherstation", method = RequestMethod.GET)
	public @ResponseBody List<WeatherStation> getDataWeatherStation(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params)  {
		
		daticampoDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				daticampoDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
	
		
		return ((JdbcDatiCampoDAO)daticampoDAO).getDataWeatherStation();
	}
	
	@RequestMapping(value = "/daticampo/sensordata", method = RequestMethod.GET)
	public @ResponseBody List<SensorData> sensor_data(@RequestParam(value="idImmobile", required=true) Integer idimmobile) {
		
		return ((JdbcDatiCampoDAO)daticampoDAO).getSensorDailyDataFromBuilding(idimmobile);
	}
	
	@RequestMapping(value = "/daticampo/energydata", method = RequestMethod.GET)
	public @ResponseBody List<EnergyData> energy_data(@RequestParam(value="idImmobile", required=true) Integer idimmobile) {
		
		return ((JdbcDatiCampoDAO)daticampoDAO).getEnergyDailyDataFromBuilding(idimmobile);
	}
	
	@RequestMapping(value = "/daticampo/schede", method = RequestMethod.GET)
	public @ResponseBody List<Schede> schede(@RequestParam(value="idImmobile", required=true) Integer idimmobile,
											 @RequestParam(value="tipoScheda", required=true) Integer tiposcheda) {
		
		return ((JdbcDatiCampoDAO)daticampoDAO).getBoardsBuilding(idimmobile,tiposcheda);
	}
	
	@RequestMapping(value = "/daticampo/datipesb", method = RequestMethod.GET)
	public @ResponseBody List<List<DatiPesb>> datipesb(@RequestParam(value="ips", required=false) String ips,
											   @RequestParam(value="ipf", required=false) String ipf,
											   @RequestParam(value="da", required=false) String da,
											   @RequestParam(value="a", required=false) String a,
											   @RequestParam(value="ipe", required=false) String ipe,
											   @RequestParam(value="ipl", required=false) String ipl,
											   @RequestParam(value="cole", required=false) String cole,
											   @RequestParam(value="iptl", required=false) String iptl,
											   @RequestParam(value="ipt", required=false) String ipt,
											   @RequestParam(value="colt", required=false) String colt,
											   @RequestParam(value="q", required=false) String q){
		
		List<List<DatiPesb>> list = new LinkedList<List<DatiPesb>>();
		
		if(q.contains("PESB"))
		{
			System.out.println("AGGIUNGO PESB");
			list.add( ((JdbcDatiCampoDAO)daticampoDAO).getDatiPesb(ips,ipf,da,a));
		}
		else
			list.add(new ArrayList<DatiPesb>());
		
		if(q.contains("ENERGY"))
		{
			System.out.println("AGGIUNGO ENERGY");
			list.add( ((JdbcDatiCampoDAO)daticampoDAO).getDatiEnergy(ipl,ipe,cole,da,a));
		}
		else
			list.add(new ArrayList<DatiPesb>());
		
		if(q.contains("THERMAL"))
		{
			System.out.println("AGGIUNGO THERMAL");
			list.add( ((JdbcDatiCampoDAO)daticampoDAO).getDatiThermal(iptl,ipt,colt,da,a));
		}
		else
			list.add(new ArrayList<DatiPesb>());
		
		
		//return ((JdbcDatiCampoDAO)daticampoDAO).getDatiBoard(ips,ipf,da,a,ipe,cole);
		return list;
	}
	
	/*@RequestMapping(value = "/daticampo/datipesb", method = RequestMethod.POST,consumes = {"application/json" },produces= {"application/json" })
	public @ResponseBody DatiSchede datipesb(@RequestBody ParametriPesb parpesb){
		System.out.println(parpesb.getDa());
		return ((JdbcDatiCampoDAO)daticampoDAO).getDatiPesb(parpesb);
	}*/
	
	
}
