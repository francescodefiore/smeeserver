package it.smee.epi.dao;

import it.smee.epi.models.*;
import it.smee.jboxlib.JBoxDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class JdbcModuloEpiDAO implements JBoxDAO {
	private JdbcTemplate jdbcTemplate;
	private String filter_string = "";
	String sql;
	
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void print() {
		System.out.println("--- JdbcEpiDAO");		
	}

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{			
				
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
				
			case "COP":	
				filter_string = filter_string.concat( copFilter(filter_params) );				
				break;
				
			case "IMMOBILE":	
				filter_string = filter_string.concat( immobileFilter(filter_params) );				
				break;

		}
		
	}


	@Override
	public void resetFilters() {
		filter_string = "";		
	}
	
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = " && idImmobile IN (SELECT idImmobile from smee_immobili WHERE pod = '" + pod[i] + "'";
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		s = s.concat(")");
		return s;
	}
	
	private String copFilter(String[] cop) {
		int i=0;
		String s = " && ( idCop = " + cop[i];
		while(++i<cop.length)
			s = s.concat(" OR idCop = " + cop[i]);
		s = s.concat(")");
		return s;
	}
	
	private String immobileFilter(String[] idImmobile) {
		int i=0;
		String s = "&& (pe.idImmobile = '" + idImmobile[i] + "' "; 
		while(++i<idImmobile.length)
			s = s.concat(" OR pe.idImmobile = '" + idImmobile[i] + "' ");
		s = s.concat(")");
		return s;
	}
	
	public DatiGenerali getDatiGenerali()
	{			
		String sql = "SELECT * FROM smee_epi_datigenerali WHERE id='0'";
		
		DatiGenerali dati = (DatiGenerali)this.jdbcTemplate.queryForObject(sql,
		          ParameterizedBeanPropertyRowMapper.newInstance(DatiGenerali.class));
		
		return dati;
		
	}
	
	
	public void AggiornaDatiGenerali(DatiGenerali dati)
	{
		
		String sql = "UPDATE smee_epi_datigenerali SET coefficienteFx = ?, fattoreFm = ?, costoEnergiaElettrica = ?, costoGasolio = ?, costoMetano = ?, rendimentoNazionale = ?, ricambioariaInverno_bench = ?, ricambioariaEstate_bench = ? WHERE id='0'";
		
		this.jdbcTemplate.update(sql, dati.getCoefficienteFx(), dati.getFattoreFm(), dati.getCostoEnergiaElettrica(), dati.getCostoGasolio(), dati.getCostoMetano(), dati.getRendimentoNazionale(), dati.getRicambioariaInverno_bench(), dati.getRicambioariaEstate_bench());
	}
	
	
	
	
	public ModuloEpi getModuloEpi(Integer idimmobile)
	{
		String id_moduloepi = "";
		String idImmobile = "";
		String idComune = "";
		
		ModuloEpi modulo = new ModuloEpi();

		
		try {
					sql = "SELECT * FROM smee_epi_moduloepi WHERE idImmobile= '"+ idimmobile + "'";
					
					List<Map<String,Object>> results = this.jdbcTemplate.queryForList(sql);
					//System.out.println("EPIEdificio::Query 1 "+sql);
					
					Irradianza irradianza_invernale = new Irradianza();
					Irradianza irradianza_estiva = new Irradianza();
					DatiEdificio geometria = new DatiEdificio();
					
					for (Map<String, Object> m : results){
						idImmobile = m.get("idImmobile").toString();
						id_moduloepi =  m.get("idmodulo_epi").toString();
												
						irradianza_invernale.setNord((float) m.get("irradianza_N"));
						irradianza_invernale.setNordest( (float) m.get("irradianza_NE"));
						irradianza_invernale.setNordovest( (float) m.get("irradianza_NO"));
						irradianza_invernale.setSud( (float) m.get("irradianza_S"));
						irradianza_invernale.setSudest( (float) m.get("irradianza_SE"));
						irradianza_invernale.setSudovest( (float) m.get("irradianza_SO"));
						irradianza_invernale.setOvest( (float) m.get("irradianza_O"));
						irradianza_invernale.setEst( (float) m.get("irradianza_E"));
						
						irradianza_estiva.setNord( (float) m.get("irradianza_N_est"));
						irradianza_estiva.setNordest( (float) m.get("irradianza_NE_est"));
						irradianza_estiva.setNordovest( (float) m.get("irradianza_NO_est"));
						irradianza_estiva.setSud( (float) m.get("irradianza_S_est"));
						irradianza_estiva.setSudest( (float) m.get("irradianza_SE_est"));
						irradianza_estiva.setSudovest( (float) m.get("irradianza_SO_est"));
						irradianza_estiva.setOvest( (float) m.get("irradianza_O_est"));
						irradianza_estiva.setEst( (float) m.get("irradianza_E_est"));				
						
						geometria.setAltezza( (float) m.get("altezza"));						
						geometria.setSuperficielorda( (float) m.get("superficie_lorda"));
		            	geometria.setSuperficienetta( (float) m.get("superficie_netta"));
		            	geometria.setSuperficiedisperdentelorda( (float) m.get("superficie_disperdente_lorda"));
		            	geometria.setVolumelordo( (float) m.get("volume_lordo"));
		            	geometria.setVolumenetto( (float) m.get("volume_netto"));									
		            	geometria.setEtae_caldaia( (float) m.get("etae"));
		            	geometria.setEtarg_caldaia( (float) m.get("etarg"));
		            	geometria.setEtad_caldaia( (float) m.get("etad"));
		            	geometria.setEtagn_caldaia( (float) m.get("etagn"));	
		            	geometria.setEtae_PdC( (float) m.get("etae_PdC"));
		            	geometria.setEtarg_PdC( (float) m.get("etarg_PdC"));
		            	geometria.setEtad_PdC( (float) m.get("etad_PdC"));
		            	geometria.setEtagn_PdC( (float) m.get("etagn_PdC"));	
		            	geometria.setEtae_estivo( (float) m.get("etae_estivo"));
		            	geometria.setEtarg_estivo( (float) m.get("etarg_estivo"));
						geometria.setEtad_estivo( (float) m.get("etad_estivo"));
						geometria.setEtagn_estivo( (float) m.get("etagn_estivo"));
						
						geometria.setApporto_interno((float) m.get("apporto_interno"));
						geometria.setApporto_interno_est((float) m.get("apporto_interno_est"));
						geometria.setRicambio_aria((float) m.get("ricambio_aria"));
						geometria.setRicambio_aria_est((float) m.get("ricambio_aria_est"));
						
						geometria.setPotenzaLux_Esterna((float) m.get("potenza_lux_esterna"));
						geometria.setPotenzaLux_Interna((float) m.get("potenza_lux_interna"));
						geometria.setOremedieLux_Esterna((float) m.get("oremedie_lux_esterna"));
						geometria.setOremedieLux_Interna((float) m.get("oremedie_lux_interna"));

						
						geometria.setQuotaCaldaie((float) m.get("quotaCaldaie"));
						geometria.setQuotaPdC( (float) m.get("quotaPdC"));
						
						geometria.setEta_bench_C( (float) m.get("eta_bench_C"));
		            		
		            	geometria.setEta_bench_PdC( (float) m.get("eta_bench_PdC"));
		            		
		            	geometria.setEta_bench_estivo( (float) m.get("eta_bench_estivo"));
		            							
						geometria.setPotenza_Caldaia((float) m.get("potenza_C"));
						geometria.setPotenza_PdC((float) m.get("potenza_PdC"));
						geometria.setPotenza_Climatizzazione((float) m.get("potenza_estivo"));
						
						geometria.setPotenza_media_Caldaia((float) m.get("potenza_media_C"));
						geometria.setPotenza_media_PdC((float) m.get("potenza_media_PdC"));
						geometria.setPotenza_media_Climatizzazione((float) m.get("potenza_media_estivo"));
						
						geometria.setTipologiadistribuzione_caldaie((String)m.get("tipologiadistribuzione_caldaie"));
						geometria.setTipologiadistribuzione_pdc((String)m.get("tipologiadistribuzione_pdc"));
						geometria.setTipologiadistribuzione_estivo((String)m.get("tipologiadistribuzione_estivo"));
						geometria.setTipologiascambio_pdc((String)m.get("tipologiascambio_pdc"));
						geometria.setTipologiascambio_estivo((String)m.get("tipologiascambio_estivo"));
						
						geometria.setNumeropiani((int) m.get("numeropiani"));
						geometria.setSuperficielordapiano((float) m.get("superficielordapiano"));
						geometria.setSuperficienettapiano((float) m.get("superficienettapiano"));
						
						//System.out.println("EPIEdificio::Query 13 "+id_moduloepi);
					}
					
					modulo.setIrradianza_invernale(irradianza_invernale);													
					modulo.setIrradianza_estiva(irradianza_estiva);
					modulo.setDatiedificio(geometria);
					modulo.setIdImmobile(idImmobile);
					
				    				    
		
					sql = "SELECT * FROM smee_epi_superfici_esterne_opache where idmodulo_epi='" + Integer.parseInt(id_moduloepi) +"'";
					//System.out.println("EPIEdificio::Query 2 "+sql);
					
					List<Supesterneopache> supesterneopache = this.jdbcTemplate.query(sql,
					        new RowMapper<Supesterneopache>() {
					            public Supesterneopache mapRow(ResultSet rs, int rowNum) throws SQLException {
					            	Supesterneopache sup = new Supesterneopache();
					            	sup.setAmbienteconfinante(rs.getString("ambiente_confinante"));
									sup.setCalorespecifico( rs.getString("calore_specifico") );
									sup.setDescrizione( rs.getString("descrizione") );
									sup.setEsposizione( rs.getString("esposizione") );
									sup.setFattorecorrezione( rs.getString("fattore_correzione") );
									sup.setMassa( rs.getString("massa") );
									sup.setMateriale(rs.getString("materiale") );
									sup.setSpessore( rs.getString("spessore") );
									sup.setSuperficieseo( rs.getString("superficie_seo") );
									sup.setTiposuperficie( rs.getString("tipo_superficie") );
									sup.setTrasmittanzaeop( rs.getString("trasmittanza_seo") );
					                
					                return sup;
					            }
					        });
								
					modulo.setSupesterneopache(supesterneopache);
					
					
					sql = "SELECT * FROM smee_epi_componenti_trasparenti where idmodulo_epi='" + Integer.parseInt(id_moduloepi) + "'";
					//System.out.println("EPIEdificio::Query 3 "+sql);
					List<ComponentiTrasparenti> comptrasp = this.jdbcTemplate.query(sql,
					        new RowMapper<ComponentiTrasparenti>() {
					            public ComponentiTrasparenti mapRow(ResultSet rs, int rowNum) throws SQLException {
					            	ComponentiTrasparenti comp = new ComponentiTrasparenti();
					            	comp = new ComponentiTrasparenti();
									comp.setEsposizioneinfissi( rs.getString("esposizione_infissi") );
									comp.setFlagsi( rs.getString("flagsi") );
									comp.setNote(rs.getString("note"));
									comp.setSupinfissosi(rs.getString("sup_infisso_si"));
									comp.setTipogas( rs.getString("tipo_gas") );
									comp.setTipotelaio( rs.getString("tipo_telaio") );
									comp.setTipovetro(rs.getString("tipo_vetro"));
									comp.setTrasmittanzausi(rs.getString("trasmittanza_usi"));
					                
					                return comp;
					            }
					        });

					modulo.setComponentitrasparenti(comptrasp);
								 
					sql = "SELECT smee_immobili.*, smee_comuni.*, o.ore FROM smee_immobili join smee_comuni on smee_immobili.idComune=smee_comuni.idComune, smee_epi_riscaldamento_ore o where idImmobile ='"+ Integer.parseInt(idImmobile) +"' AND o.zona=smee_comuni.zona";			 
					//System.out.println("EPIEdificio::Query 4 "+sql);
					List<Map<String,Object>> rs = this.jdbcTemplate.queryForList(sql);
	
					for (Map<String, Object> m : rs){
						idComune=m.get("idComune").toString();												
					}


		} 
		
		catch (Exception e) {
			    System.out.println("EPIEdificio::Query non eseguita su modulo_epi " + e );
			    
		}
		
		
		return modulo;
	}


	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}
	

	public int Calcola_Feriali(String data_da, String data_a, int sabato) {
		long ONE_HOUR = 60 * 60 * 1000L;
		int days = 0;
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		fmt.setLenient(false);
		Calendar inizio = Calendar.getInstance();
		String m_d1[]=data_da.split("-");
		inizio.set(Integer.parseInt(m_d1[2]), Integer.parseInt(m_d1[1]), Integer.parseInt(m_d1[0]));
		Calendar fine = Calendar.getInstance();
		String m_d2[]=data_a.split("-");
		fine.set(Integer.parseInt(m_d2[2]), Integer.parseInt(m_d2[1]), Integer.parseInt(m_d2[0]));
		long millisDiff = (fine.getTimeInMillis() - inizio.getTimeInMillis() + ONE_HOUR);
		int totali = (int) (millisDiff / 86400000);
		int festivi = 0;
		for (int i = 1; i <= totali; i++){
			if(IsFestivo(inizio, sabato)){
				festivi++;
			}
			inizio.add(Calendar.DAY_OF_MONTH,1);
		}
		
		days = totali - festivi;
		return days;
	}

	public Calendar GetPasquetta(int iYear)
	{
		int a,b,c,d,e,f,g,h,i,k,l,m,n,p;
		int iDay,iMonth;
	
		a=iYear % 19;
		b=(int) (iYear/100);
		c=iYear % 100;
		d=(int) (b/4);
		e=b % 4;
		f=(int) ((b+8)/25);
		g=(int) ((b-f+1)/3);
		h=(19 * a+b-d-g+15) % 30;
		i=(int) (c/4);
		k=c % 4;
		l=(32+2*e+2*i-h-k) % 7;
		m=(int) ((a+11*h-221)/451);
		n=(int) (h+l-7*m+114) / 31;
		p=(h+l-7*m+114) % 31;
	
		iDay=p+2;//p+1 è Pasqua
		iMonth=n;
		
		Calendar pasquetta = Calendar.getInstance();
		pasquetta.set(iYear,iMonth, iDay);
	
		return pasquetta;
	}
	
	public boolean IsFestivo(Calendar day, int sabato)
	{
		boolean festa=false;
		//sabato
		if (sabato==1 && day.get(Calendar.DAY_OF_WEEK) == 7)
			festa=true;
		//domenica
		if(day.get(Calendar.DAY_OF_WEEK) == 1)
			festa=true;
		//capodanno
		if (day.get(Calendar.MONTH)==0 && day.get(Calendar.DAY_OF_MONTH)==1)
			festa=true;
		//epifania
		if (day.get(Calendar.MONTH)==0 && day.get(Calendar.DAY_OF_MONTH)==6)
			festa=true;
		//25 aprile
		if (day.get(Calendar.MONTH)==3 && day.get(Calendar.DAY_OF_MONTH)==25)
			festa=true;
		//1 maggio
		if (day.get(Calendar.MONTH)==4 && day.get(Calendar.DAY_OF_MONTH)==1)
			festa=true;
		//2 giugno
		if (day.get(Calendar.MONTH)==5 && day.get(Calendar.DAY_OF_MONTH)==2)
			festa=true;
		//15 agosto
		if (day.get(Calendar.MONTH)==7 && day.get(Calendar.DAY_OF_MONTH)==15)
			festa=true;
		//1 novembre
		if (day.get(Calendar.MONTH)==10 && day.get(Calendar.DAY_OF_MONTH)==1)
			festa=true;
		//8 dicembre 
		if (day.get(Calendar.MONTH)==11 && day.get(Calendar.DAY_OF_MONTH)==8)
			festa=true;
		//25 dicembre
		if (day.get(Calendar.MONTH)==11 && day.get(Calendar.DAY_OF_MONTH)==25)
			festa=true;
		//26 dicembre
		if (day.get(Calendar.MONTH)==11 && day.get(Calendar.DAY_OF_MONTH)==26)
			festa=true;
		Calendar pasquetta = GetPasquetta(day.get(Calendar.YEAR));
		if (pasquetta.compareTo(day)==0) 
			festa = true;
		
		
		//Inserire controllo  festa Patrono
		
		return festa;
	}	
	
	public float get_irradianza_solare(String esposizione, Irradianza irradianza) {
		float irradianzainfissi;
		if (esposizione.equalsIgnoreCase("N")){
			irradianzainfissi=irradianza.getNord();
			return irradianzainfissi;
		}
		if (esposizione.equalsIgnoreCase("NE")){
			irradianzainfissi=irradianza.getNordest();
			return irradianzainfissi;
		}
		if (esposizione.equalsIgnoreCase("E")){
			irradianzainfissi=irradianza.getEst();
			return irradianzainfissi;
		}
		if (esposizione.equalsIgnoreCase("SE")){
			irradianzainfissi=irradianza.getSudest();
			return irradianzainfissi;
		}
		if (esposizione.equalsIgnoreCase("S")){
			irradianzainfissi=irradianza.getSud();
			return irradianzainfissi;
		}
		if (esposizione.equalsIgnoreCase("SO")){
			irradianzainfissi=irradianza.getSudovest();
			return irradianzainfissi;
		}
		if (esposizione.equalsIgnoreCase("O")){
			irradianzainfissi=irradianza.getOvest();
			return irradianzainfissi;
		}
		if (esposizione.equalsIgnoreCase("NO")){
			irradianzainfissi=irradianza.getNordovest();
			return irradianzainfissi;
		}
		
		
		return 0;
	}
	

	public void CalcolaPrestazioniEnergetiche(int idImmobile) {
		System.out.println(" ======== CalcolaPrestazioniEnergetiche ===========" );
		ModuloEpi modulo_epi = getModuloEpi(idImmobile);

		List<Supesterneopache> supesterneopache=modulo_epi.getSupesterneopache();
		List<ComponentiTrasparenti> componentitrasparenti=modulo_epi.getComponentitrasparenti();
		DatiEdificio datiedificio = modulo_epi.getDatiedificio();
		Irradianza irradianza_invernale = modulo_epi.getIrradianza_invernale();
		Irradianza irradianza_estiva = modulo_epi.getIrradianza_estiva();	
		
		DatiGenerali datigenerali = this.getDatiGenerali();
		
		float rendimento_nazionale = (datigenerali.getRendimentoNazionale());
		float quotaCaldaie = (datiedificio.getQuotaCaldaie());
		float quotaPdC = (datiedificio.getQuotaPdC());
		
		float Ht_superficiopache=0;
		float Ht_serramenti=0;
		float Ht_seo_bench=0;
		float Ht_infissi_bench=0;
		float HT_bench=0;
				
		String sql = "";
		String zona="";
		String data_dal="";
		String data_al="";
		float num_ore_totale=0;
		float he=1300; //valore di default
		
		//lo scambio termico per trasmissione
		float HT_inv=0; 
		//lo scambio termico per trasmissione è uguale alla prestazione invernale
		float HT_est=0;
		
		//lo scambio termico per ventilazione
		float HV_inv=0;
		float HV_est=0;
		float HV_inv_bench=0;
		float HV_est_bench=0;
		
		//Apporti gratuiti interni
		float Qi_inv=0;
		float Qi_est=0;
		
		//Apporti solari
		float Qs_inv=0;
		float Qs_est=0;
		
		//Fabbisogno termico
		float Qh_inv=0;
		float Qh_est=0;
		float Qh_inv_bench=0;
		float Qh_est_bench=0;
		
		
		//Rendimenti Globali
		float ETAg_caldaie=0;
		float ETAg_PdC=0;
		float ETAg_estivo=0;
		
		//Indici di prestazione Invernale
		float EPIsn_inv=0;
		float EPIsn_GG_inv=0;
		float EPIsl_inv=0;
		float EPIsl_GG_inv=0;
		float EPIvn_inv=0;
		float EPIvn_GG_inv=0;
		float EPIvl_inv=0;
		float EPIvl_GG_inv=0;
		
		//Indici di prestazione Estiva
		float EPEsn=0;
		float EPEsn_GG=0;
		float EPEsl=0;
		float EPEsl_GG=0;
		float EPEvl=0;
		float EPEvl_GG=0;
		
		//Indici di prestazione Invernale di benchmark
		float EPIsn_inv_bench=0;
		float EPIsn_GG_inv_bench=0;
		float EPIsl_inv_bench=0;
		float EPIsl_GG_inv_bench=0;
		float EPIvl_inv_bench=0;
		float EPIvl_GG_inv_bench=0;
		
		//Indici di prestazione Estiva di benchmark
		float EPEsn_bench=0;
		float EPEsn_GG_bench=0;
		float EPEsl_bench=0;
		float EPEsl_GG_bench=0;
		float EPEvl_bench=0;
		float EPEvl_GG_bench=0;
		
		//Indici di prestazione Illuminazione 
		float EPILL=0;
		float kWhillumesterna;
		int sabatofestivo = 0;
		Integer GG=0;
		Integer GGe = 0;
	
		try {
			sql = "SELECT o.zona, o.data_dal, o.data_al, gradigiorno as GG, gradigiornoestivi as GGe "+
				  "FROM smee_epi_riscaldamento_ore o join smee_comuni c on o.zona=c.zona "+
				  "join smee_immobili i on c.idComune=i.idComune where i.idImmobile='"+modulo_epi.getIdImmobile()+"'";
			List<Map<String,Object>> results = this.jdbcTemplate.queryForList(sql);
			
			for (Map<String, Object> m : results){
				zona = m.get("zona").toString();
				data_dal =  m.get("data_dal").toString();
				data_al = m.get("data_al").toString();
				GG = (Integer) m.get("GG");
				GGe =  (Integer) m.get("GGe");
			}
			//System.out.println(" CalcolaPrestazioniEnergetiche:: " + data_dal + " - "+data_al);	
			//modulo_epi.setZonaclimatica(zona);
			sql = "SELECT orarioSab FROM smee_creem.smee_policies_immobili WHERE idImmobile = '" + idImmobile + "' ";            
			sabatofestivo = this.jdbcTemplate.queryForObject(sql, Integer.class);
				
			}
	
		catch (Exception e) {
			    System.out.println(" CalcolaPrestazioniEnergetiche::Query non eseguita su oreriscaldamento " + e);
			    
		}
		
		Calendar calendar = GregorianCalendar.getInstance();
		data_dal = data_dal.concat("-").concat(Integer.toString(calendar.get(Calendar.YEAR)));
		calendar.add(Calendar.YEAR, 1);
		data_al = data_al.concat("-").concat(Integer.toString(calendar.get(Calendar.YEAR)));
		int days = Calcola_Feriali(data_dal,data_al,sabatofestivo);
		//ore medie di lavoro 12.25
		//num_ore_totale = days*12.25f;	
		//Secondo l'algoritmo di Train  hi va calcolato per 8 ore lavorative al giorno festivi esclusi
		num_ore_totale = days*8f;
		
		//Calcolare i GGe - numero ore estivi he da maggio a ottobre	
		calendar = GregorianCalendar.getInstance();
		data_dal = "01-05-".concat(Integer.toString(calendar.get(Calendar.YEAR)));
		data_al = "31-10-".concat(Integer.toString(calendar.get(Calendar.YEAR)));
		days = Calcola_Feriali(data_dal,data_al,sabatofestivo);

		//Secondo l'algoritmo di Train  he va calcolato per 8 ore lavorative al giorno festivi esclusi
		he = days*8f;

		//calcolare la trasmittanza corretta e metterla in tabella per la generazione dei warning M40
		
		//Coefficiente scambio termico
		for (int i=0; i<supesterneopache.size(); i++) {
			Supesterneopache seo = supesterneopache.get(i);
			double supseo = Double.parseDouble(seo.getSuperficieseo());
			double Ueoc = Double.parseDouble(seo.getTrasmittanzaeop());
			double btr = Double.parseDouble(seo.getFattorecorrezione());
			if (btr == 0) btr=1;
			Ht_superficiopache += supseo*Ueoc*btr;	
		}
		
		for (int i=0; i<componentitrasparenti.size(); i++) {
			ComponentiTrasparenti ct = componentitrasparenti.get(i);
			double supct = Double.parseDouble(ct.getSupinfissosi());
			double Utct = Double.parseDouble(ct.getTrasmittanzausi());
				
			Ht_serramenti += supct*Utct;	
		}
				
		HT_inv = Ht_superficiopache + Ht_serramenti;
		HT_inv = (float) (Math.round( HT_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		HT_est=HT_inv;
		
		float U_bench_infissi=0;
		float U_bench_parete=0;
		float U_bench_pavimento=0;
		float U_bench_coperturapiana=0;
		float U_bench_coperturaafalda=0;
		try {
	
			sql = "SELECT valore, anno FROM smee_epi_trasmittanze t inner join smee_epi_tipi_superfici s "+
			      "on t.idsuperficie=s.idtipo_superfici WHERE s.tipo_superfici like 'Infissi' AND t.idzona = '"+zona+"' order by anno desc;";       
			List<Map<String,Object>> rs_infissi = this.jdbcTemplate.queryForList(sql);
			U_bench_infissi = Float.parseFloat(rs_infissi.get(0).get("valore").toString());
			
			sql =  "SELECT valore, anno FROM smee_epi_trasmittanze t inner join smee_epi_tipi_superfici s "+
			       "on t.idsuperficie=s.idtipo_superfici WHERE s.tipo_superfici like 'Parete' AND t.idzona = '"+zona+"' order by anno desc;";  
			List<Map<String,Object>> rs_parete = this.jdbcTemplate.queryForList(sql);
			U_bench_parete = Float.parseFloat(rs_parete.get(0).get("valore").toString());
			
			sql = "SELECT valore, anno FROM smee_epi_trasmittanze t inner join smee_epi_tipi_superfici s "+
			      "on t.idsuperficie=s.idtipo_superfici WHERE s.tipo_superfici like 'Pavimento' AND t.idzona = '"+zona+"' order by anno desc;";	          
			List<Map<String,Object>> rs_pavimento = this.jdbcTemplate.queryForList(sql);
			U_bench_pavimento = Float.parseFloat(rs_pavimento.get(0).get("valore").toString());
			
			sql = "SELECT valore, anno  FROM smee_epi_trasmittanze t inner join smee_epi_tipi_superfici s "+
			      "on t.idsuperficie=s.idtipo_superfici join smee_epi_orientamento_superfici o on t.idorientamento=o.id_orientamento "+
			      "WHERE s.tipo_superfici like 'Copertura' AND o.orientamento like 'Piana'  AND t.idzona = '"+zona+"' order by anno desc;";	  
			List<Map<String,Object>> rs_piana = this.jdbcTemplate.queryForList(sql);
			U_bench_coperturapiana = Float.parseFloat(rs_piana.get(0).get("valore").toString());
			
			sql = "SELECT valore, anno FROM smee_epi_trasmittanze t inner join smee_epi_tipi_superfici s "+
			          "on t.idsuperficie=s.idtipo_superfici join smee_epi_orientamento_superfici o on t.idorientamento=o.id_orientamento "+
			          "WHERE s.tipo_superfici like 'Copertura' AND o.orientamento like 'A Falda'  AND t.idzona = '"+zona+"' "+"order by anno desc;";	          
			List<Map<String,Object>> rs_falda = this.jdbcTemplate.queryForList(sql);
			U_bench_coperturaafalda = Float.parseFloat(rs_falda.get(0).get("valore").toString());
	
		}
			
		catch (Exception e) {
			    System.out.println(" CalcolaPrestazioniEnergetiche::Query non eseguita su trasmittanza " + e);
			    
		}
		
		
		//Coefficiente scambio termico di Benchmarch
		for (int i=0; i<supesterneopache.size(); i++) {
			Supesterneopache seo = supesterneopache.get(i);
			//System.out.println("Supesterneopache " + seo.getTiposuperficie());
		    
			double supseo = Float.parseFloat(seo.getSuperficieseo());
			if ((seo.getTiposuperficie()).equalsIgnoreCase("parete")) {
				Ht_seo_bench += supseo*U_bench_parete;	
			}
			else if ((seo.getTiposuperficie()).equalsIgnoreCase("pavimento")) {
				Ht_seo_bench += supseo*U_bench_pavimento;	
			}
			else if ((seo.getTiposuperficie()).equalsIgnoreCase("piana")) {
				Ht_seo_bench += supseo*U_bench_coperturapiana;	
			}
			else if ((seo.getTiposuperficie()).equalsIgnoreCase("inclinata")) {
				Ht_seo_bench += supseo*U_bench_coperturaafalda;	
			}
		}
		
		for (int i=0; i<componentitrasparenti.size(); i++) {
			ComponentiTrasparenti ct = componentitrasparenti.get(i);
			float supct = Float.parseFloat(ct.getSupinfissosi());
				
			Ht_infissi_bench += supct*U_bench_infissi;	
		}
				
		HT_bench = Ht_seo_bench + Ht_infissi_bench;
		HT_bench = (float) (Math.round( HT_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		
		//Coefficiente scambio termico ventilazione inverno
		float n = (datiedificio.getRicambio_aria());
		float VolumeNetto = (datiedificio.getVolumenetto());
		float n_bench = (datigenerali.getRicambioariaInverno_bench());		
		
		HV_inv = 0.34f*n*VolumeNetto;
		HV_inv = (float) (Math.round( HV_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		HV_inv_bench = 0.34f*n_bench*VolumeNetto;
		HV_inv_bench = (float) (Math.round( HV_inv_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		//Coefficiente scambio termico ventilazione estivo
		float ne = (datiedificio.getRicambio_aria_est());
		float ne_bench = (datigenerali.getRicambioariaEstate_bench());
		
		HV_est = 0.34f*ne*VolumeNetto;
		HV_est = (float) (Math.round( HV_est * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));

		HV_est_bench = 0.34f*ne_bench*VolumeNetto;
		HV_est_bench = (float) (Math.round( HV_est_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		//Apporti gratuiti interni inverno
		float fattoreapporto = (datiedificio.getApporto_interno());
		float superficienetta = (datiedificio.getSuperficienetta());
		Qi_inv = (fattoreapporto*superficienetta*num_ore_totale)/1000.f;
		Qi_inv = (float) (Math.round( Qi_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		//Apporti gratuiti interni estivi
		float fattoreapporto_estivo = (datiedificio.getApporto_interno_est());
		Qi_est = (fattoreapporto_estivo*superficienetta*he)/1000.f;
		Qi_est = (float) (Math.round( Qi_est * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));

		
		
		float sommaapporti = 0;
		//Apporti solari - solo per serramenti con flag si
		//System.out.println("componentitrasparenti: " +componentitrasparenti.size());
		for (int i=0; i<componentitrasparenti.size(); i++) {
			ComponentiTrasparenti ct = componentitrasparenti.get(i);
			if (ct.getFlagsi().equalsIgnoreCase("SI")){
				float supinfissi = Float.parseFloat(ct.getSupinfissosi());
				String esposizione = ct.getEsposizioneinfissi();
				float irradianzainfissi = get_irradianza_solare(esposizione, irradianza_invernale);
				sommaapporti = sommaapporti + supinfissi*irradianzainfissi;	
			}
		}
		Qs_inv = (0.2f*sommaapporti);
		Qs_inv = (float) (Math.round( Qs_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		
		float sommaapporti_estivi = 0;
		//Apporti solari - solo per serramenti con flag si
		for (int i=0; i<componentitrasparenti.size(); i++) {
			ComponentiTrasparenti ct = componentitrasparenti.get(i);
			if (ct.getFlagsi().equalsIgnoreCase("SI")){
				float supinfissi = Float.parseFloat(ct.getSupinfissosi());
				String esposizione = ct.getEsposizioneinfissi();
				float irradianzainfissi = get_irradianza_solare(esposizione, irradianza_estiva);
				sommaapporti_estivi = sommaapporti_estivi + supinfissi*irradianzainfissi;	
			}
		}
		Qs_est = (0.2f*sommaapporti_estivi);
		Qs_est = (float) (Math.round( Qs_est * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
			
		//FABBISOGNO DI ENERGIA TERMICA DELL'EDIFICIO
		
		float fx = (datigenerali.getCoefficienteFx());
		Qh_inv=(0.024f*GG*(HT_inv+HV_inv)-fx*(Qs_inv+Qi_inv));
		Qh_inv = (float) (Math.round( Qh_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		Qh_est=(0.024f*GGe*(HT_est+HV_est)+fx*(Qs_est+Qi_est));
		Qh_est = (float) (Math.round( Qh_est * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		Qh_inv_bench=(0.024f*GG*(HT_bench+HV_inv_bench)-fx*(Qs_inv+Qi_inv));
		Qh_inv_bench = (float) (Math.round( Qh_inv_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		Qh_est_bench=(0.024f*GGe*(HT_bench+HV_est_bench)+fx*(Qs_est+Qi_est));
		Qh_est_bench = (float) (Math.round( Qh_est_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		System.out.println("==== Fabbisogno QHT INV 1==== "+(0.024f*GG*(HT_inv+HV_inv)));
		System.out.println("==== Fabbisogno QHIS INV 2==== "+(fx*(Qs_inv+Qi_inv)));
		System.out.println("==== Fabbisogno Qh Invernale==== "+Qh_inv);
		
		System.out.println("==== Fabbisogno QHT EST 1==== "+(0.024f*GGe*(HT_est+HV_est)));
		System.out.println("==== Fabbisogno QHIS EST 2==== "+(fx*(Qs_est+Qi_est)));
		System.out.println("==== Fabbisogno Qh Estiva==== "+Qh_est);
		
		System.out.println("==== Fabbisogno QHT INV 1==== "+(0.024f*GG*(HT_bench+HV_inv_bench)));
		System.out.println("==== Fabbisogno QHIS INV 2==== "+(fx*(Qs_inv+Qi_inv)));
		System.out.println("==== Fabbisogno Qh Invernale bench==== "+Qh_inv_bench);
		
		System.out.println("==== Fabbisogno QHT EST 1==== "+(0.024f*GGe*(HT_bench+HV_est_bench)));
		System.out.println("==== Fabbisogno QHIS EST 2==== "+(fx*(Qs_est+Qi_est)));
		System.out.println("==== Fabbisogno Qh Estiva bench==== "+Qh_est_bench);
		
		System.out.println("==== Numero di ore complessive hi==== "+num_ore_totale);
		System.out.println("==== Numero di ore complessive he==== "+he);
		System.out.println("==== GGe ===="+GGe);
		
		//Rendimento di emissione Caldaie
		float etae = (modulo_epi.getDatiedificio().getEtae_caldaia());
		//Rendimento di regolazione Caldaie
		float etarg = (modulo_epi.getDatiedificio().getEtarg_caldaia());
		//Rendimento di distribuzione Caldaie
		float etad= (modulo_epi.getDatiedificio().getEtad_caldaia());
		//Rendimento di generazione Caldaie
		float etagn = (modulo_epi.getDatiedificio().getEtagn_caldaia());
		
		ETAg_caldaie = etae*etarg*etad*etagn;
		ETAg_caldaie = (float) (Math.round( ETAg_caldaie * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		//Rendimento di emissione PdC
		float etae_PdC = (modulo_epi.getDatiedificio().getEtae_PdC());
		//Rendimento di regolazione PdC
		float etarg_PdC = (modulo_epi.getDatiedificio().getEtarg_PdC());
		//Rendimento di distribuzione PdC
		float etad_PdC= (modulo_epi.getDatiedificio().getEtad_PdC());
		//Rendimento di generazione PdC
		float etagn_PdC = (modulo_epi.getDatiedificio().getEtagn_PdC());
		
		ETAg_PdC = etae_PdC*etarg_PdC*etad_PdC*etagn_PdC;
		ETAg_PdC = (float) (Math.round( ETAg_PdC * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));	
		
		//Rendimento di emissione Climatizzazione Estiva
		float etae_estivo = (modulo_epi.getDatiedificio().getEtae_estivo());
		//Rendimento di regolazione Climatizzazione Estiva
		float etarg_estivo = (modulo_epi.getDatiedificio().getEtarg_estivo());
		//Rendimento di distribuzione Climatizzazione Estiva
		float etad_estivo= (modulo_epi.getDatiedificio().getEtad_estivo());
		//Rendimento di generazione Climatizzazione Estiva
		float etagn_estivo = (modulo_epi.getDatiedificio().getEtagn_estivo());
		
		ETAg_estivo = etae_estivo*etarg_estivo*etad_estivo*etagn_estivo;
		ETAg_estivo = (float) (Math.round( ETAg_estivo * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));	
		
		float FEPic = 0;
		if (quotaCaldaie>0) {
			FEPic = (Qh_inv/ETAg_caldaie)*(quotaCaldaie/100.f);
		}
		float FEPiPDC = 0;
		if (quotaPdC>0) {
			FEPiPDC = ((Qh_inv/ETAg_PdC)*(quotaPdC/100.f))/rendimento_nazionale;
		}
		float FEPi = FEPic + FEPiPDC;
		float FFe = Qh_est/ETAg_estivo;
		float FEP_estivo = FFe/rendimento_nazionale;
		
		float supnetta = (datiedificio.getSuperficienetta());
		float suplorda = (datiedificio.getSuperficielorda());
		float volnetto = (datiedificio.getVolumenetto());
		float vollordo = (datiedificio.getVolumelordo());
		
		//Calcolo indici prestazione invernale
		System.out.println("FEPi" + FEPi);
		System.out.println("supnetta" + supnetta);
		EPIsn_inv = FEPi/supnetta;
		EPIsn_inv = (float) (Math.round( EPIsn_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIsn_GG_inv = (float) (EPIsn_inv/(0.024f*GG));
		EPIsn_GG_inv = (float) (Math.round( EPIsn_GG_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIsl_inv = FEPi/suplorda;
		EPIsl_inv = (float) (Math.round( EPIsl_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIsl_GG_inv = (float) (EPIsl_inv/(0.024f*GG));
		EPIsl_GG_inv = (float) (Math.round( EPIsl_GG_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIvn_inv = FEPi/volnetto;
		EPIvn_inv = (float) (Math.round( EPIvn_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIvn_GG_inv = (float) (EPIvn_inv/(0.024f*GG));
		EPIvn_GG_inv = (float) (Math.round( EPIvn_GG_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIvl_inv = FEPi/vollordo;
		EPIvl_inv = (float) (Math.round( EPIvl_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIvl_GG_inv = (float) (EPIvl_inv/(0.024f*GG));
		EPIvl_GG_inv = (float) (Math.round( EPIvl_GG_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		//System.out.println("Manutenzione::episuperficieutile "+EPIsn_inv+" ::episuperficielorda " + EPIsl_inv);
		

		
		//Calcolo indici prestazione Estivi
		EPEsn = FEP_estivo/supnetta;
		EPEsn = (float) (Math.round( EPEsn * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPEsn_GG = (float) (EPEsn/(0.024f*GGe));
		EPEsn_GG = (float) (Math.round( EPEsn_GG * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPEsl = FEP_estivo/suplorda;
		EPEsl = (float) (Math.round( EPEsl * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPEsl_GG = (float) (EPEsl/(0.024f*GGe));
		EPEsl_GG = (float) (Math.round( EPEsl_GG * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPEvl = FEP_estivo/vollordo;
		EPEvl = (float) (Math.round( EPEvl * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPEvl_GG= (float) (EPEvl/(0.024f*GGe));
		EPEvl_GG = (float) (Math.round( EPEvl_GG * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		
		//CALCOLO DEL CONSUMO D'ENERGIA PER L'ILLUMINAZIONE INTERNA
		
		float lux_int= (datiedificio.getPotenzaLux_Interna());
		float ore_int= (datiedificio.getOremedieLux_Interna());
		float kWhilluminterna = lux_int*ore_int;
		
		EPILL = kWhilluminterna/vollordo;
		EPILL = (float) (Math.round( EPILL * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		float lux_esterna= (datiedificio.getPotenzaLux_Esterna());
		float ore_esterna= (datiedificio.getOremedieLux_Esterna());
	    kWhillumesterna = lux_esterna*ore_esterna;
	    kWhillumesterna = (float) (Math.round( kWhillumesterna * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));		
		float etag_estivo_bench = (modulo_epi.getDatiedificio().getEta_bench_estivo());
	    float ETAg_caldaie_bench = (modulo_epi.getDatiedificio().getEta_bench_C());
	    float ETAg_PdC_bench = (modulo_epi.getDatiedificio().getEta_bench_PdC());
	    
	    float FEPic_bench = ETAg_caldaie_bench == 0 ? 0 : (Qh_inv_bench/ETAg_caldaie_bench)*(quotaCaldaie/100.f);
		float FEPiPDC_bench = ETAg_PdC_bench == 0 ? 0 : ((Qh_inv_bench/ETAg_PdC_bench)*(quotaPdC/100.f))/rendimento_nazionale;
		float FEPi_bench = FEPic_bench + FEPiPDC_bench;
		
		float FFe_bench = Qh_est_bench/etag_estivo_bench;
		float FEP_estivo_bench = FFe_bench/rendimento_nazionale;
		
		System.out.println(" ");
		System.out.println("FEPic_bench: " + FEPic_bench);
		System.out.println("FEPiPDC_bench: " + FEPiPDC_bench);
		System.out.println("FEPi_bench: " + FEPi_bench);
		
		//Calcolo indici prestazione invernale di Benchmark
		EPIsn_inv_bench = FEPi_bench/supnetta;
		EPIsn_inv_bench = (float) (Math.round( EPIsn_inv_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIsn_GG_inv_bench = (float) (EPIsn_inv_bench/(0.024f*GG));
		EPIsn_GG_inv_bench = (float) (Math.round( EPIsn_GG_inv_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIsl_inv_bench = FEPi_bench/suplorda;
		EPIsl_inv_bench = (float) (Math.round( EPIsl_inv_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIsl_GG_inv_bench = (float) (EPIsl_inv_bench/(0.024f*GG));
		EPIsl_GG_inv_bench = (float) (Math.round( EPIsl_GG_inv_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIvl_inv_bench = FEPi_bench/vollordo;
		EPIvl_inv_bench = (float) (Math.round( EPIvl_inv_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		EPIvl_GG_inv_bench = (float) (EPIvl_inv_bench/(0.024f*GG));
		EPIvl_GG_inv_bench = (float) (Math.round( EPIvl_GG_inv_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
		
		
		
		//Calcolo indici prestazione Estivi di Bechmark
  		EPEsn_bench = FEP_estivo_bench/supnetta;
  		EPEsn_bench = (float) (Math.round( EPEsn_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
  		
  		EPEsn_GG_bench = (float) (EPEsn_bench/(0.024f*GGe));
  		EPEsn_GG_bench = (float) (Math.round( EPEsn_GG_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
  		
  		EPEsl_bench = FEP_estivo_bench/suplorda;
  		EPEsl_bench = (float) (Math.round( EPEsl_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
  		
  		EPEsl_GG_bench = (float) (EPEsl_bench/(0.024f*GGe));
  		EPEsl_GG_bench = (float) (Math.round( EPEsl_GG_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
  		
  		EPEvl_bench = FEP_estivo_bench/vollordo;
  		EPEvl_bench = (float) (Math.round( EPEvl_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
  		
  		EPEvl_GG_bench= (float) (EPEvl_bench/(0.024f*GGe));
  		EPEvl_GG_bench = (float) (Math.round( EPEvl_GG_bench * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
	

		try 
		{
			sql = "DELETE FROM smee_epi_prestazioni_energetiche WHERE idImmobile =?";
			
			this.jdbcTemplate.update(sql, Integer.parseInt(modulo_epi.getIdImmobile()));
			
			sql = "INSERT INTO smee_epi_prestazioni_energetiche (idImmobile, "+
				  "coefficientescambiotermico, coefficientescambioventilazione, "+
				  "apportigratuitiinterni, apportisolari,fabbisognoenergiatermica, "+
				  "rendimentoglobale, episuperficieutile,episuperficielorda, "+
				  "epivolumenetto, epivolumelordo,benchmarkscambiotermico, "+
				  "coefficientescambiotermico_estivo, coefficientescambioventilazione_estivo, "+
				  "apportigratuitiinterni_estivo, apportisolari_estivo,fabbisognoenergiatermica_estivo, "+
				  "rendimentoglobale_PdC, rendimentoglobale_estivo, episuperficieutile_GG, epesuperficieutile, "+
				  "epesuperficieutile_GGe, episuperficielorda_GG, epesuperficielorda, epesuperficielorda_GGe, "+
				  "epivolumenetto_GG, epivolumelordo_GG, epevolumelordo, epevolumelordo_GGe, epillinterna, consumo_illesterna, consumo_illinterna"+
				  ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
			
			this.jdbcTemplate.update(sql, Integer.parseInt(modulo_epi.getIdImmobile()), HT_inv, HV_inv, Qi_inv, Qs_inv, Qh_inv, ETAg_caldaie, EPIsn_inv, EPIsl_inv, EPIvn_inv, EPIvl_inv, HT_bench,HT_est, HV_est, Qi_est, Qs_est, Qh_est, ETAg_PdC, ETAg_estivo, EPIsn_GG_inv, EPEsn, EPEsn_GG, EPIsl_GG_inv, EPEsl, EPEsl_GG, EPIvn_GG_inv,EPIvl_GG_inv, EPEvl, EPEvl_GG, EPILL, kWhillumesterna, kWhilluminterna);

			sql = "DELETE FROM smee_epi_benchmark WHERE idImmobile =?";
			//System.out.println("==== smee_epi_benchmark ===="+sql);
			
			this.jdbcTemplate.update(sql, Integer.parseInt(modulo_epi.getIdImmobile()));
			
			//EPI bench
			sql = "INSERT INTO smee_epi_benchmark (idImmobile,idBenchmark, "+
					  "epiSn_bench, epiSn_GG_bench, epiSl_bench, epiSl_GG_bench,epiVl_bench, epiVl_GG_bench, "+
					  "coefficientescambiotermico_bench, coefficientescambioventilazione_bench, "+
					  "fabbisognoenergiatermica_bench, "+
					  "epeSn_bench, epeSn_GGe_bench, epeSl_bench, epeSl_GGe_bench,epeVl_bench, epeVl_GGe_bench, "+
					  "coefficientescambiotermico_estivo_bench, coefficientescambioventilazione_estivo_bench, "+
					  "fabbisognoenergiatermica_estivo_bench "+
					  ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
			
			//System.out.println("==== smee_epi_benchmark ===="+sql);
			this.jdbcTemplate.update(sql, Integer.parseInt(modulo_epi.getIdImmobile()),2,  EPIsn_inv_bench, EPIsn_GG_inv_bench, EPIsl_inv_bench, EPIsl_GG_inv_bench,EPIvl_inv_bench, EPIvl_GG_inv_bench, HT_bench, HV_inv_bench, Qh_inv_bench, EPEsn_bench, EPEsn_GG_bench, EPEsl_bench, EPEsl_GG_bench,EPEvl_bench, EPEvl_GG_bench, HT_bench, HV_est_bench, Qh_est_bench);
		
		} 
		
		catch (Exception e) {
			    System.out.println(" CalcolaIndiciPerfomance::Query non eseguita " + e);	    
		}		

	}

	
	public void AggiornaModuloEpi(ModuloEpi edificio) {
		
		System.out.println("==== AggiornaDatiEdificio new request ====");
	
		String idImmobile = edificio.getIdImmobile();

		Irradianza irradianza_estiva = edificio.getIrradianza_estiva();
		Irradianza irradianza_invernale = edificio.getIrradianza_invernale();
		DatiGenerali datigenerali = this.getDatiGenerali();
		
		DatiEdificio datiedificio=edificio.getDatiedificio();
		
		float rendimentonazionale = datigenerali.getRendimentoNazionale();
		float quotaCaldaie = datiedificio.getQuotaCaldaie();
		float quotaPdC = datiedificio.getQuotaPdC();
		
		
		List<Supesterneopache> supesterneopache=edificio.getSupesterneopache();
		List<ComponentiTrasparenti> componentitrasparenti=edificio.getComponentitrasparenti();
				
		
		boolean update = false;
		Integer id_locazione = -1;
		Integer id_moduloepi = -1;
		Integer id_moduloepi_old = -1;
		
		
		try{
			
			try{
				sql = "SELECT  idmodulo_epi " +
						 "FROM smee_epi_moduloepi " +
						 "where idImmobile='" + idImmobile + "'";
				
				id_moduloepi_old = this.jdbcTemplate.queryForObject(sql, Integer.class);
			}catch(Exception e){}
				
			
			
				if(id_moduloepi_old>-1){
					System.out.println("==== Modulo EPI gi� esistente ==== ");
					update = true;
				}
			
				
//					if(update)
//						this.EliminaModuloEpi(idImmobile, id_moduloepi);
													
					
					
					sql ="INSERT INTO smee_epi_moduloepi (idImmobile, irradianza_N, irradianza_NE, irradianza_E, irradianza_SE, irradianza_S, irradianza_SO, irradianza_O, irradianza_NO, " + 
									  "superficie_lorda, superficie_netta, volume_lordo, volume_netto, altezza, ricambio_aria, apporto_interno, etae, etarg, etad, etagn, " +
									  "superficie_disperdente_lorda, ricambio_aria_est,  apporto_interno_est, irradianza_N_est, irradianza_NE_est, irradianza_E_est, " +
									  " irradianza_SE_est, irradianza_S_est, irradianza_SO_est, irradianza_O_est, irradianza_NO_est, etae_PdC, etarg_PdC, etad_PdC, etagn_PdC, " + 
									  " potenza_lux_interna, oremedie_lux_interna, potenza_lux_esterna, oremedie_lux_esterna, etae_estivo, etarg_estivo, etad_estivo, etagn_estivo, " +
									  " rendimento_elettrico_nazionale, quotaCaldaie, quotaPdC, eta_bench_estivo, eta_bench_PdC, eta_bench_C, potenza_PdC, potenza_C, potenza_estivo, potenza_media_PdC, potenza_media_C, potenza_media_estivo, tipologiadistribuzione_caldaie, tipologiadistribuzione_pdc, tipologiadistribuzione_estivo, tipologiascambio_pdc, tipologiascambio_estivo, numeropiani, superficielordapiano, superficienettapiano) " +
									  "VALUES ('"+ idImmobile +"','"+ irradianza_invernale.getNord()+"','"+irradianza_invernale.getNordest()+"','"+irradianza_invernale.getEst()+"','"+irradianza_invernale.getSudest()+"','"+irradianza_invernale.getSud()+"','"+irradianza_invernale.getSudovest()+"','"+irradianza_invernale.getOvest()+"','"+irradianza_invernale.getNordovest()+
									  "','"+ datiedificio.getSuperficielorda() +"','"+datiedificio.getSuperficienetta()+"','"+datiedificio.getVolumelordo()+"','"+datiedificio.getVolumenetto()+"','"+datiedificio.getAltezza()+"','"+datiedificio.getRicambio_aria()+"','"+datiedificio.getApporto_interno()+"','"+ datiedificio.getEtae_caldaia() +"', '"+ datiedificio.getEtarg_caldaia()+"', '"+ datiedificio.getEtad_caldaia() +"', '"+ datiedificio.getEtagn_caldaia()+ "', " +
									  " '" + datiedificio.getSuperficiedisperdentelorda() + "', '" + datiedificio.getRicambio_aria_est() + "', '" +  datiedificio.getApporto_interno_est()+ "', '" + irradianza_estiva.getNord() + "', '" + irradianza_estiva.getNordest() + "', '" + irradianza_estiva.getEst() + 
									  "', '" + irradianza_estiva.getSudest() + "', '" + irradianza_estiva.getSud() + "', '" + irradianza_estiva.getSudovest() + "', '" + irradianza_estiva.getOvest() + "', '" +  irradianza_estiva.getNordovest() + "', '" + datiedificio.getEtae_PdC() + "', '"+ datiedificio.getEtarg_PdC() + "', '" + datiedificio.getEtad_PdC() + "', '" + datiedificio.getEtagn_PdC() + "', '" +
									  datiedificio.getPotenzaLux_Interna() + "', '" + datiedificio.getOremedieLux_Interna() + "', '" + datiedificio.getPotenzaLux_Esterna() + "', '" + datiedificio.getOremedieLux_Esterna() + "', '" + datiedificio.getEtae_estivo() + "', '" + datiedificio.getEtarg_estivo() + "', '" + datiedificio.getEtad_estivo() + "', '" + datiedificio.getEtagn_estivo() + "', '" + rendimentonazionale + "', '" + quotaCaldaie + "', '" + quotaPdC + "', '" + 
									  datiedificio.getEta_bench_estivo() + "', '" + datiedificio.getEta_bench_PdC() + "', '" + datiedificio.getEta_bench_C() + "', '" + datiedificio.getPotenza_PdC() + "', '" + datiedificio.getPotenza_Caldaia() + "', '" + datiedificio.getPotenza_Climatizzazione() + "', '" + datiedificio.getPotenza_media_PdC() + "', '" + datiedificio.getPotenza_media_Caldaia() + "', '" +
									  datiedificio.getPotenza_media_Climatizzazione() + "', '" + datiedificio.getTipologiadistribuzione_caldaie() + "', '" + datiedificio.getTipologiadistribuzione_pdc() + "', '" + datiedificio.getTipologiadistribuzione_estivo() + "', '" + datiedificio.getTipologiascambio_pdc() + "', '" + datiedificio.getTipologiascambio_estivo() + "', '" + datiedificio.getNumeropiani() + "', '" + datiedificio.getSuperficielordapiano() + "', '" + datiedificio.getSuperficienettapiano() + "')";

					

										
					KeyHolder holder = new GeneratedKeyHolder();

					this.jdbcTemplate.update(new PreparedStatementCreator() {           

					                @Override
					                public java.sql.PreparedStatement createPreparedStatement(Connection connection)
					                        throws SQLException {
					                    java.sql.PreparedStatement ps = connection.prepareStatement(sql , Statement.RETURN_GENERATED_KEYS);
					                    return ps;
					                }
					            }, holder);

					id_moduloepi = holder.getKey().intValue();
							
					System.out.println("==== Result Insert modulo epi id = " + id_moduloepi);
					
					if(update)
						this.EliminaModuloEpi(idImmobile, id_moduloepi_old);
						
						
						if(supesterneopache!=null)
							for (int i=0; i<supesterneopache.size(); i++) {
								Supesterneopache seo=supesterneopache.get(i);			
								sql ="INSERT INTO smee_epi_superfici_esterne_opache (idmodulo_epi, descrizione, tipo_superficie, materiale, esposizione, superficie_seo, spessore, trasmittanza_seo, ambiente_confinante, fattore_correzione, massa, calore_specifico) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
								this.jdbcTemplate.update(sql, id_moduloepi, seo.getDescrizione(), seo.getTiposuperficie(), seo.getMateriale(), seo.getEsposizione(), Double.parseDouble(seo.getSuperficieseo()), Double.parseDouble(seo.getSpessore()), Double.parseDouble(seo.getTrasmittanzaeop()), seo.getAmbienteconfinante(), Double.parseDouble(seo.getFattorecorrezione()), Double.parseDouble(seo.getMassa()), Double.parseDouble(seo.getCalorespecifico()) );           				
							}
									
						
						if(componentitrasparenti!=null)
							for (int i=0; i<componentitrasparenti.size(); i++) {
								ComponentiTrasparenti ct=componentitrasparenti.get(i);
								sql ="INSERT INTO smee_epi_componenti_trasparenti (idmodulo_epi, note, tipo_vetro, tipo_telaio, tipo_gas, esposizione_infissi, sup_infisso_si, trasmittanza_usi, flagsi ) VALUES (?,?,?,?,?,?,?,?,?);";
								this.jdbcTemplate.update(sql, id_moduloepi, ct.getNote(), ct.getTipovetro(), ct.getTipotelaio(), ct.getTipogas(), ct.getEsposizioneinfissi(), Double.parseDouble(ct.getSupinfissosi()), Double.parseDouble(ct.getTrasmittanzausi()), ct.getFlagsi() );   								
								
							}

					CalcolaPrestazioniEnergetiche(Integer.parseInt(idImmobile));
								
	
				
		}catch(Exception e)
		{
			 System.out.println("AggiornaDatiEdificio::Query non eseguita " + e);
			 System.out.println(sql);
		}
					
	}
	
	public void EliminaModuloEpi(String idImmobile, Integer idModuloEpi) {
		System.out.println("==== EliminaDatiEdificio ====");			
		String sql = "";
		try {											
			 sql ="DELETE FROM smee_epi_superfici_esterne_opache WHERE idmodulo_epi='" + idModuloEpi + "'";
			 this.jdbcTemplate.update(sql);
			
			 sql ="DELETE FROM smee_epi_componenti_trasparenti WHERE idmodulo_epi='" + idModuloEpi + "'";
			 this.jdbcTemplate.update(sql);
		
			 sql ="DELETE FROM smee_epi_moduloepi WHERE idmodulo_epi='" + idModuloEpi + "'";
			 this.jdbcTemplate.update(sql);

			 System.out.println("==== Eliminato edificio con idmodulo_epi = " + idModuloEpi);												 										
							
		}catch (Exception e) {
			    System.out.println("EliminaDatiEdificio::Query non eseguita " + e);  
		}		
	}

	@Override
	public List<Map<String, Object>> getData() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Map<String, Object>> getCOP() {
		List<Map<String, Object>> cop = new ArrayList<Map<String, Object>>();
		String sql = "SELECT * FROM smee_epi_cop , smee_immobili pe join smee_comuni c on pe.idComune=c.idComune WHERE 1=1 "+ filter_string;

		try{
			cop = this.jdbcTemplate.queryForList(sql);		
		}
		catch(Exception e)
		{
			 System.out.println("JdbcMOduloEpiDAO::getCOP()::Query non eseguita " + e);		
		}
		return cop;
	}

	public List<Map<String, Object>> getEpiValue() {
		List<Map<String, Object>> indici_epi = new ArrayList<Map<String, Object>>();
		String sql = "SELECT pe.idImmobile, pe.episuperficieutile as EPI_SN, eb.epiSn_bench as EPI_SN_Bench, "+
				   "pe.episuperficieutile_GG as EPI_SN_GG, eb.epiSn_GG_bench as EPI_SN_GG_Bench, "+
			       "pe.episuperficielorda as EPI_SL, eb.epiSl_bench as EPI_SL_Bench, "+
			       "pe.episuperficielorda_GG as EPI_SL_GG, eb.epiSl_GG_bench as EPI_SL_GG_Bench, "+
			       "pe.epivolumelordo as EPI_VL, eb.epiVl_bench as EPI_VL_Bench, "+
			       "pe.epivolumelordo_GG as EPI_VL_GG, eb.epiVl_GG_bench as EPI_VL_GG_Bench, "+
			       "pe.epesuperficielorda as EPE_SL, eb.epeSl_bench as EPE_SL_Bench, "+
			       "pe.epesuperficielorda_GGe as EPE_SL_GGe, eb.epeSl_GGe_bench as EPE_SL_GGe_Bench, "+
			       "pe.epesuperficieutile as EPE_SN, eb.epeSn_bench as EPE_SN_Bench, "+
				   "pe.epesuperficieutile_GGe as EPE_SN_GGe, eb.epeSn_GGE_bench as EPE_SN_GGe_Bench, "+
			       "pe.epevolumelordo as EPE_VL, eb.epeVl_bench as EPE_VL_Bench, "+
			       "pe.epevolumelordo_GGe as EPE_VL_GGe, eb.epeVl_GGe_bench as EPE_VL_GGe_Bench "+
				   "FROM smee_epi_prestazioni_energetiche pe join smee_epi_benchmark eb "+
				   "on pe.idImmobile=eb.idImmobile where 1=1 "+ filter_string;

		try{
			indici_epi = this.jdbcTemplate.queryForList(sql);
			System.out.println(sql);		
		}
		catch(Exception e)
		{
			 System.out.println("JdbcMOduloEpiDAO::getEpiValue()::Query non eseguita " + e);		
		}

		return indici_epi;
	}
	
	
}