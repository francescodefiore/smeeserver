package it.smee.power.dao;

import it.smee.jboxlib.JBoxDAO;

import javax.sql.DataSource;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;



public class JdbcBaseloadsDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;	
	private String filter_string = "";
	
	private String name = "";

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Map<String, Object>> getData() {
		List< Map<String, Object> > profiles = null;
		
		try {					
					String sql = "SELECT * " +
							     "FROM smee_baseload_pod " +
							     "WHERE 1=1 "+ filter_string;

					System.out.println("query " + sql);
					
					profiles = this.jdbcTemplate.queryForList(sql);
		}
			
		catch (Exception e) {
			    System.out.println(" BudgetEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return profiles;
	}

	@Override
	public void resetFilters() {
		filter_string = "";		
	}
	
	
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = " && (pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		s = s.concat(")");
		return s;
	}
	
	
	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		switch(filter_name)
		{
				
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;				
		}
		

	}


	@Override
	public void print() {
		System.out.println("--- JdbcBaseloadsDAO");		
	}

	
	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) 
	{
	}
	


	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
		
	}
}
