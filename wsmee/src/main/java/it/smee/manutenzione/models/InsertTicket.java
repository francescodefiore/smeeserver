package it.smee.manutenzione.models;

public class InsertTicket {

	private String codiceImmobile;
	private String denominazioneUp;
	private String descrizione;
	private int categoria;
	private String dataRichiesta;
	private String tipoTicket;
	private int idImpComp;
	private double co2Risparmiata;
	private int priorita;
	private int idImmobile;
	private String provincia;
	private String prioritaStr;
	
	public String getCodiceImmobile() {
		return codiceImmobile;
	}
	public void setCodiceImmobile(String codiceImmobile) {
		this.codiceImmobile = codiceImmobile;
	}
	public String getDenominazioneUp() {
		return denominazioneUp;
	}
	public void setDenominazioneUp(String denominazioneUp) {
		this.denominazioneUp = denominazioneUp;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getDataRichiesta() {
		return dataRichiesta;
	}
	public void setDataRichiesta(String dataRichiesta) {
		this.dataRichiesta = dataRichiesta;
	}
	
	public int getCategoria() {
		return categoria;
	}
	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}
	public String getTipoTicket() {
		return tipoTicket;
	}
	public void setTipoTicket(String tipoTicket) {
		this.tipoTicket = tipoTicket;
	}
	
	public int getIdImpComp() {
		return idImpComp;
	}
	public void setIdImpComp(int idImpComp) {
		this.idImpComp = idImpComp;
	}
	public double getCo2Risparmiata() {
		return co2Risparmiata;
	}
	public void setCo2Risparmiata(double co2Risparmiata) {
		this.co2Risparmiata = co2Risparmiata;
	}
	public int getPriorita() {
		return priorita;
	}
	public void setPriorita(int priorita) {
		this.priorita = priorita;
	}
	public int getIdImmobile() {
		return idImmobile;
	}
	public void setIdImmobile(int idImmobile) {
		this.idImmobile = idImmobile;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getPrioritaStr() {
		return prioritaStr;
	}
	public void setPrioritaStr(String prioritaStr) {
		this.prioritaStr = prioritaStr;
	}
	
}
