package it.smee.manutenzione.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import it.smee.epi.dao.JdbcModuloEpiDAO;
import it.smee.epi.models.ComponentiTrasparenti;
import it.smee.epi.models.DatiEdificio;
import it.smee.epi.models.DatiGenerali;
import it.smee.epi.models.ModuloEpi;
import it.smee.epi.models.Supesterneopache;
import it.smee.jboxlib.JBoxDAO;
import it.smee.knapsack.Item;
import it.smee.knapsack.ZeroOneKnapsack;
import it.smee.manutenzione.models.Co2Manutenzione;
import it.smee.manutenzione.models.Co2Ticket;
import it.smee.manutenzione.models.InsertTicket;
import it.smee.manutenzione.models.Interventi;
import it.smee.manutenzione.models.Scenari;
import it.smee.manutenzione.models.Simulazioni;
import it.smee.manutenzione.models.Ticket;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import static java.lang.String.format;

import java.util.function.*;


class MyPredicate<T> implements Predicate<Simulazioni>{
  Simulazioni var1;
  public boolean test(Simulazioni var){
  if(var1.getTipologiaIntervento() == var.getTipologiaIntervento()){
   return true;
  }
  return false;
  }
}

public class JdbcManutenzioneDAO implements JBoxDAO {
	private String filter_string = "";
	private JdbcTemplate jdbcTemplate;
	private static final ApplicationContext context =	new ClassPathXmlApplicationContext("Spring-Module.xml");
	private static final JBoxDAO epiDAO = (JBoxDAO) context.getBean("moduloepiDAO");

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Map<String, Object>> getData() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Co2Ticket> getManutenzioneTicketCO2(int anno) {
		String storeprocedure ="{call CalcoloC02RisparmiataTicket("+anno+")}";
		List<Co2Ticket> co2_ticket = null;
		
		co2_ticket  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Co2Ticket>(Co2Ticket.class));
		System.out.println("Manutenzione::getManutenzioneTicketCO2 " + storeprocedure);
		
		return co2_ticket;
	}
	

	public List<Interventi> getManutenzioneInterventi(int idImmobile) {
		String storeprocedure ="{call ManutenzioneOreFunzionamento('"+idImmobile+"',0.95)}";
		List<Interventi> manutenzione = null;
		
		manutenzione  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Interventi>(Interventi.class));
		System.out.println("Manutenzione::getManutenzioneInterventi " + storeprocedure);
		
		return manutenzione;
	}
	
	
	public List<Interventi> getManutenzioneAllInterventi() {
		String storeprocedure ="{call ManutenzioneOreFunzionamento('*',0.95)}";
		List<Interventi> manutenzione = null;
		
		manutenzione  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Interventi>(Interventi.class));
		System.out.println("Manutenzione::getManutenzioneAllInterventi " + storeprocedure);
		
		return manutenzione;
	}

	/*
	public List<Scenari> getManutenzioneScenari(Integer num_scenari, Integer idimmobile) {
		String storeprocedure ="{call ManutenzioneStraordinaria(0,"+num_scenari+","+idimmobile+")}";
		List<Scenari> scenari = null;
		
		scenari  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Scenari>(Scenari.class));
		System.out.println("Manutenzione::getManutenzioneScenari " + storeprocedure);
		
		return scenari;
	}
	*/
	
	public List<Ticket> getManutenzioneTicket(String time) {
		List<Ticket> ticket = null;
		
		String sql ="SELECT * FROM smee_ticket_storici where dataInizioIntervento != '0000-00-00' and dataRichiesta >= '" + time + "'";
		
		ticket  = this.jdbcTemplate.query(sql,	new BeanPropertyRowMapper<Ticket>(Ticket.class));
		System.out.println("Manutenzione::getManutenzioneTicket " + sql);
		
		return ticket;
	}
	
	
	public List<Map<String, Object>> getTipoInterventi() {
		List<Map<String, Object>> interventi = new ArrayList<Map<String, Object>>();
		String sql = "SELECT * FROM smee_intervento" ;
		
		try{
			interventi = this.jdbcTemplate.queryForList(sql);		
		}
		catch(Exception e)
		{
			 System.out.println("JdbcManutenzioneDAO::getTipoInterventi()::Query non eseguita " + e);		
		}
		return interventi;
	}
	

	public List<Scenari> getScenariImmobile(int idImmobile) {
		
		List<Scenari> scenari = null;
		List<Simulazioni> simulazioni = null;
		String codice = null;
		
		String sqlCodice = "SELECT codice FROM smee_immobili WHERE idImmobile = " + idImmobile;
		List<Map<String,Object>> resultsCodice = this.jdbcTemplate.queryForList(sqlCodice);
		for (Map<String, Object> m : resultsCodice){
			codice = (String) m.get("codice");
		}
		
		String sql = "SELECT gradigiorno as GGi, gradigiornoestivi as GGe "+
			  "FROM smee_epi_riscaldamento_ore o join smee_comuni c on o.zona=c.zona "+
			  "join smee_immobili i on c.idComune=i.idComune where i.idImmobile='"+idImmobile+"'";
		int GGi = 0;
		int GGe = 0;
		List<Map<String,Object>> results = this.jdbcTemplate.queryForList(sql);
		for (Map<String, Object> m : results){
			GGi = (Integer) m.get("GGi");
			GGe =  (Integer) m.get("GGe");
		}
		
		ModuloEpi modulo_epi = ((JdbcModuloEpiDAO)epiDAO).getModuloEpi(idImmobile);
		List<Supesterneopache> supesterneopache=modulo_epi.getSupesterneopache();
		List<ComponentiTrasparenti> componentitrasparenti=modulo_epi.getComponentitrasparenti();
		DatiEdificio datiedificio = modulo_epi.getDatiedificio();
		float superficie_lorda= datiedificio.getSuperficielorda();//datiedificio.getSuperficiedisperdentelorda();
		float quotaCaldaie=datiedificio.getQuotaCaldaie();
		float quotaPdC=datiedificio.getQuotaPdC();
		
		DatiGenerali datigenerali = ((JdbcModuloEpiDAO)epiDAO).getDatiGenerali();
		float fx = datigenerali.getCoefficienteFx();
		float rendimento_nazionale = (datigenerali.getRendimentoNazionale());
		
		List<Map<String, Object>> epiattuale = new ArrayList<Map<String, Object>>();
		sql = "SELECT * FROM smee_epi_prestazioni_energetiche where idImmobile = " + idImmobile;		
	    epiattuale = this.jdbcTemplate.queryForList(sql);
	    float Etag_caldaia=0; 
	    float ETAg_PdC=0;
	    float HV = 0;
		
	    float Qi = 0;
		float Qs = 0;
		float episuperficielorda=0;
		float epesuperficielorda=0;
		float epillinterna=0;
		float HT_inv = 0;
		float HT_estivo = 0;
		float HV_est =0;
		float Qi_est = 0;
		float Qs_est = 0;
		float ETAg_estivo = 0;
		
		for (Map<String, Object> e : epiattuale){
			episuperficielorda = (float) e.get("episuperficielorda");
			epesuperficielorda = (float) e.get("epesuperficielorda");
			epillinterna = (float) e.get("epillinterna");
			
			Etag_caldaia = (float) e.get("rendimentoglobale");
			ETAg_PdC =  (float) e.get("rendimentoglobale_PdC");
			HV = (float) e.get("coefficientescambioventilazione");
			Qi = (float) e.get("apportigratuitiinterni");
			Qs = (float) e.get("apportisolari");
			HT_inv = (float) e.get("coefficientescambiotermico");
			HT_estivo = (float) e.get("coefficientescambiotermico_estivo");
			HV_est = (float) e.get("coefficientescambioventilazione_estivo");
			Qi_est = (float) e.get("apportigratuitiinterni_estivo");
			Qs_est = (float) e.get("apportisolari_estivo");
			ETAg_estivo = (float) e.get("rendimentoglobale_estivo");
		}
		//System.out.println("Manutenzione::epesuperficielorda "+epesuperficielorda+" ::episuperficielorda " + episuperficielorda);
	
		
		//PRIMA InterventiImpiantiLuce		
	    String storeprocedure ="{call InterventiImpiantiLuce(" + idImmobile + ")}";	
	    simulazioni  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Simulazioni>(Simulazioni.class));
	    //codice = simulazioni.get(0).getCodiceImmobile();

		for (int i=0; i<simulazioni.size(); i++) {
			Simulazioni sm = simulazioni.get(i);
			//EPI SIMULATO ILLUMINAZIONE
		    float EPILL = 0;
			int cod_int = sm.getCodiceIntervento();
			float risparmio_lux_int= sm.getParametro();
							
			EPILL = epillinterna - (epillinterna*0.01f*risparmio_lux_int);
			EPILL = (float) (Math.round( EPILL * Math.pow( 10, 3 ) )/Math.pow( 10, 3 ));
			
			float episimulato = epesuperficielorda + episuperficielorda + EPILL;
			episimulato = (float) (Math.round( episimulato * Math.pow( 10, 3 ) )/Math.pow( 10, 3 ));
			
			sql = "UPDATE smee_interventi_temp "+
				  "SET epiSimulato = " + episimulato + " WHERE codiceIntervento = " +cod_int +" and tipologiaIntervento = 6;" ;
			this.jdbcTemplate.update(sql);
				
			//System.out.println("Manutenzione::tipo_intervento  "+sm.getTipologiaIntervento()+"  ::epiAttuale " + sm.getEpiAttuale() + " ::epiSimulato " + episimulato);

		}
		
		
				//SECONDA InterventiInvolucroOpaco
				storeprocedure ="{call InterventiInvolucroOpaco(" + idImmobile + ")}";
				simulazioni  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Simulazioni>(Simulazioni.class));
				
				for (int i=0; i<simulazioni.size(); i++) {
					Simulazioni sm = simulazioni.get(i);
					int cod_int = sm.getCodiceIntervento();
					int tipo_intervento = sm.getTipologiaIntervento();
					float trasmittanza = sm.getParametro();
					//EPI SIMULATO Involucro opaco: pareti / divisioni verticali esterne
				    float Ht_superficiopache=0;
					float Ht_serramenti=0;
					
					
					if ((tipo_intervento == 1) || (tipo_intervento == 2) || (tipo_intervento == 9)) {
						double supseo = 0;
						
						//Coefficiente scambio termico
						for (int j=0; j<supesterneopache.size(); j++) {
							Supesterneopache seo = supesterneopache.get(j);
							supseo = Double.parseDouble(seo.getSuperficieseo());
							double btr = Double.parseDouble(seo.getFattorecorrezione());
							if (btr == 0) btr=1;
							Ht_superficiopache += supseo*trasmittanza*btr;
							
							//System.out.println("trasmittanza: " + trasmittanza);
						}
						
						for (int j=0; j<componentitrasparenti.size(); j++) {
							ComponentiTrasparenti ct = componentitrasparenti.get(j);
							double supct = Double.parseDouble(ct.getSupinfissosi());
							double Utct = Double.parseDouble(ct.getTrasmittanzausi());	
							Ht_serramenti += supct*Utct;	
						}
						
						/*System.out.println("");
						System.out.println("tipo_intervento: " + tipo_intervento 
											+ " Ht_superficiopache: " + Ht_superficiopache
											+ " Ht_serramenti: " + Ht_serramenti);*/

					}
				
					
					float HT = Ht_superficiopache + Ht_serramenti;
					HT = (float) (Math.round( HT * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
					
					float Qh=(0.024f*GGi*(HT+HV)-fx*(Qs+Qi));
					Qh = (float) (Math.round( Qh * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));	
						
					//float FEPic = (Qh/Etag_caldaia)*(quotaCaldaie/100.f);
					float FEPic = 0;
					if (quotaCaldaie>0) {
						FEPic = (Qh/Etag_caldaia)*(quotaCaldaie/100.f);
					}
					
					//float FEPiPDC = ((Qh/ETAg_PdC)*(quotaPdC/100.f))/rendimento_nazionale;
					float FEPiPDC = 0;
					if (quotaPdC>0) {
						FEPiPDC = ((Qh/ETAg_PdC)*(quotaPdC/100.f))/rendimento_nazionale;
					}
					float FEPi = FEPic + FEPiPDC;	
					float EPIsl = FEPi/superficie_lorda;
					EPIsl = (float) (Math.round( EPIsl * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
					
					float Qh_est=(0.024f*GGe*(HT+HV_est)+fx*(Qs_est+Qi_est));
					Qh_est = (float) (Math.round( Qh_est * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));	
					
					float FEP_estivo = (Qh_est/ETAg_estivo)/rendimento_nazionale;
					float EPEsl = FEP_estivo/superficie_lorda;
					EPEsl = (float) (Math.round( EPEsl * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
					
					float episimulato = EPIsl + EPEsl + epillinterna;
					episimulato = (float) (Math.round( episimulato * Math.pow( 10, 3 ) )/Math.pow( 10, 3 ));
					
					System.out.println("tipo_intervento: " + tipo_intervento);
					System.out.println("trasmittanza: " + trasmittanza);
					System.out.println("HT: " + HT);
					System.out.println("Qh: " + Qh);
					System.out.println("ETAg_PdC: " + ETAg_PdC);
					System.out.println("quotaPdC: " + quotaPdC);
					System.out.println("FEPic: " + FEPic);
					System.out.println("FEPiPDC: " + FEPiPDC);
					System.out.println("FEPi: " + FEPi);
					System.out.println("EPIsl: " + EPIsl);
					System.out.println("EPEsl: " + EPEsl);
					System.out.println("epillinterna: " + epillinterna);
					System.out.println("episimulato: " + episimulato);
					System.out.println("");
					
					sql = "UPDATE smee_interventi_temp "+
							  "SET epiSimulato = " + episimulato + " WHERE codiceIntervento = " +cod_int +" and tipologiaIntervento = " + tipo_intervento;
					this.jdbcTemplate.update(sql);
					
					//System.out.println("Manutenzione::tipo_intervento "+tipo_intervento+" ::epiAttuale " + sm.getEpiAttuale() + " ::epiSimulato " + episimulato);

				}
				
				//TERZA InterventiInvolucroOpaco
				storeprocedure ="{call InterventiInvolucroTrasparenti(" + idImmobile + ")}";
				simulazioni  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Simulazioni>(Simulazioni.class));
				
				for (int i=0; i<simulazioni.size(); i++) {
					Simulazioni sm = simulazioni.get(i);
					int cod_int = sm.getCodiceIntervento();
					int tipo_intervento = sm.getTipologiaIntervento();
					float trasmittanza = sm.getParametro();
					//EPI SIMULATO Involucro opaco: pareti / divisioni verticali esterne
				    float Ht_superficiopache=0;
					float Ht_serramenti=0;
					
					
					if (tipo_intervento == 3){
						
						double supseo = 0;
						double Ueoc = 0;
						double btr = 0;

						//Coefficiente scambio termico
						for (int j=0; j<supesterneopache.size(); j++) {
							Supesterneopache seo = supesterneopache.get(j);
							supseo = Double.parseDouble(seo.getSuperficieseo());
							Ueoc = Double.parseDouble(seo.getTrasmittanzaeop());
							btr = Double.parseDouble(seo.getFattorecorrezione());
							if (btr == 0) btr=1;
							Ht_superficiopache += supseo*Ueoc*btr;	
						}
						
						for (int j=0; j<componentitrasparenti.size(); j++) {
							ComponentiTrasparenti ct = componentitrasparenti.get(j);
							double supct = Double.parseDouble(ct.getSupinfissosi());		
							Ht_serramenti += supct*trasmittanza;	
						}
					}
				
					
					float HT = Ht_superficiopache + Ht_serramenti;
					HT = (float) (Math.round( HT * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
					
					float Qh=(0.024f*GGi*(HT+HV)-fx*(Qs+Qi));
					Qh = (float) (Math.round( Qh * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));	
						
					//float FEPic = (Qh/Etag_caldaia)*(quotaCaldaie/100.f);
					float FEPic = 0;
					if (quotaCaldaie>0) {
						FEPic = (Qh/Etag_caldaia)*(quotaCaldaie/100.f);
					}
					
					//float FEPiPDC = ((Qh/ETAg_PdC)*(quotaPdC/100.f))/rendimento_nazionale;
					float FEPiPDC = 0;
					if (quotaPdC>0) {
						FEPiPDC = ((Qh/ETAg_PdC)*(quotaPdC/100.f))/rendimento_nazionale;
					}
					float FEPi = FEPic + FEPiPDC;	
					float EPIsl = FEPi/superficie_lorda;
					EPIsl = (float) (Math.round( EPIsl * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
					
					float Qh_est=(0.024f*GGe*(HT+HV_est)-fx*(Qs_est+Qi_est));
					Qh_est = (float) (Math.round( Qh_est * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));	
					
					float EPEsl = FEPi/superficie_lorda;
					EPEsl = (float) (Math.round( EPEsl * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
					
					float episimulato = EPIsl + EPEsl + epillinterna;
					episimulato = (float) (Math.round( episimulato * Math.pow( 10, 3 ) )/Math.pow( 10, 3 ));
					
					//System.out.println("Epi int 3: " + episimulato);
					
					sql = "UPDATE smee_interventi_temp "+
							  "SET epiSimulato = " + episimulato + " WHERE codiceIntervento = " +cod_int +" and tipologiaIntervento = " + tipo_intervento;
					this.jdbcTemplate.update(sql);

				}
				
		//QUARTA InterventiImpiantiClima
		storeprocedure ="{call InterventiImpiantiClima(" + idImmobile + ")}";
		simulazioni  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Simulazioni>(Simulazioni.class));
		
		for (int i=0; i<simulazioni.size(); i++) {
			Simulazioni sm = simulazioni.get(i);
			int tipo_intervento = sm.getTipologiaIntervento();
			//EPI SIMULATO CLIMA
		    int cod_int = sm.getCodiceIntervento();
			float rendimento = sm.getParametro();
			float Qh=0;
			float episimulato = 0;
			if (tipo_intervento == 4) {
				Qh=(0.024f*GGi*(HT_inv+HV)-fx*(Qs+Qi));
				
				//float FEPic = (Qh/rendimento)*(quotaCaldaie/100.f);
				float FEPic = 0;
				if (quotaCaldaie>0) {
					FEPic = (Qh/rendimento)*(quotaCaldaie/100.f);
				}
				
				//float FEPiPDC = ((Qh/rendimento)*(quotaPdC/100.f))/rendimento_nazionale;
				float FEPiPDC = 0;
				if (quotaPdC>0) {
					FEPiPDC = ((Qh/rendimento)*(quotaPdC/100.f))/rendimento_nazionale;
				}
				float FEPi = FEPic + FEPiPDC;
				float EPIsl_inv = FEPi/superficie_lorda;
				EPIsl_inv = (float) (Math.round( EPIsl_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
				
				episimulato = EPIsl_inv + epesuperficielorda + epillinterna;
				
				/*System.out.println("");
				System.out.println("quotaCaldaie 4: " + quotaCaldaie);
				System.out.println("quotaPdC 4: " + quotaPdC);
				System.out.println("rendimento 4: " + rendimento);
				System.out.println("rendimento_nazionale 4: " + rendimento_nazionale);
				System.out.println("FEPic 4: " + FEPic);
				System.out.println("FEPiPDC 4: " + FEPiPDC);
				System.out.println("EPIsl_inv 4: " + EPIsl_inv);
				System.out.println("epesuperficielorda 4: " + epesuperficielorda);
				System.out.println("epillinterna 4: " + epillinterna);
				System.out.println("Epi int 4: " + episimulato);
				System.out.println("");*/
			}
			else if (tipo_intervento == 5) {
				Qh=(0.024f*GGi*(HT_estivo+HV_est)-fx*(Qs_est+Qi_est));
				
				//float FEPic = (Qh/rendimento)*(quotaCaldaie/100.f);
				float FEPic = 0;
				if (quotaCaldaie>0) {
					FEPic = (Qh/rendimento)*(quotaCaldaie/100.f);
				}
				
				//float FEPiPDC = ((Qh/rendimento)*(quotaPdC/100.f))/rendimento_nazionale;
				float FEPiPDC = 0;
				if (quotaPdC>0) {
					FEPiPDC = ((Qh/rendimento)*(quotaPdC/100.f))/rendimento_nazionale;
				}
				float FEPi = FEPic + FEPiPDC;
				float EPEsl_inv = FEPi/superficie_lorda;
				EPEsl_inv = (float) (Math.round( EPEsl_inv * Math.pow( 10, 2 ) )/Math.pow( 10, 2 ));
				
				episimulato = EPEsl_inv + episuperficielorda + epillinterna;
				//System.out.println("Epi int 5: " + episimulato);
			}
			
			episimulato = (float) (Math.round( episimulato * Math.pow( 10, 3 ) )/Math.pow( 10, 3 ));
			
			sql = "UPDATE smee_interventi_temp "+
				  "SET epiSimulato = " + episimulato + " WHERE codiceIntervento = " +cod_int +" and tipologiaIntervento = " + tipo_intervento;
			this.jdbcTemplate.update(sql);
			
			//System.out.println("Manutenzione::tipo_intervento 45 ::epiAttuale " + sm.getEpiAttuale() + " ::epiSimulato " + episimulato);

		}
		
		storeprocedure ="SELECT * FROM smee_interventi_temp where codiceImmobile = '" + codice + "'";
		simulazioni  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Simulazioni>(Simulazioni.class));
		
		
		scenari = new ArrayList<Scenari>();
		float budget_max = 300000; 		
		int i=0;
		MyPredicate<Simulazioni> filter;
		filter = new MyPredicate<> ();
		
		for(float budget=budget_max/10.f; budget<=budget_max; budget += budget_max/10.f, i++)
		{
			ZeroOneKnapsack zok = new ZeroOneKnapsack((int)budget);
			Scenari scenario = new Scenari();
			float risparmio = 0;
			List<Simulazioni> interventi = new ArrayList<Simulazioni>();
			List<Simulazioni> simulazioni_temp = new ArrayList<Simulazioni>();
			List<Simulazioni> interventiFinali = new ArrayList<Simulazioni>();
			simulazioni_temp.addAll(simulazioni);
			
			/*do
			{		

				int cursor = (int)(Math.random() * ((simulazioni_temp.size() - 1) + 1));
				interventi.add(simulazioni_temp.get(cursor));
				System.out.println("cursor: " + cursor);
				
				risparmio = simulazioni_temp.get(0).getEpiAttuale() - simulazioni_temp.get(cursor).getEpiSimulato();
				
				//System.out.println("Zok risparmio: " + risparmio);
				//System.out.println("");
				
				zok.add(Integer.toString(simulazioni_temp.get(cursor).getCodiceIntervento()), (int)simulazioni_temp.get(cursor).getCostoTotale(), (int)risparmio);				
			
				filter.var1 = simulazioni_temp.get(cursor);			
				simulazioni_temp.removeIf(filter);
							
			}while(simulazioni_temp.size() > 0);*/
			
			for(int y=0;y<simulazioni_temp.size();y++)
			{
				interventi.add(simulazioni_temp.get(y));
				risparmio = simulazioni_temp.get(0).getEpiAttuale() - simulazioni_temp.get(y).getEpiSimulato();
				zok.add(Integer.toString(simulazioni_temp.get(y).getCodiceIntervento()), (int)simulazioni_temp.get(y).getCostoTotale(), (int)risparmio);
			}
			
			
			
			List<Item> itemList = zok.calcSolution();
			
			scenario.setIdScenario(i);
			scenario.setCostoTotale(zok.getSolutionWeight());
			scenario.setEpiRisparmio(zok.getProfit());
			scenario.setEpiAttuale(simulazioni.get(0).getEpiAttuale());

			
			
			
			System.out.println("\n Scenario " + i + " Budget: " + budget);
			
			for (Item item : itemList) {
                if (item.getInKnapsack() == 1) {
                	
                    System.out.format(
                        "%1$-23s %2$-3s %3$-5s %4$-15s \n",
                        item.getName(),
                        item.getWeight(), "Euro  ",
                        "(epi risparmiato = " + item.getValue() + ")"
                    );
                    
                    
                }
            }
			
			float mediaPerc = 0;
			for (Simulazioni simulazione : interventi) 
			{
				
				for (Item item : itemList) 
				{
					if(Integer.parseInt(item.getName()) == simulazione.getCodiceIntervento() && item.getInKnapsack() == 1)
					{
						interventiFinali.add(simulazione);
						System.out.println(simulazione.getCodiceIntervento() + " " + item.getInKnapsack());
						//mediaPerc += (item.getValue()*100)/simulazioni.get(0).getEpiAttuale();
						/*System.out.println("getEpiAttuale parziale: " + simulazioni.get(0).getEpiAttuale());
						System.out.println("getEpiSimulato: " + item.getValue());
						System.out.println("mediaPerc parziale: " + ((item.getValue()*100)/simulazioni.get(0).getEpiAttuale()));*/
					}
					
				}
			}
			
			mediaPerc = mediaPerc/simulazioni_temp.size();
			
			//scenario.setEpiRisparmio((mediaPerc*simulazioni.get(0).getEpiAttuale())/100);
			//mediaPerc = 0;
			
			//scenario.setInterventi(simulazioni_temp);		
			scenario.setInterventi(interventiFinali);
			scenari.add(scenario);					
		}
		return scenari;
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{
			case "TIMEINTERVAL":
				filter_string = filter_string.concat( timeFilter(filter_params) );
				break;
		}
		
	}

	private String timeFilter(String[] interval) {
		int i=0;
		String s = " && (dataRichiesta BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'";
		i = i+2;
		while(i<interval.length)
		{
			s = s.concat(" OR dataRichiesta BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'");
			i = i+2;
		}
		s = s.concat(")");
		return s;
	}

	@Override
	public void resetFilters() {
		filter_string = "";
	}
	
	public void insertTickets(List<InsertTicket> tickets)
	{
		String sql = "SELECT cdIntervento FROM main_intervento where cdIntervento like '" + tickets.get(0).getTipoTicket() +"%' order by idIntervento desc limit 1";
		List<Map<String, Object>> codIntervento = new ArrayList<Map<String, Object>>();
		codIntervento = this.jdbcTemplate.queryForList(sql);
		
		String codice = "0";
		for(Map<String, Object> i : codIntervento)
		{
			String app = i.get("cdIntervento").toString();
			codice = app.split(tickets.get(0).getTipoTicket())[1];
		}
		
		int codiceI = Integer.parseInt(codice);
		System.out.println(codiceI);
		
		StringBuilder builderInsert = new StringBuilder("INSERT INTO main_intervento(cdIntervento,codiceImmobile,idCategoria,denominazioneUp,descrizione,dataRichiesta,kgCO2Risparmiati,priorita,idImmobile,idSedeCentrale,prioritaString) VALUES");
		StringBuilder builderUpdate = new StringBuilder("UPDATE smee_impianti_componente set inManutenzione = 1 WHERE id in(");
		for(int i = 0;i<tickets.size();i++)
		{
			if(i!=0)
			{
				builderInsert.append(",");
				builderUpdate.append(",");
				
			}
			
			codiceI++;
			builderInsert.append(format("('%s','%s','%s','%s','%s','%s',%s,%s,%s,'%s','%s')",tickets.get(i).getTipoTicket() + codiceI,tickets.get(i).getCodiceImmobile(),tickets.get(i).getCategoria(),tickets.get(i).getDenominazioneUp(),
																				   tickets.get(i).getDescrizione(),tickets.get(i).getDataRichiesta(),
																				   tickets.get(i).getCo2Risparmiata(),tickets.get(i).getPriorita(),
																				   tickets.get(i).getIdImmobile(),tickets.get(i).getProvincia(),
																				   tickets.get(i).getPrioritaStr()));
			builderUpdate.append(format("%s",tickets.get(i).getIdImpComp()));
		}
		
		builderUpdate.append(")");
		
		final String queryI = builderInsert.toString();
		final String queryU = builderUpdate.toString();
		System.out.println(queryI);
		System.out.println(queryU);
		this.jdbcTemplate.execute(queryI);
		
		if(tickets.get(0).getTipoTicket().equals("MO"))
			this.jdbcTemplate.execute(queryU);
	}
	
	public List<Co2Manutenzione> getManutenzioneCO2(int annoDa,int annoA) {
		String storeprocedure ="{call CalcoloC02RisparmiataManutenzione("+annoDa+","+annoA+")}";
		List<Co2Manutenzione> co2_ticket = null;
		
		co2_ticket  = this.jdbcTemplate.query(storeprocedure,	new BeanPropertyRowMapper<Co2Manutenzione>(Co2Manutenzione.class));
		System.out.println("Manutenzione::getManutenzioneCO2 " + storeprocedure);
		
		return co2_ticket;
	}
	
	public List<Ticket> getManutenzioneTicketDaSchedulare(String time) {
		List<Ticket> ticket = null;
		
		String sql ="SELECT idIntervento,cdIntervento,idImmobile,idImpianto,codSocieta,denominazioneUp,codiceImmobile,idCategoria,descrizione,fornitore,buildingManager,utenteInserimento,dataRichiesta,dataInizioIntervento as dataFineIntervento,dataLimite,dataFineIntervento,durata,priorita,contatoreScarti,eseguito,dataCardine,numEsecutori,pianificato as pianificato,altezza,carico,sistemaEmittente FROM smee_ticket where dataRichiesta >= '" + time + "'";
		
		ticket  = this.jdbcTemplate.query(sql,	new BeanPropertyRowMapper<Ticket>(Ticket.class));
		System.out.println("Manutenzione::getManutenzioneTicket " + sql);
		
		return ticket;
	}



}
