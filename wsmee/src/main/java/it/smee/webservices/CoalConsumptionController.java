package it.smee.webservices;

import it.smee.consumption.dao.JdbcCoalConsumptionDAO;
import it.smee.jboxlib.JBoxDAO;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class CoalConsumptionController {
    private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
    @SuppressWarnings("unchecked")
    private static JBoxDAO coalConsumptionDAO = (JBoxDAO) context.getBean("coalConsumptionDAO");

    public CoalConsumptionController() {
        System.out.println("==== /CoalConsumptionController ====");
    }


    //WebService weather
    @RequestMapping(value = "/coal", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> consumption(
            @RequestParam(value="filterBy", required=true) List<String> filters,
            @RequestParam(value="filterParams", required=true) List<String> filter_params)
    {

        coalConsumptionDAO.resetFilters();

        if( (filters!=null) && (filter_params!=null))
            for(int i=0; i<filters.size(); i++)
                coalConsumptionDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

        return coalConsumptionDAO.getData();
    }
}
