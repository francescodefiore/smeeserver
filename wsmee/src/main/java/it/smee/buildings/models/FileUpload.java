package it.smee.buildings.models;

public class FileUpload {
	
	private String[] record;
	
    public FileUpload(){}
	
	public FileUpload(String[] record)
	{ 
		this.record= record;	
    }

	public String[] getRecord() {
		return record;
	}

	public void setRecord(String[] record) {
		this.record = record;
	}

}