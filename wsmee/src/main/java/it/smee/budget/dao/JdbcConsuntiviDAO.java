package it.smee.budget.dao;

import it.smee.jboxlib.JBoxDAO;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;


public class JdbcConsuntiviDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;
	private String filter_string = "";
	private String filter_columns = "*";
	private String aggregation_string = "";
	private String name = "";

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List< Map<String, Object>> getData() {
		List< Map<String, Object> > consuntivi = null;
		
		try {			
					String sql = "SELECT " + filter_columns + " " +
						     "FROM smee_consuntivi " +
						     "WHERE 1=1 "+ filter_string + " " +
						     aggregation_string;
					
					consuntivi = this.jdbcTemplate.queryForList(sql);
					
					System.out.println("query " + sql);

		}
			
		catch (Exception e) {
			    System.out.println(" ConsuntivoEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return consuntivi;
	}
	
	public List< Map<String, Object>> getDataAnno(String anno) {
		List< Map<String, Object> > consuntivi = null;
		
		try {			
					String sql = "SELECT " + filter_columns + " " +
						     "FROM smee_consuntivi " +
						     "WHERE 1=1 "
						     +" and (anno = " + anno + " or anno = " + (Integer.parseInt(anno) -1) + ") "
						     + filter_string + " " +
						     aggregation_string
						     //+" order by anno desc"
						     ;
					
					consuntivi = this.jdbcTemplate.queryForList(sql);
					
					System.out.println("query " + sql);

		}
			
		catch (Exception e) {
			    System.out.println(" ConsuntivoEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return consuntivi;
	}
	
		
	public List< Map<String, Object>> getConsumiMedi() {
		List< Map<String, Object> > consuntivi = null;
		
		try {			
						   
					String sql = "SELECT smee_immobili.*, smee_policies_immobili.*, smee_comuni.nome, smee_comuni.siglaProv as siglaprov, smee_comuni.gradigiorno, smee_comuni.gradigiornoestivi, smee_comuni.slm, smee_comuni.zona, (gen + feb + mar + apr + mag + giu + lug + ago + sett + ott + nov + dic)/1000 as consumo_medio_annuo  FROM smee_consuntivi RIGHT JOIN ((smee_immobili LEFT JOIN smee_comuni ON smee_immobili.IdComune=smee_comuni.IdComune) LEFT JOIN smee_policies_immobili on smee_immobili.idImmobile=smee_policies_immobili.idImmobile) on smee_immobili.pod=smee_consuntivi.pod where anno='2014' AND sitoCreem='SI' ORDER BY consumo_medio_annuo DESC";

					consuntivi = this.jdbcTemplate.queryForList(sql);
					
					System.out.println("query " + sql);

		}
			
		catch (Exception e) {
			    System.out.println(" getConsumiMedi::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return consuntivi;
	}

	@Override
	public void resetFilters() {
		filter_string = "";
		filter_columns = "*";
		aggregation_string = "";
	}
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = " && (pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		s = s.concat(")");
		
		return s;
	}
	
	
	private String yearFilter(String[] year)
	{			
		int i=0;
		String s = " && (anno = " + year[i]; 
		while(++i<year.length)
			s = s.concat(" OR anno = " + year[i]);
		s = s.concat(")");
		
		return s;
	}

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		switch(filter_name)
		{
			case "PROV":
				filter_string = filter_string.concat( provFilter(filter_params) );
				break;
				
			case "CLUSTER":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );				
				break;
				
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
			
			case "YEAR":	
				filter_string = filter_string.concat( yearFilter(filter_params) );				
				break;	

		}
		
	}
	
	private String clusterFilter(String[] cluster)
	{					
		int i=0;
		String s = " && pod IN (SELECT pod FROM smee_immobili WHERE clusterEnergy ='" + cluster[0] +"'";
		while(++i<cluster.length)
			s = s.concat(" OR clusterEnergy = '" + cluster[i] +"'");
		s = s.concat(")");
		return s;
	}

	private String provFilter(String[] prov)
	{	
		int i=0;
		String s = " && pod IN (SELECT pod FROM smee_immobili JOIN smee_comuni ON smee_immobili.idComune=smee_comuni.idComune WHERE siglaprov ='" + prov[0] +"'";	
		while(++i<prov.length)
			s = s.concat(" OR provincia = '" + prov[i] +"'");
		s = s.concat(")");
		return s;
	}

	
	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{
			case "PROV":	
					if( filter_params[0].compareTo("ALL") != 0)
						filter_string = filter_string.concat( provFilter(filter_params) );
				filter_columns = "pod, anno, sum(gen) as gen, sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(mag) as mag, sum(giu) as giu, sum(lug) as lug, sum(ago) as ago, sum(sett) as 'sett', sum(ott) as ott, sum(nov) as nov, sum(dic) as dic";
				aggregation_string = "GROUP BY anno";
				break;
				
			
			case "CLUSTERENERGY":	
				if( filter_params[0].compareTo("ALL") != 0)
					filter_string = filter_string.concat( clusterFilter(filter_params) );	
				filter_columns = "pod, anno, sum(gen) as gen, sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(mag) as mag, sum(giu) as giu, sum(lug) as lug, sum(ago) as ago, sum(sett) as 'sett', sum(ott) as ott, sum(nov) as nov, sum(dic) as dic";
				aggregation_string = "GROUP BY anno";
				break;
				
			case "PROV_AVG":
				if( filter_params[0].compareTo("ALL") != 0)
					filter_string = filter_string.concat( provFilter(filter_params) );
				filter_columns = "pod, anno, avg(gen) as gen, avg(feb) as feb, avg(mar) as mar, avg(apr) as apr, avg(mag) as mag, avg(giu) as giu, avg(lug) as lug, avg(ago) as ago, avg(sett) as 'sett', avg(ott) as ott, avg(nov) as nov, avg(dic) as dic";
				aggregation_string = "GROUP BY anno";
				break;
			
			case "CLUSTERENERGY_AVG":
				if( filter_params[0].compareTo("ALL") != 0)
					filter_string = filter_string.concat( clusterFilter(filter_params) );	
				filter_columns = "pod, anno, avg(gen) as gen, avg(feb) as feb, avg(mar) as mar, avg(apr) as apr, avg(mag) as mag, avg(giu) as giu, avg(lug) as lug, avg(ago) as ago, avg(sett) as 'sett', avg(ott) as ott, avg(nov) as nov, avg(dic) as dic";
				aggregation_string = "GROUP BY anno";
				break;
		}
	}
	


	@Override
	public void print() {
		System.out.println("--- JdbcConsuntiviDAO");		
	}

	@Override
	public String getName() {		
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
		
	}
	
	public List< Map<String, Object> > FileImport(String filename) {
		List< Map<String, Object> > consuntivi = null;
		try {		
				String sql = "LOAD DATA LOCAL INFILE '" + filename + "' " +
					 " INTO TABLE smee_consuntivi "+
					 " CHARACTER SET latin1 FIELDS TERMINATED BY \";\" IGNORE 1 LINES " +
					 "(pod,anno,@g,@f,@m,@a,@ma,@gi,@l,@ag,@s,@o,@n,@d) SET gen= REPLACE(@g, ',', '.'), feb= REPLACE(@f, ',', '.'),mar= REPLACE(@m, ',', '.'), apr= REPLACE(@a, ',', '.'),mag= REPLACE(@ma, ',', '.'), giu= REPLACE(@gi, ',', '.'),lug= REPLACE(@l, ',', '.'), ago= REPLACE(@ag, ',', '.'),sett= REPLACE(@s, ',', '.'), ott= REPLACE(@o, ',', '.'),nov= REPLACE(@n, ',', '.'), dic= REPLACE(@d, ',', '.')";
				
					System.out.println("query " + sql);
					this.jdbcTemplate.execute(sql);
		}
			
		catch (Exception e) {
			    System.out.println(" FileImport::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return consuntivi;
	}

}