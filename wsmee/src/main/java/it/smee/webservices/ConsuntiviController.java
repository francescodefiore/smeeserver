package it.smee.webservices;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import it.smee.budget.dao.JdbcConsuntiviDAO;
import it.smee.jboxlib.*;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


@Controller
public class ConsuntiviController {
	
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO consuntiviDAO = (JBoxDAO)context.getBean("consuntiviDAO");
    
	public ConsuntiviController() {
		System.out.println("==== /ConsuntiviController ====");
	} 

	@RequestMapping(value = "/consuntivi", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody List< Map<String, Object>> consuntivi(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,    
    		@RequestParam(value="groupByParams", required=false) List<String> group_params)
	{
		consuntiviDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				consuntiviDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )
			for(int i=0; i<groups.split(";").length; i++)
				consuntiviDAO.addAggregationFilter( groups.split(";")[i], group_params.get(i).split(";") );

		
		return consuntiviDAO.getData();
		
	}
	
	@RequestMapping(value = "/consuntivi/anno", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody List< Map<String, Object>> consuntivi_anno(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,    
    		@RequestParam(value="groupByParams", required=false) List<String> group_params,
    		@RequestParam(value="year", required=false) String year)
	{
		consuntiviDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				consuntiviDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )
			for(int i=0; i<groups.split(";").length; i++)
				consuntiviDAO.addAggregationFilter( groups.split(";")[i], group_params.get(i).split(";") );

		
		return ((JdbcConsuntiviDAO)consuntiviDAO).getDataAnno(year);
		
	}
	
	@RequestMapping(value = "/consumi_medi", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody List< Map<String, Object>> consumimedi()
	{		
		return ((JdbcConsuntiviDAO)consuntiviDAO).getConsumiMedi();		
	}
	
	@RequestMapping(value="/consuntivi/upload", method=RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    public void handleFileUpload(@RequestParam("file") MultipartFile file){
		System.out.println("==== /consuntivi/upload new request ====");
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Calendar c = Calendar.getInstance();
                String directory = "./imported/consuntivi";
                boolean success=(new File(directory)).mkdirs();		           
	            
	            	String name = directory + "/"+c.getTimeInMillis()+"_consuntivi.csv";
	            	BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name)));
	            	stream.write(bytes);
	            	stream.close();
	            	((JdbcConsuntiviDAO)consuntiviDAO).FileImport(name);
	                            
            } catch (Exception e) { }
        }
    }

	
}