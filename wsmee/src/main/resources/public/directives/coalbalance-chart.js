CREEMapp.directive('coalbalanceChart', 

function() 
{

	return {
    
    controller: ['$scope', 'CreemSettings', 'MonthlyConsumptionsFactory', 'ngTableParams',
	
		function($scope, CreemSettings, MonthlyConsumptionsFactory, ngTableParams)
		{			
					
			var seriesCounter = 0;
			$scope.dataset = [];
			$scope.data = {};
			
			var createChart = function(title, stitle)
			{
				var monthlyconsumptionschart = new Highcharts.Chart({
				chart: {
						renderTo: 'coalbalancechart',
						type: 'column'
					},
					credits: {
						enabled: false
					},
				
					title: {
						text: title
					},
					subtitle: {
						text: stitle
					},
					xAxis: {
						categories: [
							'2012',
							'2013'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Emissioni di CO2 (ton)'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.2f} ton </b> (<i>{point.percentage:.0f} %</i>)</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0,
							stacking: 'value'
						} 
					},
					series: $scope.dataset
				});
			}
							

			var getdata = function () 
			{
				
					var data = [];
					
			
					data = MonthlyConsumptionsFactory.getMonthlyConsumptions().query( function(){ prepareChart(data); });

			}
						
			var prepareChart = function(data)
			{				
				
				
				var consumptions = {};
				$scope.data['_2012'] = [0, 0, 0, 0, 0];
				$scope.data['_2013'] = [0, 0, 0, 0, 0];
				$scope.data['Fonte'] = ['Gasolio', 'Elettricità', 'Gas Naturale', 'GPL', 'Totale CO2 emessa'];
				$scope.data['_2012vs2013'] = [0, 0, 0, 0, 0];
								
				
				var currentTime = new Date();
				for(var i=0; i<data.length; i++)
				{
					if(data[i].anno < currentTime.getFullYear() - 1 ) 
					{	
						$scope.data['_' + data[i].anno][4] = data[i].gen + data[i].feb + data[i].mar + data[i].apr + data[i].mag + data[i].giu + data[i].lug + data[i].ago + data[i].sett + data[i].ott + data[i].nov + data[i].dic + $scope.data['_' + data[i].anno][4];												
					}													
				}
				
				
				
				
				$scope.data['_2012'][0] = 0;
				$scope.data['_2012'][1] = $scope.data['_2012'][4]*0.001*0.4332;
				$scope.data['_2012'][2] = 20.60*1000*1.95;
				$scope.data['_2012'][3] = 0;
				$scope.data['_2012'][4] = $scope.data['_2012'][1] + $scope.data['_2012'][2];
				
				
				$scope.data['_2013'][0] = 0;
				$scope.data['_2013'][1] = $scope.data['_2013'][4]*0.001*0.4332;
				$scope.data['_2013'][2] = 24.17*1000*1.95;
				$scope.data['_2013'][3] = 0;
				$scope.data['_2013'][4] = $scope.data['_2013'][1] + $scope.data['_2013'][2];
				
				console.log($scope.data);
				
				$scope.data['_2012vs2013'] = [ (($scope.data['_2012'][0] - $scope.data['_2013'][0])*100)/$scope.data['_2012'][0], (($scope.data['_2012'][1] - $scope.data['_2013'][1])*100)/$scope.data['_2012'][1], (($scope.data['_2012'][2] - $scope.data['_2013'][2])*100)/$scope.data['_2012'][2], (($scope.data['_2012'][3] - $scope.data['_2013'][3])*100)/$scope.data['_2012'][3], (($scope.data['_2012'][4] - $scope.data['_2013'][4])*100)/$scope.data['_2012'][4]];

				$scope.dataset = [
				{
					name: 'Gasolio',
					data: [$scope.data['_2012'][0], $scope.data['_2013'][0]]					
				},				
				{
					name: 'Elettricità',
					data: [$scope.data['_2012'][1], $scope.data['_2013'][1]]
				},
				{	name: 'Gas Naturale',
					data: [$scope.data['_2012'][2], $scope.data['_2013'][2]]
				},
				{	name: 'GPL',
					data: [$scope.data['_2012'][3], $scope.data['_2013'][3]]
				}
				];		
				
				createChart('Emissioni di CO2 degli immobili CREEM ', '');											
			}
			
			$scope.cols = [
					{ field: "Fonte", title: "Fonte", show: true},
					{ field: "_2012", title: "2012", show: true},
					{ field: "_2013", title: "2013", show: true},
					{ field: "_2012vs2013", title: "% CO<sub>2</sub> 2012 vs 2013", show: true}					
				];
				
				
			$scope.tableParams = new ngTableParams({
							page: 1, // show first page
							count: 10, // count per page,							
							data: $scope.data
					});
				
			var refreshData = function ()
			{										
					$scope.dataset.splice(0, $scope.dataset.length);		

					getdata();

			}

			refreshData();
   

		}],
		
		templateUrl: 'directives/coalbalance-chart.html',
		scope: false
  };
  
});
	
	