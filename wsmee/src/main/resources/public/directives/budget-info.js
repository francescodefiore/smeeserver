CREEMapp.directive('budgetInfo', 

function() 
{
	
	return {
	
	require:'^budgetChart',
    controller: ['$scope', 'CreemSettings','MonthlyConsumptionsBandsFactory',
		
		function($scope, CreemSettings, MonthlyConsumptionsBandsFactory) 
		{			

			var currentTime = new Date();
			$scope.monthBudget  = currentTime.getMonth() + 1;

			
			var gaugeOptions = {

				chart: {
					type: 'solidgauge',
					backgroundColor:'transparent'
				},

				title: null,

				pane: {
					center: ['50%', '85%'],
					size: '120%',
					startAngle: -90, 
					endAngle: 90,
					background: {
						backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE'
						,
						innerRadius: '60%',
						outerRadius: '100%',
						shape: 'arc'
					}
				},

				tooltip: {
					enabled: false
				},

				// the value axis
				yAxis: {
					stops: [
						[0.5, '#55BF3B'], // green
						[0.75, '#DDDF0D'], // yellow
						[0.98, '#DF5353'] // red
					],
					lineWidth: 0,
					minorTickInterval: null,
					tickPixelInterval: 400,
					tickWidth: 0,
					title: {
						y: -70
					},
					labels: {
						y: 16
					}
				},
				
				exporting: {
					enabled: false
				},

				plotOptions: {
					solidgauge: {
						dataLabels: {
							y: 5,
							borderWidth: 0,
							useHTML: true
						}
					}
				}
				
			};
			
			
			
			$('#gaugeBudget').highcharts(Highcharts.merge(gaugeOptions, {
				yAxis: {
					min: 0,
					max: 100,
					title: {
						text: 'Budget attualmente utilizzato'
					}
				},

				credits: {
					enabled: false
				},

				series: [{
					name: 'Budget Utilizzato',
					data: [0],
					 dataLabels: {
					format: '<div style="text-align:center"><span style="font-size:20px;color:' +
						((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') +  '">{y}</span>' +
						   '<span style="font-size:20px;color:'+
						((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') +'">%</span></div>'
					},
					tooltip: {
						valueSuffix: '%'
					}
				}]

			}));
			
			var refreshData = function ()
			{										

					if(CreemSettings.selectedbuildings.length > 0)
						$scope.budgetclass = CreemSettings.selectedbuildings[0].codice;
					else if(CreemSettings.selectedCities.length > 0)
						$scope.budgetclass = CreemSettings.selectedCities[0];
					else if(CreemSettings.selectedClusters.length > 0)
						$scope.budgetclass = CreemSettings.selectedClusters[0];
							
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();


		}],
		
		templateUrl: 'directives/budget-info.html'
		
  };
  
});
	
	