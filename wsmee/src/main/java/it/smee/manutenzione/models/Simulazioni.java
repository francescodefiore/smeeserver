package it.smee.manutenzione.models;

public class Simulazioni {
	private int idImmobile;
	private int codiceIntervento;
	private String codiceImmobile;
	private int tipologiaIntervento;
	private String ubicazione;
	private int numeroInterventi;
	private float costoTotale;
	private float epiAttuale;
	private float epiSimulato;
	private float parametro;
	private String livello;
	private String descrizioneIntervento;
	
	public int getIdImmobile() {
		return idImmobile;
	}
	public void setIdImmobile(int idImmobile) {
		this.idImmobile = idImmobile;
	}
	public int getCodiceIntervento() {
		return codiceIntervento;
	}
	public void setCodiceIntervento(int codiceIntervento) {
		this.codiceIntervento = codiceIntervento;
	}
	public String getCodiceImmobile() {
		return codiceImmobile;
	}
	public void setCodiceImmobile(String codiceImmobile) {
		this.codiceImmobile = codiceImmobile;
	}
	public int getTipologiaIntervento() {
		return tipologiaIntervento;
	}
	public void setTipologiaIntervento(int tipologiaIntervento) {
		this.tipologiaIntervento = tipologiaIntervento;
	}
	public String getUbicazione() {
		return ubicazione;
	}
	public void setUbicazione(String ubicazione) {
		this.ubicazione = ubicazione;
	}
	public int getNumeroInterventi() {
		return numeroInterventi;
	}
	public void setNumeroInterventi(int numeroInterventi) {
		this.numeroInterventi = numeroInterventi;
	}
	public float getCostoTotale() {
		return costoTotale;
	}
	public void setCostoTotale(float costoTotale) {
		this.costoTotale = costoTotale;
	}
	public float getEpiAttuale() {
		return epiAttuale;
	}
	public void setEpiAttuale(float epiAttuale) {
		this.epiAttuale = epiAttuale;
	}
	public float getEpiSimulato() {
		return epiSimulato;
	}
	public void setEpiSimulato(float epiSimulato) {
		this.epiSimulato = epiSimulato;
	}
	public float getParametro() {
		return parametro;
	}
	public void setParametro(float parametro) {
		this.parametro = parametro;
	}
	public String getLivello() {
		return livello;
	}
	public void setLivello(String livello) {
		this.livello = livello;
	}
	public String getDescrizioneIntervento() {
		return descrizioneIntervento;
	}
	public void setDescrizioneIntervento(String descrizioneIntervento) {
		this.descrizioneIntervento = descrizioneIntervento;
	}
}
