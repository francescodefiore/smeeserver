package it.smee.buildings.models;

public class Edificio{
	
	private int idImmobile;
	private String pod;
	private String codice;
	private String denominazione;
	private String indirizzoPod;
	private String indirizzo; 
	private double latitudine;
	private double longitudine;
	private String areaImmobiliare;
	private int potenzaInstallata;
	private String tipologiaFornitura;
	private String frazionario;
	private String classificazione;
	private String tipoTurno;
	private String funzioniBusiness;
	private String clusterEnergy;
	private String occupazioneImmobile;
	private String destinazioneDuso;
	private String nome;
	private String siglaprov;
	private String provincia;
	private String zona;
	private String orarioLunVen;
	private String orarioSab;
	
	
	
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getDenominazione() {
		return denominazione;
	}
	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}
	public String getIndirizzoPod() {
		return indirizzoPod;
	}
	public void setIndirizzoPod(String indirizzoPod) {
		this.indirizzoPod = indirizzoPod;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public double getLatitudine() {
		return latitudine;
	}
	public void setLatitudine(double latitudine) {
		this.latitudine = latitudine;
	}
	public double getLongitudine() {
		return longitudine;
	}
	public void setLongitudine(double longitudine) {
		this.longitudine = longitudine;
	}
	
	
	public String getAreaImmobiliare() {
		return areaImmobiliare;
	}
	public void setAreaImmobiliare(String areaImmobiliare) {
		this.areaImmobiliare = areaImmobiliare;
	}
	public String getTipologiaFornitura() {
		return tipologiaFornitura;
	}
	public void setTipologiaFornitura(String tipologiaFornitura) {
		this.tipologiaFornitura = tipologiaFornitura;
	}
	public int getPotenzaInstallata() {
		return potenzaInstallata;
	}
	public void setPotenzaInstallata(int potenzaInstallata) {
		this.potenzaInstallata = potenzaInstallata;
	}
	public String getFrazionario() {
		return frazionario;
	}
	public void setFrazionario(String frazionario) {
		this.frazionario = frazionario;
	}
	public String getClassificazione() {
		return classificazione;
	}
	public void setClassificazione(String classificazione) {
		this.classificazione = classificazione;
	}
	public String getTipoTurno() {
		return tipoTurno;
	}
	public void setTipoTurno(String tipoTurno) {
		this.tipoTurno = tipoTurno;
	}
	public String getFunzioniBusiness() {
		return funzioniBusiness;
	}
	public void setFunzioniBusiness(String funzioniBusiness) {
		this.funzioniBusiness = funzioniBusiness;
	}
	public String getClusterEnergy() {
		return clusterEnergy;
	}
	public void setClusterEnergy(String clusterEnergy) {
		this.clusterEnergy = clusterEnergy;
	}
	public String getOccupazioneImmobile() {
		return occupazioneImmobile;
	}
	public void setOccupazioneImmobile(String occupazioneImmobile) {
		this.occupazioneImmobile = occupazioneImmobile;
	}
	
	public int getIdImmobile() {
		return idImmobile;
	}
	public void setIdImmobile(int idImmobile) {
		this.idImmobile = idImmobile;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSiglaprov() {
		return siglaprov;
	}
	public void setSiglaprov(String siglaprov) {
		this.siglaprov = siglaprov;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public String getDestinazioneDuso() {
		return destinazioneDuso;
	}
	public void setDestinazioneDuso(String destinazioneDuso) {
		this.destinazioneDuso = destinazioneDuso;
	}
	public String getOrarioLunVen() {
		return orarioLunVen;
	}
	public void setOrarioLunVen(String orarioLunVen) {
		this.orarioLunVen = orarioLunVen;
	}
	public String getOrarioSab() {
		return orarioSab;
	}
	public void setOrarioSab(String orarioSab) {
		this.orarioSab = orarioSab;
	}
	


}