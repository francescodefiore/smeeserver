package it.smee.budget.dao;

import it.smee.jboxlib.JBoxDAO;

import javax.sql.DataSource;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;



public class JdbcBudgetDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;	
	private String filter_string = "";
	private String filter_columns = "*";
	private String aggregation_string = "";
	private String name = "";

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Map<String, Object>> getData() {
		List< Map<String, Object> > budget = null;
		
		try {					
					String sql = "SELECT " + filter_columns + " " +
							     "FROM smee_budget " +
							     "WHERE 1=1 "+ filter_string + " " + aggregation_string;

					System.out.println("query " + sql);
					
					budget = this.jdbcTemplate.queryForList(sql);
		}
			
		catch (Exception e) {
			    System.out.println(" BudgetEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return budget;
	}
	
	public List<Map<String, Object>> getDataAnno(String anno) {
		List< Map<String, Object> > budget = null;
		
		try {					
					String sql = "SELECT " + filter_columns + " " +
							     "FROM smee_budget " 
							     + "WHERE 1=1 "
							     +" and anno = " + anno + " "
							     + filter_string + " " + aggregation_string;

					System.out.println("query " + sql);
					
					budget = this.jdbcTemplate.queryForList(sql);
		}
			
		catch (Exception e) {
			    System.out.println(" BudgetEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return budget;
	}

	@Override
	public void resetFilters() {
		filter_string = "";
		filter_columns = "*";
		aggregation_string = "";
	}
	
	
	private String yearFilter(String[] year)
	{			
		int i=0;
		String s = " && (anno = " + year[i]; 
		while(++i<year.length)
			s = s.concat(" OR anno = " + year[i]);
		s = s.concat(")");
		return s;
	}
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = " && (pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		s = s.concat(")");
		return s;
	}
	
	private String clusterFilter(String[] cluster)
	{		
		int i=0;
		String s = " && pod IN (SELECT pod FROM smee_immobili WHERE clusterEnergy ='" + cluster[0] + "'";
		while(++i<cluster.length)
			s = s.concat(" OR clusterEnergy = '" + cluster[i] +"'");
		s = s.concat(")");
		return s;
	}
	
	private String provFilter(String[] prov)
	{	
		int i=0;
		String s = " && pod IN (SELECT pod FROM smee_immobili JOIN smee_comuni ON smee_immobili.idComune=smee_comuni.idComune WHERE siglaprov ='" + prov[0] + "'";	
		while(++i<prov.length)
			s = s.concat(" OR provincia = '" + prov[i] + "'");
		s = s.concat(")");
		return s;
	}



	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		switch(filter_name)
		{
			case "PROV":
				filter_string = filter_string.concat( provFilter(filter_params) );
				break;
				
			case "CLUSTER":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );				
				break;
				
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
			
			case "ANNO":	
				filter_string = filter_string.concat( yearFilter(filter_params) );				
				break;
		}
		

	}


	@Override
	public void print() {
		System.out.println("--- JdbcBudgetDAO");		
	}

	
	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{
			case "PROV":	
				filter_string = filter_string.concat( provFilter(filter_params) );
				filter_columns = "pod, anno, sum(gen) as gen, sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(mag) as mag, sum(giu) as giu, sum(lug) as lug, sum(ago) as ago, sum(sett) as 'sett', sum(ott) as ott, sum(nov) as nov, sum(dic) as dic";
				aggregation_string = "GROUP BY anno";
				break;
			
			case "CLUSTERENERGY":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );	
				filter_columns = "pod, anno, sum(gen) as gen, sum(feb) as feb, sum(mar) as mar, sum(apr) as apr, sum(mag) as mag, sum(giu) as giu, sum(lug) as lug, sum(ago) as ago, sum(sett) as 'sett', sum(ott) as ott, sum(nov) as nov, sum(dic) as dic";
				aggregation_string = "GROUP BY anno";
				break;
				
				
			case "PROV_AVG":	
				filter_string = filter_string.concat( provFilter(filter_params) );
				filter_columns = "pod, anno, avg(gen) as gen, avg(feb) as feb, avg(mar) as mar, avg(apr) as apr, avg(mag) as mag, avg(giu) as giu, avg(lug) as lug, avg(ago) as ago, avg(sett) as 'sett', avg(ott) as ott, avg(nov) as nov, avg(dic) as dic";
				aggregation_string = "GROUP BY anno";
				break;
			
			case "CLUSTERENERGY_AVG":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );	
				filter_columns = "pod, anno, avg(gen) as gen, avg(feb) as feb, avg(mar) as mar, avg(apr) as apr, avg(mag) as mag, avg(giu) as giu, avg(lug) as lug, avg(ago) as ago, avg(sett) as 'sett', avg(ott) as ott, avg(nov) as nov, avg(dic) as dic";
				aggregation_string = "GROUP BY anno";
				break;

		}
	}
	
/*
	public FileUpload Import(List<FileUpload> filerows) {
		FileUpload success= new FileUpload();
		success.setRecord(null);
		
		String sql;
		
		for (int i = 1; i < filerows.size(); i++) {
			String[] record = filerows.get(i).getRecord();
			if (record.length>2) {
				sql = "SELECT count(pod) FROM softeco_budget where pod ='" + record[0] + "' AND anno='" + record[1] + "'";
				int id_pod = this.jdbcTemplate.queryForObject(sql, Integer.class);	
				if (id_pod == 0)  { 	
					sql = "INSERT INTO softeco_budget (pod, anno, gen, feb, mar, apr, mag, giu, lug, ago, set, ott, nov, dic) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
					this.jdbcTemplate.update(sql, record[0], record[1],record[2], record[3], record[4],record[5],record[6], record[7],record[8], record[9], record[10],record[11],record[12],record[13]);           				
					success.setRecord(record);
				}
			}
		}       
		return success;
	}
*/
	
	public List< Map<String, Object> > FileImport(String filename) {
				List< Map<String, Object> > budget = null;
		
		try {		
			
			String sql = "LOAD DATA LOCAL INFILE '" + filename +"' " +
						 " INTO TABLE smee_budget "+
						 " CHARACTER SET latin1 FIELDS TERMINATED BY \";\" IGNORE 1 LINES " +
						 "(pod,anno,@g,@f,@m,@a,@ma,@gi,@l,@ag,@s,@o,@n,@d) SET gen= REPLACE(@g, ',', '.'), feb= REPLACE(@f, ',', '.'),mar= REPLACE(@m, ',', '.'), apr= REPLACE(@a, ',', '.'),mag= REPLACE(@ma, ',', '.'), giu= REPLACE(@gi, ',', '.'),lug= REPLACE(@l, ',', '.'), ago= REPLACE(@ag, ',', '.'),sett= REPLACE(@s, ',', '.'), ott= REPLACE(@o, ',', '.'),nov= REPLACE(@n, ',', '.'), dic= REPLACE(@d, ',', '.')";
					
			System.out.println("query " + sql);							
			this.jdbcTemplate.execute(sql);
		}
			
		catch (Exception e) {
			System.out.println(" BudgetEdificio::Query non eseguita");
			System.out.println(e);
			    
		}	
		
		return budget;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
		
	}
}
