package it.smee.webservices;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import it.smee.budget.dao.JdbcBudgetDAO;
import it.smee.budget.dao.JdbcConsuntiviDAO;
import it.smee.jboxlib.JBox;
import it.smee.jboxlib.JBoxComposite;
import it.smee.jboxlib.JBoxDAO;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;




@Controller
public class DashboardBudgetController {
	
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");	
	
	private static final JBoxDAO dashboardBudgetDAO = (JBoxDAO) context.getBean("dashboardBudgetDAO");
    
	public DashboardBudgetController() {
		System.out.println("==== /DashboardController ====");
	} 
	
	@RequestMapping(value = "/dashboard_budget", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody List< Map<String, Object> > dashboard_budget(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,
    		@RequestParam(value="groupByParams", required=false) List<String> group_params,
    		@RequestParam(value="orderBy", required=false) String sortings)
	{
		System.out.println("==== /dashboard_budget new request ====");
		
		dashboardBudgetDAO.resetFilters();
		
		//if( (filters!=null) && (filter_params!=null))
		//	for(int i=0; i<filters.size(); i++)				
		//		dashboardDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

		
		return dashboardBudgetDAO.getData();
	}
	
	
	/*
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");	
	private static final DataSource datasource = (DataSource) context.getBean("mysqlDataSource");
		

	public DashboardBudgetController() {
		System.out.println("==== /DashboardBudgetController ====");
	}
	
	@RequestMapping(value = "/dashboard_budget", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody JBox dashboard_budget(
					@RequestParam(value="filterBy", required=false) List<String> filters,
		    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
		    		@RequestParam(value="groupBy", required=false) String groups,
		    		@RequestParam(value="groupByParams", required=false) List<String> group_params)
	{

		
		JBoxComposite root = new JBoxComposite("root");
		
		JBoxDAO consuntivi = new JdbcConsuntiviDAO();		
		consuntivi.setName("consuntivi");
		consuntivi.setDataSource(datasource);
		consuntivi.resetFilters();		
		root.add(consuntivi);
		
		
		JBoxDAO budget = new JdbcBudgetDAO();
		budget.setName("budget");
		budget.setDataSource(datasource);
		budget.resetFilters();
		root.add(budget);
		
		root.print();

		
		
		
		
		for(i=0; i<CONDITIONAL_FILTERS.length; i++)
			try{
				boxes.get( Arrays.asList(NODES).indexOf( CONDITIONAL_NODES[i] ) ).addConditionalFilter(CONDITIONAL_FILTERS[i], conditionalParams.get(i).split(";") );
			}catch(Exception e)
			{
				System.out.println("==== /CustomController ==== Impossibile aggiungere filtro condizionale " + CONDITIONAL_FILTERS[i]);
			}
		
		for(i=0; i<AGGREGATION_FILTERS.length; i++)
			try{
				boxes.get( Arrays.asList(NODES).indexOf( AGGREGATION_NODES[i] ) ).addAggregationFilter(AGGREGATION_FILTERS[i], aggregationParams.get(i).split(";")  );
			}catch(Exception e)
			{
				System.out.println("==== /CustomController ==== Impossibile aggiungere filtro di aggregazione " + AGGREGATION_FILTERS[i]);
			}
	
		return root;
		*/
	}
