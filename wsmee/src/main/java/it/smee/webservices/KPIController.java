package it.smee.webservices;

import it.smee.epi.dao.JdbcModuloEpiDAO;
import it.smee.epi.models.ModuloEpi;
import it.smee.jboxlib.JBoxDAO;
import it.smee.kpi.dao.JdbcKpiDAO;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class KPIController {
	
	private static final ApplicationContext context =	new ClassPathXmlApplicationContext("Spring-Module.xml");
	private static final JBoxDAO kpiDAO = (JBoxDAO) context.getBean("kpiDAO");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO potenzapiccoDAO = (JBoxDAO) context.getBean("potenzapiccoDAO");
	//private static final JBoxDAO energiatotaleDAO = (JBoxDAO) context.getBean("energiatotaleDAO");
	
	public KPIController() 
	{
		System.out.println("==== /ControllerKPI ====");
	}
	
	/*
	@RequestMapping(value = "/kpi/energia_totale_annua", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List<EnergiaTotaleAnnua> multi( 
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups)
    {  
	
		energiatotaleDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				energiatotaleDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )	
			for(int i=0; i<groups.split(";").length; i++)
				energiatotaleDAO.addAggregationFilter( groups.split(";")[i], null );
		

		return energiatotaleDAO.getData();	
    }
	*/
	
	@RequestMapping(value = "/kpi/potenza_picco", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object> > potenzapicco( 
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups)
    {  
	
		potenzapiccoDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				potenzapiccoDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )	
			for(int i=0; i<groups.split(";").length; i++)
				potenzapiccoDAO.addAggregationFilter( groups.split(";")[i], null );
		

		return potenzapiccoDAO.getData();	
    }
	
	
	@RequestMapping(value = "/kpi/avviaCalcolo", method = RequestMethod.GET,headers="Accept=application/json")
	public  @ResponseBody List<Map<String, Object>> avviaCalcolo()
	{
		System.out.println("==== /kpi/avviaCalcolo ====");
		return kpiDAO.getData();
	}
	
	@RequestMapping(value = "/kpi/tolleranza", method = RequestMethod.GET,headers="Accept=application/json")
	public  @ResponseBody List<Map<String, Object>> getTolleranza(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params) 
	{
		System.out.println("==== /kpi/tolleranza ====");
		kpiDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				kpiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		return ((JdbcKpiDAO)kpiDAO).getTolleranza();
	}
	
	@RequestMapping(value = "/kpi/tolleranzaUpdate", method = RequestMethod.POST)
	public  ResponseEntity<List<Map<String, Object>>> updateTolleranza(@RequestBody List<Map<String, Object>> listaTolleranza)
	{
		System.out.println("==== /kpi/tolleranzaUpdate ====");
		((JdbcKpiDAO)kpiDAO).updateTolleranza(listaTolleranza);
		
		return new ResponseEntity<List<Map<String, Object>>>(listaTolleranza, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kpi/tipo", method = RequestMethod.GET,headers="Accept=application/json")
	public  @ResponseBody List<Map<String, Object>> getKpi(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params) 
	{
		System.out.println("==== /kpi/tipo ====");
		kpiDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				kpiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		return ((JdbcKpiDAO)kpiDAO).getKpi();
	}
	
	
	@RequestMapping(value = "/kpi", method = RequestMethod.GET,headers="Accept=application/json")
	 public  @ResponseBody List<Map<String, Object>> getKpiValue(
	   @RequestParam(value="filterBy", required=false) List<String> filters,
	      @RequestParam(value="filterParams", required=false) List<String> filter_params) 
	 {
	  System.out.println("==== /kpi ====");
	  kpiDAO.resetFilters();
	  
	  if( (filters!=null) && (filter_params!=null))
	   for(int i=0; i<filters.size(); i++)    
	    kpiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
	  
	  return ((JdbcKpiDAO)kpiDAO).getKpiValue();
	 }
	
	
	
	@RequestMapping(value = "/kpi/alert", method = RequestMethod.GET,headers="Accept=application/json")
	 public  @ResponseBody List<Map<String, Object>> getKpiAlert(
	   @RequestParam(value="filterBy", required=false) List<String> filters,
	      @RequestParam(value="filterParams", required=false) List<String> filter_params) 
	 {
	  System.out.println("==== /kpi/alert ====");
	  kpiDAO.resetFilters();
	  
	  if( (filters!=null) && (filter_params!=null))
	   for(int i=0; i<filters.size(); i++)    
	    kpiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
	  
	  return ((JdbcKpiDAO)kpiDAO).getKpiAlert();
	 }
	
	@RequestMapping(value = "/alerts", method = RequestMethod.GET,headers="Accept=application/json")
	 public  @ResponseBody List<Map<String, Object>> getAlerts(@RequestParam(value="filterBy", required=false) List<String> filters,
		      @RequestParam(value="filterParams", required=false) List<String> filter_params) 
	{
	  System.out.println("==== /alerts ====");
	  kpiDAO.resetFilters();
	  
	  if( (filters!=null) && (filter_params!=null))
	   for(int i=0; i<filters.size(); i++)    
	    kpiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
	  
	  return ((JdbcKpiDAO)kpiDAO).getAlerts();
	 }
	
	
	@RequestMapping(value = "/kpi/gettipo", method = RequestMethod.GET,headers="Accept=application/json")
	public  @ResponseBody List<Map<String, Object>> getTipoKpi() 
	{
			//ritorna i tipi kpi 
			System.out.println("==== /kpi/gettipo ====");
			
			return ((JdbcKpiDAO)kpiDAO).getTipoKpi();
	}
	
    
}