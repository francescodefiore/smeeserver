package it.smee.status.dao;

import it.smee.status.models.*;


public interface StatusDAO{
	
	public Status retrieveData();
}

