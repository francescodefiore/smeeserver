package it.smee.power.dao;

import it.smee.jboxlib.JBoxDAO;

import javax.sql.DataSource;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;



public class JdbcProfilesDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;	
	private String filter_string = "";
	private String table_string = "";
	private String group_string = "";
	
	private String name = "";

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Map<String, Object>> getData() {
		List< Map<String, Object> > profiles = null;
		
		try {					
					String sql = "SELECT * " +
							     "FROM smee_profili_" + group_string + "_" + table_string + " "+
							     "WHERE 1=1 "+ filter_string;

					System.out.println("query " + sql);
					
					profiles = this.jdbcTemplate.queryForList(sql);
		}
			
		catch (Exception e) {
			    System.out.println(" BudgetEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return profiles;
	}

	@Override
	public void resetFilters() {
		filter_string = "";
		table_string = "";
		group_string = "";
	}
	
	
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = " && (pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		s = s.concat(")");
		return s;
	}
	
	private String turnoFilter(String turno)
	{			
		return turno;
	}
	
	private String groupFilter(String group)
	{			
		return group;
	}
	
	private String clusterFilter(String[] cluster)
	{		
		int i=0;
		String s = "&& (clusterEnergy ='" + cluster[0] + "'";
		while(++i<cluster.length)
			s = s.concat(" OR clusterEnergy = '" + cluster[i] +"'");
		s = s.concat(")");
		return s;
	}
	
	private String orarioFilter(String[] orario)
	{		
		int i=0;
		String s = "&& (orario ='" + orario[0] + "'";
		while(++i<orario.length)
			s = s.concat(" OR orario = '" + orario[i] +"'");
		s = s.concat(")");
		return s;
	}
	
	private String zonaFilter(String[] zona)
	{		
		int i=0;
		String s = "&& (zonaclimatica ='" + zona[0] + "'";
		while(++i<zona.length)
			s = s.concat(" OR zonaclimatica = '" + zona[i] +"'");
		s = s.concat(")");
		return s;
	}
	


	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		switch(filter_name)
		{
				
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
			
			case "CLUSTER":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );				
				break;
	
			case "TURNO":	
				table_string = table_string.concat( turnoFilter(filter_params[0]) );				
				break;
				
			case "GROUP":	
				group_string = group_string.concat( groupFilter(filter_params[0]) );				
				break;
				
			case "ORARIO":	
				filter_string = filter_string.concat( orarioFilter(filter_params) );				
				break;
				
			case "ZONA":	
				filter_string = filter_string.concat( zonaFilter(filter_params) );				
				break;
	
	
			
		}
		

	}


	@Override
	public void print() {
		System.out.println("--- JdbcProfilesDAO");		
	}

	
	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) 
	{
	}
	


	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
		
	}
}
