CREEMapp.directive('settingsDaticampoSelection', function() {
	
	  return {
		scope: {},
	  
		templateUrl: 'directives/settings-daticampo-selection.html',
				
		controller: ['$scope', 'BuildingsFactory', 'CreemSettings',   function($scope, BuildingsFactory, CreemSettings) {
									
									
							$scope.selectedBuildings = [];
							
							$scope.selected_buildings = [];

							$scope.selected_measure = '';
							
							$scope.immobili = BuildingsFactory.getBuildings().query(
								function(){
									
									if(CreemSettings.selectedbuildings.length == 0)													
										CreemSettings.setBuilding($scope.immobili[0]);
									
									for(var i=0; i<CreemSettings.selectedbuildings.length; i++)
									{									
										$scope.selected_buildings.push( CreemSettings.selectedbuildings[i].codice);										
										$scope.selectedBuildings = CreemSettings.selectedbuildings;
									}
									
									
								}
							);
							

							$scope.selectedDate = CreemSettings.selectedDates; 
							$scope.selected_measure = CreemSettings.selectedMeasures;
							
							$scope.selected_dates = {startDate: moment(CreemSettings.selectedDates.from), endDate: moment(CreemSettings.selectedDates.to)}
							
							$scope.ranges = {							
								'Ieri': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
								'Ultimi 7 giorni': [moment().subtract(7, 'days'), moment()],
								'Ultimi 30 giorni': [moment().subtract(30, 'days'), moment()],
								'Mese corrente': [moment().startOf('month'), moment().subtract(1, 'days')]
							};
														
							
							$scope.setFilters = function() {
																								
								var selDates = {from:  moment($scope.selected_dates.startDate).format("YYYY-MM-DD"), to: moment($scope.selected_dates.endDate).format("YYYY-MM-DD")};															
								var conf = { buildings: $scope.selectedBuildings, measures: $scope.selected_measure, dates: selDates, datescampo: selDates};
								CreemSettings.setSettings(conf);
							}
														
						}
		],
		
		link: function(scope, element)
			  { 			  			
					$(element).find('#measure_list').val(scope.selected_measure);	
					$(element).find('#date_range').val(scope.selectedDate.from + " - " + scope.selectedDate.to);
					$(element).find('#buildings_list').val(scope.selected_buildings);
					
					scope.$watch('immobili', function(newValue, oldValue) {																	
																				
										if (newValue.length != 0)																				
											$(element).find('.chosen-select').chosen();
										
								}, true);
					
				}
		
		
	  }
	});
	