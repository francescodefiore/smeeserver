CREEMapp.directive('buildingSelection', function() {

	  return {		
		
		templateUrl: 'directives/building-selection.html',
				
		controller: ['$scope', 'BuildingsFactory', 'CreemSettings', function($scope, BuildingsFactory, CreemSettings) {
							
							$scope.selected_building;
																										
							$scope.immobili = BuildingsFactory.getBuildings().query(
								function(){
									if(CreemSettings.selectedbuildings.length == 0)													
										CreemSettings.setBuilding($scope.immobili[0]);
									$scope.selected_building = CreemSettings.selectedbuildings[0];	
								}
							);

							$scope.setBuilding = function() 
							{													
								CreemSettings.setBuilding($scope.selected_building);
							}
												
						}
		],
		
		link: function(scope, element)
			  { 		
					
					$(element).find('#buildings_list').val(scope.selected_building);										
					scope.$watch('immobili', function(newValue, oldValue) {																	
										if (newValue.length != 0)																				
										{
											$(element).find('#buildings_list').chosen();
										}
										
								}, true);	
								
				}
				
		
		
	  }
	});
	
	