package it.smee.webservices;

import java.util.List;
import java.util.Map;

import it.smee.jboxlib.JBoxDAO;
import it.smee.manutenzione.dao.JdbcManutenzioneDAO;
import it.smee.manutenzione.models.Co2Manutenzione;
import it.smee.manutenzione.models.Co2Ticket;
import it.smee.manutenzione.models.InsertTicket;
import it.smee.manutenzione.models.Interventi;
import it.smee.manutenzione.models.Scenari;
import it.smee.manutenzione.models.Simulazioni;
import it.smee.manutenzione.models.Ticket;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ManutenzioneController {
	
	private static final ApplicationContext context =	new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO manutenzioneDAO = (JBoxDAO) context.getBean("manutenzioneDAO");
	
	@RequestMapping(value = "/manutenzione/interventi", method = RequestMethod.GET)
	public @ResponseBody List<Interventi> getManutenzioneInterventi(@RequestParam(value="idImmobile", required=true) Integer idimmobile) {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getManutenzioneInterventi(idimmobile);
	}
	
	@RequestMapping(value = "/manutenzione/interventi/all", method = RequestMethod.GET)
	public @ResponseBody List<Interventi> getManutenzioneAllInterventi() {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getManutenzioneAllInterventi();
	}
	
	@RequestMapping(value = "/manutenzione/getinterventi", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> getInterventi() {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getTipoInterventi();
	}
	/*
	@RequestMapping(value = "/manutenzione/scenari", method = RequestMethod.GET)
	public @ResponseBody List<Scenari> getManutenzioneScenari(
			@RequestParam(value="scenari", required=true) Integer scenari, 
			@RequestParam(value="idImmobile", required=true) Integer idimmobile) {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getManutenzioneScenari(scenari,idimmobile);
	}
	*/
	@RequestMapping(value = "/manutenzione/ticket", method = RequestMethod.GET)
	public @ResponseBody List<Ticket> getManutenzioneTicket(@RequestParam(value="time", required=true) String time) {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getManutenzioneTicket(time);
	}
	
	@RequestMapping(value = "/manutenzione/ticketCO2", method = RequestMethod.GET)
	public @ResponseBody List<Co2Ticket> getManutenzioneTicketCO2(@RequestParam(value="anno", required=true) int anno) {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getManutenzioneTicketCO2(anno);
	}
	
	
	@RequestMapping(value = "/smee/scenari", method = RequestMethod.GET)
	public @ResponseBody List<Scenari> getScenariImmobile(@RequestParam(value="idImmobile", required=true) Integer idimmobile) {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getScenariImmobile(idimmobile);
	}

	@RequestMapping(value = "/manutenzione/insertTickets", method = RequestMethod.POST)
	public @ResponseBody void insertTickets(@RequestBody List<InsertTicket> tickets) {	
		((JdbcManutenzioneDAO)manutenzioneDAO).insertTickets(tickets);
	}
	
	@RequestMapping(value = "/manutenzione/co2Manutenzione", method = RequestMethod.GET)
	public @ResponseBody List<Co2Manutenzione> getManutenzioneCO2(@RequestParam(value="annoDa", required=true) int annoDa,
															@RequestParam(value="annoA", required=true) int annoA) {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getManutenzioneCO2(annoDa,annoA);
	}
	
	@RequestMapping(value = "/manutenzione/ticketDaSchedulare", method = RequestMethod.GET)
	public @ResponseBody List<Ticket> getManutenzioneTicketDaSchedulare(@RequestParam(value="time", required=true) String time) {	
		return ((JdbcManutenzioneDAO)manutenzioneDAO).getManutenzioneTicketDaSchedulare(time);
	}
}
