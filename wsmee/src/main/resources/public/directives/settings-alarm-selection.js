CREEMapp.directive('settingsAlarmSelection', function() {

	  return {		
		templateUrl: 'directives/settings-alarm-selection.html',
				
		controller: ['$scope', 'CreemSettings', function($scope, CreemSettings) {
							
							$scope.setDateAlarm = function() 
							{													
								CreemSettings.setDateAlarm($scope.selected_date);
							}
							
							$scope.setRiferimento = function() 
							{													
								CreemSettings.setRiferimento($scope.selected_periodo);
							}
							
							$scope.setBuilding = function() 
							{													
								CreemSettings.setBuilding($scope.selected_building);
							}
							
							$scope.setKpi = function() 
							{													
								CreemSettings.setKpi($scope.selectedKpi);
							}
							
							$scope.onClickUpdate = function (tab)
							{
							
								if($scope.currentTab == 'building_tab')
									$scope.setBuilding();								
								if($scope.currentTab == 'date_tab')
									$scope.setDateAlarm();									
								if($scope.currentTab == 'periodo_tab')
									$scope.setRiferimento();
								if($scope.currentTab == 'kpi_tab')
									$scope.setKpi();
									
							}
							
							
							$scope.building_tab = {	name: 'building_tab'};					
							$scope.date_tab = {	name: 'date_tab'};
							$scope.periodo_tab = {	name: 'periodo_tab'};
							$scope.kpi_tab = {	name: 'kpi_tab'};
							

								$scope.currentTab = 'building_tab';

								$scope.onClickTab = function (tab) {
									$scope.currentTab = tab.name;
									
								}
								
								$scope.isActiveTab = function(name) {
									return name == $scope.currentTab;
								}	
								
							

						}
						
		]

		
	  }

	});