'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'views/login/LoginView.html'
    });
}]);