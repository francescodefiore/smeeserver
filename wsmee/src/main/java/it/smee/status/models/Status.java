package it.smee.status.models;

public class Status {
	
	private String db_connector;
	private String db_url;
	private String db_user;
	
	public Status(String db_connector, String db_url, String db_user) {
		this.db_connector = db_connector;
		this.db_url = db_url;
		this.db_user = db_user;
	}
	
	public String getDBConnector()
	{
		return this.db_connector;
	}
	
	public String getDBUrl()
	{
		return this.db_url;
	}
	
	public String getDBUser()
	{
		return this.db_user;
	}

}
