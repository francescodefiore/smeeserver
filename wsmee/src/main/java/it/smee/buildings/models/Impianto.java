package it.smee.buildings.models;

import java.util.List;

public class Impianto{
	
	private int idImpianti;	
	private int idImmobile;
	private String tipoImp;
	private String ubicazione;	
	private String dataInstallazione;
	private List<Componente> componenti;
	
	
	public String getDataInstallazione() {
		return dataInstallazione;
	}
	public void setDataInstallazione(String dataInstallazione) {
		this.dataInstallazione = dataInstallazione;
	}
	public String getUbicazione() {
		return ubicazione;
	}
	public void setUbicazione(String ubicazione) {
		this.ubicazione = ubicazione;
	}
	public int getIdImmobile() {
		return idImmobile;
	}
	public void setIdImmobile(int idImmobile) {
		this.idImmobile = idImmobile;
	}

	public String getTipoImp() {
		return tipoImp;
	}

	public void setTipoImp(String tipoImp) {
		this.tipoImp = tipoImp;
	}
	public int getIdImpianti() {
		return idImpianti;
	}
	public void setIdImpianti(int idImpianti) {
		this.idImpianti = idImpianti;
	}
	public List<Componente> getComponenti() {
		return componenti;
	}
	public void setComponenti(List<Componente> componenti) {
		this.componenti = componenti;
	}
	
}