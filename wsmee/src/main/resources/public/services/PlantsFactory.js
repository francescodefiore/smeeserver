
CREEMapp.factory('PlantsFactory', ['RESTservAddr','$resource','LoadingFactory', 


	function(RESTservAddr, $resource, LoadingFactory) 
	{

		var urlBase = '/impianti';
		
		var PlantsFactory = {};

		
		PlantsFactory.getPlants = function(idImmobile) {

			LoadingFactory.startLoading();
			

			return $resource(RESTservAddr + urlBase + "?idImmobile=" + idImmobile, {}, {
								query: {
									method: 'GET',
									isArray : true,
									cache: true,
									interceptor: {
										response: function (data) {										
											LoadingFactory.stopLoading();
										},
										responseError: function (data) {
											LoadingFactory.stopLoading();
										}
									}								
								}							
							});
		}
		
		PlantsFactory.getComponents = function(idImpianto) {

			LoadingFactory.startLoading();
			

			return $resource(RESTservAddr + urlBase + "/componenti?idImpianto=" + idImpianto, {}, {
								query: {
									method: 'GET',
									isArray : true,
									cache: true,
									interceptor: {
										response: function (data) {										
											LoadingFactory.stopLoading();
										},
										responseError: function (data) {
											LoadingFactory.stopLoading();
										}
									}								
								}							
							});
		}

		return PlantsFactory;
	}
]);



