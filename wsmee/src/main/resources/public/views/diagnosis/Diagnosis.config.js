'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/diagnosis', {
    templateUrl: 'views/diagnosis/DiagnosisView.html'
  });
}]);