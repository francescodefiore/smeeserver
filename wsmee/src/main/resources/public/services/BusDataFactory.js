CREEMapp.factory('BusDataFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/daticampo';
			var BusDataFactory = {};

			
			BusDataFactory.getSensorData = function(idImmobile) 
			{

				LoadingFactory.startLoading();
								
				return $resource(RESTservAddr + urlBase + "/sensordata?idImmobile=" + idImmobile, {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						cache: true,										
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();												
							},
							responseError: function (data) {
								console.log("error");
								LoadingFactory.stopLoading();
							}
						}					
					}							
				});
								
			}
			
			BusDataFactory.getEnergyData = function(idImmobile) 
			{

				LoadingFactory.startLoading();
								
				return $resource(RESTservAddr + urlBase + "/energydata?idImmobile=" + idImmobile, {}, 
				{
					query: {
						method: 'GET',
						isArray : true,
						cache: true,										
						interceptor: {
							response: function (data) {										
								LoadingFactory.stopLoading();												
							},
							responseError: function (data) {
								console.log("error");
								LoadingFactory.stopLoading();
							}
						}					
					}							
				});
								
			}
			
			
			
			return BusDataFactory;
	}
]);




