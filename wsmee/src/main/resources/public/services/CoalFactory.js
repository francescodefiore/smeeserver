CREEMapp.factory('CoalFactory', ['RESTservAddr','$resource','LoadingFactory','$http',

	function(RESTservAddr, $resource, LoadingFactory, $http)
	{

		var urlBase = '/coal';
		var CoalFactory = {};


		CoalFactory.getConsumptions = function(pod) {

			LoadingFactory.startLoading();
			return $resource(RESTservAddr + urlBase + "?filterBy=POD&filterParams=" + pod, {}, {
				query: {
					method: 'GET',
					isArray : true,
					cache: true,
					interceptor: {
						response: function (data) {
							LoadingFactory.stopLoading();
						},
						responseError: function (data) {
							console.log("error");
							LoadingFactory.stopLoading();
						}
					}
				}
			});

		}

		return CoalFactory;
	}
]);




