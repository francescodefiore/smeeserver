package it.smee.manutenzione.models;

public class Co2Ticket {
	private int anno;
	private int mese;
	private double kgCO2Risparmiati;
	
	
	public int getAnno() {
		return anno;
	}
	
	public void setAnno(int anno) {
		this.anno = anno;
	}
	
	public int getMese() {
		return mese;
	}
	
	public void setMese(int mese) {
		this.mese = mese;
	}
	
	public double getKgCO2Risparmiati() {
		return kgCO2Risparmiati;
	}
	
	public void setKgCO2Risparmiati(double kgCO2Risparmiati) {
		this.kgCO2Risparmiati = kgCO2Risparmiati;
	}

}