package it.smee.epi.models;

import java.util.List;


public class ModuloEpi {

	private String idImmobile;
	
	private Irradianza irradianza_invernale;
	private Irradianza irradianza_estiva;
	private DatiEdificio datiedificio;
	private List<Supesterneopache> supesterneopache;
	private List<ComponentiTrasparenti> componentitrasparenti;
	

	public DatiEdificio getDatiedificio() {
		return datiedificio;
	}

	public void setDatiedificio(DatiEdificio datiedificio) {
		this.datiedificio = datiedificio;
	}

	public List<Supesterneopache> getSupesterneopache() {
		return supesterneopache;
	}

	public void setSupesterneopache(List<Supesterneopache> supesterneopache2) {
		this.supesterneopache = supesterneopache2;
	}
	
	public List<ComponentiTrasparenti> getComponentitrasparenti() {
		return componentitrasparenti;
	}

	public void setComponentitrasparenti(List<ComponentiTrasparenti> componentitrasparenti) {
		this.componentitrasparenti = componentitrasparenti;
	}

	
	public Irradianza getIrradianza_invernale() {
		return irradianza_invernale;
	}

	public void setIrradianza_invernale(Irradianza irradianza_invernale) {
		this.irradianza_invernale = irradianza_invernale;
	}

	public Irradianza getIrradianza_estiva() {
		return irradianza_estiva;
	}

	public void setIrradianza_estiva(Irradianza irradianza_estiva) {
		this.irradianza_estiva = irradianza_estiva;
	}

	public String getIdImmobile() {
		return idImmobile;
	}

	public void setIdImmobile(String idImmobile) {
		this.idImmobile = idImmobile;
	}
	
}