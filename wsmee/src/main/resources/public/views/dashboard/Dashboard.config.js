'use strict';

CREEMapp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/dashboard', {
    templateUrl: 'views/dashboard/DashboardView.html'
  });
  
}]);