package it.smee.daticampo.models;

public class WeatherStation {
	private String idMeasure;
	private String idOsservazione;
	private String system_id;
	private String giorno;
	private String windDirection;
	private String windVelocity;
	private String temperature;
	private String humidity;
	private String solarRadiation;
	private String rainLevel;
	private String atmosphericPressure;
	
	
	public String getIdMeasure() {
		return idMeasure;
	}
	public void setIdMeasure(String idMeasure) {
		this.idMeasure = idMeasure;
	}
	public String getIdOsservazione() {
		return idOsservazione;
	}
	public void setIdOsservazione(String idOsservazione) {
		this.idOsservazione = idOsservazione;
	}
	public String getGiorno() {
		return giorno;
	}
	public void setGiorno(String giorno) {
		this.giorno = giorno;
	}
	public String getSystem_id() {
		return system_id;
	}
	public void setSystem_id(String system_id) {
		this.system_id = system_id;
	}
	public String getWindDirection() {
		return windDirection;
	}
	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}
	public String getWindVelocity() {
		return windVelocity;
	}
	public void setWindVelocity(String windVelocity) {
		this.windVelocity = windVelocity;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public String getSolarRadiation() {
		return solarRadiation;
	}
	public void setSolarRadiation(String solarRadiation) {
		this.solarRadiation = solarRadiation;
	}
	public String getRainLevel() {
		return rainLevel;
	}
	public void setRainLevel(String rainLevel) {
		this.rainLevel = rainLevel;
	}
	public String getAtmosphericPressure() {
		return atmosphericPressure;
	}
	public void setAtmosphericPressure(String atmosphericPressure) {
		this.atmosphericPressure = atmosphericPressure;
	}
}
