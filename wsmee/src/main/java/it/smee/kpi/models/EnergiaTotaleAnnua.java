package it.smee.kpi.models;

public class EnergiaTotaleAnnua extends KPI{

	private float energia_totale_annua;
	
	@Override
	public float getValue() {
		// TODO Auto-generated method stub
		return energia_totale_annua;
	}

	@Override
	public void setValue(float value) {
		// TODO Auto-generated method stub
		energia_totale_annua = value;
	}

	@Override
	public String getSQL_SELECT_column() {
		return "(SUM(gen) + SUM(feb) + sum(mar) + sum(apr) + sum(mag) + sum(giu) + sum(lug) + sum(ago) + sum(sett) + sum(ott) + sum(nov) + sum(dic)) as energia_totale_anno";
	}

	@Override
	public String getSQL_TABLE_from() {
		return "consuntivi INNER JOIN immobile ON consuntivi.podedificio=immobile.pod";
	}
	
	
	
	
	
}