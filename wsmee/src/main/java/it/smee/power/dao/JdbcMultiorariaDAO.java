package it.smee.power.dao;

import it.smee.jboxlib.JBoxDAO;

import javax.sql.DataSource;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;


public class JdbcMultiorariaDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;	
	private String filter_string = "";
	private String filter_columns = "*";
	private String aggregation_string = "";
	private String name;
	
	
	@Override
	public void print() {
		System.out.println("--- JdbcMultiorariaDAO");		
	}
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = " && (pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		s = s.concat(")");
		return s;
	}
	
	private String codiceImmobileFilter(String[] pod)
	{			
		int i=0;
		String s = " && (codiceImmobile = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR codiceImmobile = '" + pod[i] + "'");
		s = s.concat(")");
		return s;
	}
	
	private String timeFilter(String[] interval)
	{	
		
		int i=0;
		String s = " && (data BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'";
		i = i+2;
		while(i<interval.length)
		{
			s = s.concat(" OR data BETWEEN '" + interval[i] +"' AND '"+ interval[i+1]+ "'");
			i = i+2;
		}
		s = s.concat(")");
		return s;
	}
	
	private String workingdaysFilter(String[] days)
	{			
		String s = " && ( giornosettimana >= 2 && giornosettimana <= 6 )"; 		
		return s;
	}
	
	private String zoneFilter(String[] zone)
	{			
		String s = " && pod IN (SELECT pod FROM smee_immobili JOIN smee_comuni ON smee_immobili.idComune=smee_comuni.idComune WHERE zona ='" + zone[0] + "')"; 		
		return s;
	}
	
	private String turnoFilter(String[] turni)
	{			

		String s = " && pod IN (SELECT pod FROM smee_immobili JOIN smee_policies_immobili ON smee_immobili.idImmobile=smee_policies_immobili.idImmobile WHERE orarioLunVen ='" + turni[0] + "')"; 		
		return s;
	}
	
	private String nonZeroFilter()
	{			
		String s = " && ( 01_am != 0 && 02_am != 0 && 03_am != 0 && 04_am != 0 && 05_am != 0 && 06_am != 0 && 07_am != 0 && 08_am != 0 && 09_am != 0 && 10_am != 0 && 11_am != 0 && 12_pm != 0 && 01_pm != 0 && 02_pm != 0 && 03_pm != 0 && 04_pm != 0 && 05_pm != 0 && 06_pm != 0 && 07_pm != 0 && 08_pm != 0 && 09_pm != 0 && 10_pm != 0 && 11_pm != 0 && 12_am != 0 )"; 		
		return s;
	}


	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		
		switch(filter_name)
		{
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
			
			case "CODICEIMM":	
				filter_string = filter_string.concat( codiceImmobileFilter(filter_params) );				
				break;
			
			case "TIMEINTERVAL":	
				filter_string = filter_string.concat( timeFilter(filter_params) );				
				break;
				
			case "WORKINGDAYS":	
				filter_string = filter_string.concat( workingdaysFilter(filter_params) );				
				break;
				
			case "ZONACLIMATICA":	
				filter_string = filter_string.concat( zoneFilter(filter_params) );				
				break;
				
			case "TIPOTURNO":	
				filter_string = filter_string.concat( turnoFilter(filter_params) );				
				break;
				
				
			case "PROVF1F2F3":	
				filter_string = filter_string.concat( provF123Filter(filter_params) );
			break;
			
			case "ANNOF1F2F3":	
				filter_string = filter_string.concat( annoF123Filter(filter_params) );
			break;
			
			case "CLUSTERF1F2F3":	
				filter_string = filter_string.concat( clusterF123Filter(filter_params) );
			break;
			
			case "PODF1F2F3":	
				filter_string = filter_string.concat( PodF123Filter(filter_params) );
			break;
		}
		

	}
	
	private String PodF123Filter(String[] podFilter) {
		int i=0;
		String s = "&& (pod = '" + podFilter[i] + "' "; 
		while(++i<podFilter.length)
			s = s.concat(" OR pod = '" + podFilter[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String clusterF123Filter(String[] clusterFilter) {
		int i=0;
		String s = "&& (clusterEnergy = '" + clusterFilter[i] + "' "; 
		while(++i<clusterFilter.length)
			s = s.concat(" OR clusterEnergy = '" + clusterFilter[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String annoF123Filter(String[] annoFilter) {
		int i=0;
		String s = "&& (anno = '" + annoFilter[i] + "' "; 
		while(++i<annoFilter.length)
			s = s.concat(" OR anno = '" + annoFilter[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String provF123Filter(String[] provFilter) {
		int i=0;
		String s = "&& (siglaprov = '" + provFilter[i] + "' "; 
		while(++i<provFilter.length)
			s = s.concat(" OR siglaprov = '" + provFilter[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String clusterFilter(String cluster)
	{					
		String s = " && pod IN (SELECT pod FROM smee_immobili WHERE clusterEnergy ='" + cluster + "')";			
		return s;
	}
	
	private String provFilter(String prov)
	{					
		String s = " && pod IN (SELECT pod FROM smee_immobili JOIN smee_comuni ON smee_immobili.idComune=smee_comuni.idComune WHERE siglaprov ='" + prov + "')";			
		return s;
	}
	
	private String PODFilter(String POD)
	{					
		String s = " && pod='" + POD + "'";			
		return s;
	}
	
	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{
			case "PROV":	
				filter_string = filter_string.concat( provFilter(filter_params[0]) );
				filter_columns = "pod, YEAR(data) as anno, data, giornosettimana, sum(01_am) as 01_am, sum(02_am) as 02_am, sum(03_am) as 03_am, sum(04_am) as 04_am, sum(05_am) as 05_am, sum(06_am) as 06_am, sum(07_am) as 07_am, sum(08_am) as 08_am, sum(09_am) as 09_am, sum(10_am) as 10_am, sum(11_am) as 11_am, sum(12_pm) as 12_pm, sum(01_pm) as 01_pm, sum(02_pm) as 02_pm, sum(03_pm) as 03_pm, sum(04_pm) as 04_pm, sum(05_pm) as 05_pm, sum(06_pm) as 06_pm, sum(07_pm) as 07_pm, sum(08_pm) as 08_pm, sum(09_pm) as 09_pm, sum(10_pm) as 10_pm, sum(11_pm) as 11_pm, sum(12_am) as 12_am";
				break;
			
			case "CLUSTERENERGY":	
				filter_string = filter_string.concat( clusterFilter(filter_params[0]) );	
				filter_columns = "pod, YEAR(data) as anno, data, giornosettimana, sum(01_am) as 01_am, sum(02_am) as 02_am, sum(03_am) as 03_am, sum(04_am) as 04_am, sum(05_am) as 05_am, sum(06_am) as 06_am, sum(07_am) as 07_am, sum(08_am) as 08_am, sum(09_am) as 09_am, sum(10_am) as 10_am, sum(11_am) as 11_am, sum(12_pm) as 12_pm, sum(01_pm) as 01_pm, sum(02_pm) as 02_pm, sum(03_pm) as 03_pm, sum(04_pm) as 04_pm, sum(05_pm) as 05_pm, sum(06_pm) as 06_pm, sum(07_pm) as 07_pm, sum(08_pm) as 08_pm, sum(09_pm) as 09_pm, sum(10_pm) as 10_pm, sum(11_pm) as 11_pm, sum(12_am) as 12_am";
				break;
				
			case "PROV_AVG":	
				filter_string = filter_string.concat( provFilter(filter_params[0]) );
				filter_string = filter_string.concat( nonZeroFilter() );
				filter_columns = "pod, YEAR(data) as anno, data, giornosettimana, avg(01_am) as 01_am, avg(02_am) as 02_am, avg(03_am) as 03_am, avg(04_am) as 04_am, avg(05_am) as 05_am, avg(06_am) as 06_am, avg(07_am) as 07_am, avg(08_am) as 08_am, avg(09_am) as 09_am, avg(10_am) as 10_am, avg(11_am) as 11_am, avg(12_pm) as 12_pm, avg(01_pm) as 01_pm, avg(02_pm) as 02_pm, avg(03_pm) as 03_pm, avg(04_pm) as 04_pm, avg(05_pm) as 05_pm, avg(06_pm) as 06_pm, avg(07_pm) as 07_pm, avg(08_pm) as 08_pm, avg(09_pm) as 09_pm, avg(10_pm) as 10_pm, avg(11_pm) as 11_pm, avg(12_am) as 12_am";
				break;
			
			case "CLUSTERENERGY_AVG":	
				filter_string = filter_string.concat( clusterFilter(filter_params[0]) );
				filter_string = filter_string.concat( nonZeroFilter() );
				filter_columns = "pod, YEAR(data) as anno, data, giornosettimana, avg(01_am) as 01_am, avg(02_am) as 02_am, avg(03_am) as 03_am, avg(04_am) as 04_am, avg(05_am) as 05_am, avg(06_am) as 06_am, avg(07_am) as 07_am, avg(08_am) as 08_am, avg(09_am) as 09_am, avg(10_am) as 10_am, avg(11_am) as 11_am, avg(12_pm) as 12_pm, avg(01_pm) as 01_pm, avg(02_pm) as 02_pm, avg(03_pm) as 03_pm, avg(04_pm) as 04_pm, avg(05_pm) as 05_pm, avg(06_pm) as 06_pm, avg(07_pm) as 07_pm, avg(08_pm) as 08_pm, avg(09_pm) as 09_pm, avg(10_pm) as 10_pm, avg(11_pm) as 11_pm, avg(12_am) as 12_am";
				break;
				
			case "POD":	
				if(filter_params[0].compareTo("ALL") == 0)
					aggregation_string = "GROUP BY anno, pod";
				else
					filter_string = filter_string.concat( PODFilter(filter_params[0]) );	
				filter_columns = "pod, YEAR(data) as anno, data, giornosettimana, sum(01_am) as 01_am, sum(02_am) as 02_am, sum(03_am) as 03_am, sum(04_am) as 04_am, sum(05_am) as 05_am, sum(06_am) as 06_am, sum(07_am) as 07_am, sum(08_am) as 08_am, sum(09_am) as 09_am, sum(10_am) as 10_am, sum(11_am) as 11_am, sum(12_pm) as 12_pm, sum(01_pm) as 01_pm, sum(02_pm) as 02_pm, sum(03_pm) as 03_pm, sum(04_pm) as 04_pm, sum(05_pm) as 05_pm, sum(06_pm) as 06_pm, sum(07_pm) as 07_pm, sum(08_pm) as 08_pm, sum(09_pm) as 09_pm, sum(10_pm) as 10_pm, sum(11_pm) as 11_pm, sum(12_am) as 12_am";
				break;
				
			case "POD_AVG":					
				filter_string = filter_string.concat( PODFilter(filter_params[0]) );	
				filter_columns = "pod, YEAR(data) as anno, data, giornosettimana, avg(01_am) as 01_am, avg(02_am) as 02_am, avg(03_am) as 03_am, avg(04_am) as 04_am, avg(05_am) as 05_am, avg(06_am) as 06_am, avg(07_am) as 07_am, avg(08_am) as 08_am, avg(09_am) as 09_am, avg(10_am) as 10_am, avg(11_am) as 11_am, avg(12_pm) as 12_pm, avg(01_pm) as 01_pm, avg(02_pm) as 02_pm, avg(03_pm) as 03_pm, avg(04_pm) as 04_pm, avg(05_pm) as 05_pm, avg(06_pm) as 06_pm, avg(07_pm) as 07_pm, avg(08_pm) as 08_pm, avg(09_pm) as 09_pm, avg(10_pm) as 10_pm, avg(11_pm) as 11_pm, avg(12_am) as 12_am";
				break;
				
			case "POD_AVG_WEEK":					
				filter_string = filter_string.concat( PODFilter(filter_params[0]) );	
				filter_columns = "pod, giornosettimana, avg(01_am) as 01_am, avg(02_am) as 02_am, avg(03_am) as 03_am, avg(04_am) as 04_am, avg(05_am) as 05_am, avg(06_am) as 06_am, avg(07_am) as 07_am, avg(08_am) as 08_am, avg(09_am) as 09_am, avg(10_am) as 10_am, avg(11_am) as 11_am, avg(12_pm) as 12_pm, avg(01_pm) as 01_pm, avg(02_pm) as 02_pm, avg(03_pm) as 03_pm, avg(04_pm) as 04_pm, avg(05_pm) as 05_pm, avg(06_pm) as 06_pm, avg(07_pm) as 07_pm, avg(08_pm) as 08_pm, avg(09_pm) as 09_pm, avg(10_pm) as 10_pm, avg(11_pm) as 11_pm, avg(12_am) as 12_am";
				aggregation_string = "GROUP BY giornosettimana";
				break;
				
			case "CLUSTERENERGY_WEEK":	
				filter_string = filter_string.concat( clusterFilter(filter_params[0]) );
				filter_string = filter_string.concat( nonZeroFilter() );
				filter_columns = "pod, giornosettimana, avg(01_am) as 01_am, avg(02_am) as 02_am, avg(03_am) as 03_am, avg(04_am) as 04_am, avg(05_am) as 05_am, avg(06_am) as 06_am, avg(07_am) as 07_am, avg(08_am) as 08_am, avg(09_am) as 09_am, avg(10_am) as 10_am, avg(11_am) as 11_am, avg(12_pm) as 12_pm, avg(01_pm) as 01_pm, avg(02_pm) as 02_pm, avg(03_pm) as 03_pm, avg(04_pm) as 04_pm, avg(05_pm) as 05_pm, avg(06_pm) as 06_pm, avg(07_pm) as 07_pm, avg(08_pm) as 08_pm, avg(09_pm) as 09_pm, avg(10_pm) as 10_pm, avg(11_pm) as 11_pm, avg(12_am) as 12_am";
				aggregation_string = "GROUP BY giornosettimana";
				break;
				
			
		}
	}


	@Override
	public void resetFilters() {
		filter_string = "";
		filter_columns = "*";
		aggregation_string = "";
	}

	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List< Map<String, Object> > getData() {
		List<Map<String, Object>> multi = null;
		
		try {					
					
					String sql = "SELECT " + filter_columns + " " +
						     "FROM smee_datimultiorari " +
						     "WHERE 1=1 "+ filter_string + " "
						     + aggregation_string;
						     

					System.out.println("query " + sql);
					
					multi = this.jdbcTemplate.queryForList(sql);
							
		}
			
		catch (Exception e) {
			    System.out.println(" BudgetEdificio::Query non eseguita");
			    System.out.println(e);
			    
		}	
		
		return multi;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;		
	}

	public List< Map<String, Object> > FileImport(String filename) {
		List< Map<String, Object> > consumi = null;
		try {		
			String sql = "LOAD DATA LOCAL INFILE '" + filename +"' " +
			 " INTO TABLE smee_datimultiorari "+
			 " CHARACTER SET latin1 FIELDS TERMINATED BY \";\" IGNORE 1 LINES " +
			 "(pod,@data,giornosettimana,@01,@02,@03,@04,@05,@06,@07,@08,@09,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24) SET data=DATE_FORMAT(STR_TO_DATE(@data,'%d/%m/%Y'),'%Y-%m-%d'), 01_am= REPLACE(@01, ',', '.'), 02_am= REPLACE(@02, ',', '.'),03_am= REPLACE(@03, ',', '.'), 04_am= REPLACE(@04, ',', '.'),05_am= REPLACE(@05, ',', '.'), 06_am= REPLACE(@06, ',', '.'),07_am= REPLACE(@07, ',', '.'), 08_am= REPLACE(@08, ',', '.'),09_am= REPLACE(@09, ',', '.'), 10_am= REPLACE(@10, ',', '.'),11_am= REPLACE(@11, ',', '.'), 12_pm= REPLACE(@12, ',', '.'), 01_pm= REPLACE(@13, ',', '.'), 02_pm= REPLACE(@14, ',', '.'),03_pm= REPLACE(@15, ',', '.'), 04_pm= REPLACE(@16, ',', '.'),05_pm= REPLACE(@17, ',', '.'), 06_pm= REPLACE(@18, ',', '.'),07_pm= REPLACE(@19, ',', '.'), 08_pm= REPLACE(@20, ',', '.'),09_pm= REPLACE(@21, ',', '.'), 10_pm= REPLACE(@22, ',', '.'),11_pm= REPLACE(@23, ',', '.'), 12_am= REPLACE(@24, ',', '.')";

			System.out.println("query " + sql);
			this.jdbcTemplate.execute(sql);
		}
		catch (Exception e) {
		    System.out.println(" FileImport::Query non eseguita");
		    System.out.println(e);    
		}	
		
		return consumi;
	}

	public List<Map<String, Object>> getPodF1F2F3() {
		List< Map<String, Object> > consumiPod = null;
		try {		
			String sql = "SELECT * FROM smee_consumi_totali_mensili_f1f2f3_pod " +
						 "WHERE 1=1 "+ filter_string ;
			
			//System.out.println("query " + sql);
			consumiPod = this.jdbcTemplate.queryForList(sql);
		}
		catch (Exception e) {
		    System.out.println(" getPodF1F2F3::Query non eseguita");
		    System.out.println(e);    
		}	
		return consumiPod;
	}
	
	public List<Map<String, Object>> getPodF1F2F3Anno(String anno) {
		List< Map<String, Object> > consumiPod = null;
		try {		
			String sql = "SELECT * FROM smee_consumi_totali_mensili_f1f2f3_pod " +
						 "WHERE 1=1 "
						 +" and anno = " + anno + " "
						 + filter_string ;
			
			System.out.println("query " + sql);
			consumiPod = this.jdbcTemplate.queryForList(sql);
		}
		catch (Exception e) {
		    System.out.println(" getPodF1F2F3Anno::Query non eseguita");
		    System.out.println(e);    
		}	
		return consumiPod;
	}

	
	
	public List<Map<String, Object>> getClusterF1F2F3() {
		List< Map<String, Object> > consumiPod = null;
		try {		
			String sql = "SELECT * FROM smee_consumi_totali_mensili_f1f2f3_cluster " +
						 "WHERE 1=1 "+ filter_string ;
			
			//System.out.println("query " + sql);
			consumiPod = this.jdbcTemplate.queryForList(sql);
		}
		catch (Exception e) {
		    System.out.println(" getClusterF1F2F3::Query non eseguita");
		    System.out.println(e);    
		}	
		return consumiPod;
	}
	
	public List<Map<String, Object>> getClusterF1F2F3Anno(String anno) {
		List< Map<String, Object> > consumiPod = null;
		try {		
			String sql = "SELECT * FROM smee_consumi_totali_mensili_f1f2f3_cluster " +
						 "WHERE 1=1 "
						 +" and anno = " + anno + " "
						 + filter_string ;
			
			//System.out.println("query " + sql);
			consumiPod = this.jdbcTemplate.queryForList(sql);
		}
		catch (Exception e) {
		    System.out.println(" getClusterF1F2F3Anno::Query non eseguita");
		    System.out.println(e);    
		}	
		return consumiPod;
	}

	public List<Map<String, Object>> getProvinceF1F2F3() {
		List< Map<String, Object> > consumiPod = null;
		try {		
			String sql = "SELECT * FROM smee_consumi_totali_mensili_f1f2f3_province " +
						 "WHERE 1=1 "+ filter_string ;
			
			//System.out.println("query " + sql);
			consumiPod = this.jdbcTemplate.queryForList(sql);
		}
		catch (Exception e) {
		    System.out.println(" getProvinceF1F2F3::Query non eseguita");
		    System.out.println(e);    
		}	
		return consumiPod;
	}
	
	public List<Map<String, Object>> getProvinceF1F2F3Anno(String anno) {
		List< Map<String, Object> > consumiPod = null;
		try {		
			String sql = "SELECT * FROM smee_consumi_totali_mensili_f1f2f3_province " +
						 "WHERE 1=1 "
						 +" and anno = " + anno + " "
						 + filter_string ;
			
			//System.out.println("query " + sql);
			consumiPod = this.jdbcTemplate.queryForList(sql);
		}
		catch (Exception e) {
		    System.out.println(" getProvinceF1F2F3Anno::Query non eseguita");
		    System.out.println(e);    
		}	
		return consumiPod;
	}
	
	public List<Map<String, Object>> getDatiAcotel() {
		List< Map<String, Object> > acotel = null;
		try {		
			String sql = "SELECT " + filter_columns + " " +
				     "FROM smee_datimultiorari_acotel " +
				     "WHERE 1=1 "+ filter_string + " "
				     + aggregation_string;
				     

			System.out.println("query " + sql);
			
			acotel = this.jdbcTemplate.queryForList(sql);
		}
		catch (Exception e) {
		    System.out.println(" getDatiAcotel::Query non eseguita");
		    System.out.println(e);    
		}	
		return acotel;
	}
}