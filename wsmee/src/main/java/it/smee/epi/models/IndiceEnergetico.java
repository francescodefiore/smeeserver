package it.smee.epi.models;

public class IndiceEnergetico {
	
	private String simbolo;
	private String nome;
	private float valore;
	private String unitamisura;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public float getValore() {
		return valore;
	}
	
	public void setValore(float valore) {
		this.valore = valore;
	}

	public String getUnitamisura() {
		return unitamisura;
	}

	public void setUnitamisura(String unitamisura) {
		this.unitamisura = unitamisura;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
}
