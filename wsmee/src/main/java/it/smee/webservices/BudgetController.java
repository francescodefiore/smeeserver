package it.smee.webservices;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import it.smee.budget.dao.JdbcBudgetDAO;
import it.smee.jboxlib.*;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;


@Controller
public class BudgetController {
	
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO budgetDAO = (JBoxDAO)context.getBean("budgetDAO");
		
    
	public BudgetController() {
		System.out.println("==== /BudgetController ====");
	} 
	
	@RequestMapping(value = "/Budget/delete/", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody void delete_budget(@PathVariable String idedificio, @PathVariable String anno)
    {
		System.out.println("==== /budget/delete/ new request ====");  
    }

	
	@RequestMapping(value = "/Budget", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody List< Map<String, Object> > budget(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,    		
    		@RequestParam(value="groupByParams", required=false) List<String> group_params)
	{
		budgetDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				budgetDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )	
			for(int i=0; i<groups.split(";").length; i++)
				budgetDAO.addAggregationFilter( groups.split(";")[i], group_params.get(i).split(";") );
		
		
		return budgetDAO.getData();
		
	}
	
	@RequestMapping(value = "/Budget/anno", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody List< Map<String, Object> > budgetAnno(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,    		
    		@RequestParam(value="groupByParams", required=false) List<String> group_params,
    		@RequestParam(value="year", required=false) String year)
	{
		budgetDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				budgetDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )	
			for(int i=0; i<groups.split(";").length; i++)
				budgetDAO.addAggregationFilter( groups.split(";")[i], group_params.get(i).split(";") );
		
		
		return ((JdbcBudgetDAO)budgetDAO).getDataAnno(year);
		
	}
	
	
	
	@RequestMapping(value="/Budget/upload", method=RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    public void handleFileUpload(@RequestParam("file") MultipartFile file){
		System.out.println("==== /budget/upload new request ====");
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Calendar c = Calendar.getInstance();
                String directory = "./imported/budget";
                boolean success=(new File(directory)).mkdirs();		           
	            
	            	String name = directory + "/"+ c.getTimeInMillis()+"_budget.csv";            
	            	BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name)));
	            	stream.write(bytes);
	            	stream.close();
	            	((JdbcBudgetDAO)budgetDAO).FileImport(name);
	            
                
            } catch (Exception e) { }            
        }
	}
	
        

	
}