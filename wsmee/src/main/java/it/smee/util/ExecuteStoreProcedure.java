package it.smee.util;


import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.object.StoredProcedure;

public class ExecuteStoreProcedure extends StoredProcedure {
	
	String spName = "";
	DataSource dataSource;
	
	public ExecuteStoreProcedure()
	{}
	
	public ExecuteStoreProcedure(DataSource dataSource, String spName)
	{
		super(dataSource,spName);
	}
	
	public Map<String,Object> ExcecuteNoParameters()
	{
		Map<String,Object> results = super.execute();
		return results;
	}
	

}
