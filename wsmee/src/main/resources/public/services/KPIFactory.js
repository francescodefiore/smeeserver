
CREEMapp.factory('KPIFactory', ['RESTservAddr','$resource','LoadingFactory','$http', 

	function(RESTservAddr, $resource, LoadingFactory, $http) 
	{

			var urlBase = '/kpi';
			var KPIFactory = {};
			
			KPIFactory.getCurrentMonthKPI = function(idImmobile) 
			{
				var month = moment().add(1, 'month').subtract(1, 'year').date(0).format('YYYY-MM-DD');				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=IMMOBILE&filterParams=" + idImmobile + "&filterBy=CADENZA&filterParams=M&filterBy=KPI&filterParams=ReqF001;ReqF002&filterBy=TIME&filterParams=" + month, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			KPIFactory.getYearlyKPI = function(idImmobile, year) 
			{				
				LoadingFactory.startLoading();
				
				var date_string = "";
				var startDate;
				
				for (var i=0; i<12; i++)
				{
					startDate = moment([year, i]);
					date_string +=  moment(startDate).endOf('month').format('YYYY-MM-DD') + ";";
				}
				
				return $resource(RESTservAddr + urlBase + "?filterBy=IMMOBILE&filterParams=" + idImmobile + "&filterBy=CADENZA&filterParams=M&filterBy=KPI&filterParams=ReqF001;ReqF002&filterBy=TIME&filterParams=" + date_string, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: false,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			KPIFactory.getBuildingKPI = function(idImmobile) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=IMMOBILE&filterParams=" + idImmobile, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: true,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
			
			KPIFactory.getKPI = function(date, kpi, typeKpi, clusters, superficie_dss) 
			{
				
				LoadingFactory.startLoading();
				
				return $resource(RESTservAddr + urlBase + "?filterBy=CADENZA&filterParams=" + typeKpi + "&filterBy=KPI&filterParams=" + kpi + "&filterBy=TIME&filterParams=" + date + "&filterBy=CLUSTER&filterParams=" + clusters + "&filterBy=SUPDSS&filterParams=" + superficie_dss, {}, 
								{
									query: {
										method: 'GET',
										isArray : true,
										cache: false,										
										interceptor: {
											response: function (data) {										
												LoadingFactory.stopLoading();
											},
											responseError: function (data) {
												LoadingFactory.stopLoading();
											}
										}							
									}							
								});
								
			}
						
			
			KPIFactory.getTipoKPI = function() 
			{
				
				LoadingFactory.startLoading();
				urlBase = '/kpi/tipo';
				return $resource(RESTservAddr + urlBase, {}, 
					{
						query: {
							method: 'GET',
							isArray : true,
							cache: true,										
							interceptor: {
								response: function (data) {										
									LoadingFactory.stopLoading();
								},
								responseError: function (data) {
									LoadingFactory.stopLoading();
								}
							}							
						}							
					});
								
			}
						
			
			return KPIFactory;
	}
]);


