package it.smee.webservices;

import it.smee.budget.dao.JdbcBudgetDAO;
import it.smee.budget.dao.JdbcConsuntiviDAO;
import it.smee.buildings.dao.JdbcEdificiDAO;
import it.smee.jboxlib.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.sql.DataSource;


@Controller
public class CustomController {
	
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");	
	private static final DataSource datasource = (DataSource) context.getBean("mysqlDataSource");
		
	
    
	public CustomController() {
		System.out.println("==== /CustomControllers ====");
	}
	
	@RequestMapping(value = "/aggregator", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody JBox custom( 
			@RequestParam(value="service", required=true) List<String> services,
			@RequestParam(value="filterBy", required=false) List<String> conditionalFilters,			
			@RequestParam(value="filterParams", required=false) List<String> conditionalParams,
			@RequestParam(value="groupBy", required=false) List<String> aggregationFilters,			
			@RequestParam(value="groupByParams", required=false) List<String> aggregationParams)
	{
		
		JBoxComposite root =  new JBoxComposite("root");
		JBoxDAO serviceDAO = null;
		
		System.out.println("SERVICES" + services);
		System.out.println("FILTERS" + conditionalFilters);
		System.out.println("FILTERPARAMS" + conditionalParams);
		
		
		for(String service : services)
		{
			try
			{
				switch(service)
				{
					case "budget":
						serviceDAO = new JdbcBudgetDAO();
					break;
					
					case "consuntivi":
						serviceDAO = new JdbcConsuntiviDAO();
					break;
					
					case "immobili":
						serviceDAO = new JdbcEdificiDAO();
					break;
					
				}
				
				serviceDAO.setName(service);
				serviceDAO.setDataSource(datasource);
				serviceDAO.resetFilters();				
				
				
				for(int i=0; i<conditionalFilters.size(); i++)
				{					
						String[] CONDITIONAL_FILTERS = conditionalFilters.get(i).split(";");
						for(int j=0; j<CONDITIONAL_FILTERS.length; j++)						
							serviceDAO.addConditionalFilter(CONDITIONAL_FILTERS[j], conditionalParams.get(i).split(";") );

				}
				
				root.add( serviceDAO );
				
				
			}catch(Exception e)
			{
				System.out.println("==== /CustomController ==== Impossibile aggiungere web-service " + service);
				System.out.println(e);
			}
		}
		
		root.print();
		return root;	
	}
	
		
}