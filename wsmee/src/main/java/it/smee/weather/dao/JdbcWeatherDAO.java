package it.smee.weather.dao;

import it.smee.jboxlib.JWeather;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import java.lang.Override;
import java.util.List;
import java.util.Map;


public class JdbcWeatherDAO implements JWeather {

    private JdbcTemplate jdbcTemplate;

    @Override
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List< Map<String, Object> > getData(String city, String dateFrom, String dateTo) {

        List< Map<String, Object> > weather = null;

        String sql = "SELECT *, DATE(Time) as date_w, TIME(Time) as time_w FROM smee_meteo as m inner join smee_comuni as c " +
                "on m.IdComune=c.idComune " +
                "where nome = '" + city + "' and Time >= '" + dateFrom + "' and Time <= '" + dateTo + "' ORDER BY Time; ";


        System.out.println("query " + sql);


        try {
            weather = this.jdbcTemplate.queryForList(sql);

        } catch(Exception e) {
            System.out.println("JdbcEdificiDAO::getData()::Query non eseguita");
            System.out.println(e);
        }

        return weather;
    }




}