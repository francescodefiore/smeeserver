package it.smee.buildings.models;

public class Componente{
	
	private int idComp;	
	private String tipoComp;
	private String marcaComp;
	private String modComp;	
	private String valoreComp;
	private String dataInstallazione;
	private int oreFunzionamento;
	private int numeroComponenti;
	private int inManutenzione;
	
	
	public int getIdComp() {
		return idComp;
	}
	public void setIdComp(int idComp) {
		this.idComp = idComp;
	}
	
	public String getMarcaComp() {
		return marcaComp;
	}
	public void setMarcaComp(String marcaComp) {
		this.marcaComp = marcaComp;
	}
	public String getModComp() {
		return modComp;
	}
	public void setModComp(String modComp) {
		this.modComp = modComp;
	}
	public String getValoreComp() {
		return valoreComp;
	}
	public void setValoreComp(String valoreComp) {
		this.valoreComp = valoreComp;
	}
	public String getDataInstallazione() {
		return dataInstallazione;
	}
	public void setDataInstallazione(String dataInstallazione) {
		this.dataInstallazione = dataInstallazione;
	}
	public int getOreFunzionamento() {
		return oreFunzionamento;
	}
	public void setOreFunzionamento(int oreFunzionamento) {
		this.oreFunzionamento = oreFunzionamento;
	}
	public int getNumeroComponenti() {
		return numeroComponenti;
	}
	public void setNumeroComponenti(int numeroComponenti) {
		this.numeroComponenti = numeroComponenti;
	}
	public int getInManutenzione() {
		return inManutenzione;
	}
	public void setInManutenzione(int inManutenzione) {
		this.inManutenzione = inManutenzione;
	}
	public String getTipoComp() {
		return tipoComp;
	}
	public void setTipoComp(String tipoComp) {
		this.tipoComp = tipoComp;
	}
}