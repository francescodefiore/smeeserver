package it.smee.webservices;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import it.smee.jboxlib.JBoxDAO;

import java.util.List;
import java.util.Map;




@Controller
public class DashboardController {
	
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");	
	
	private static final JBoxDAO dashboardDAO = (JBoxDAO) context.getBean("dashboardDAO");
    
	public DashboardController() {
		System.out.println("==== /DashboardController ====");
	} 
	
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET, headers="Accept=application/json")
	public @ResponseBody List< Map<String, Object> > indici(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,
    		@RequestParam(value="groupByParams", required=false) List<String> group_params,
    		@RequestParam(value="orderBy", required=false) String sortings)
	{
		System.out.println("==== /dashboard new request ====");
		
		dashboardDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				dashboardDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

		
		return dashboardDAO.getData();
	}
	
}
	