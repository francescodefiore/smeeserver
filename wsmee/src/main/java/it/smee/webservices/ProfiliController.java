package it.smee.webservices;


import it.smee.jboxlib.JBoxDAO;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ProfiliController {
	
	private static final ApplicationContext context =	new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO profiliDAO = (JBoxDAO) context.getBean("profiliDAO");
	
	public ProfiliController() 
	{
		System.out.println("==== /ControllerProfili ====");
	}
	
	@RequestMapping(value = "/profili", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> multi( 
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params)
    {  
	
		profiliDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				profiliDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		

		return profiliDAO.getData();	
    }
	
	
}
