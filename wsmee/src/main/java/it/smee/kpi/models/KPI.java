package it.smee.kpi.models;


public abstract class KPI{
		
	private String pod;
	private String comune;
	private String provincia;
	private String indirizzo;
	private String clusterEnergy;
	private String time_interval_from;
	private String time_interval_to;
	
	
	public abstract float getValue();
	public abstract void  setValue(float value);
	
	public abstract String getSQL_SELECT_column();
	public abstract String getSQL_TABLE_from();
	
	
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public String getComune() {
		return comune;
	}
	public void setComune(String comune) {
		this.comune = comune;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getClusterEnergy() {
		return clusterEnergy;
	}
	public void setClusterEnergy(String clusterEnergy) {
		this.clusterEnergy = clusterEnergy;
	}
	public String getTime_interval_from() {
		return time_interval_from;
	}
	public void setTime_interval_from(String time_interval_from) {
		this.time_interval_from = time_interval_from;
	}
	public String getTime_interval_to() {
		return time_interval_to;
	}
	public void setTime_interval_to(String time_interval_to) {
		this.time_interval_to = time_interval_to;
	}	
	
	
}