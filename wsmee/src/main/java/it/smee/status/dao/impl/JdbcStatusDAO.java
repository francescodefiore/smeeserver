package it.smee.status.dao.impl;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import it.smee.status.dao.StatusDAO;
import it.smee.status.models.*;


public class JdbcStatusDAO implements StatusDAO  {


	private JdbcTemplate jdbcTemplate;
	private Status s;
	
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);	
		 try {
			 	 s = new Status(jdbcTemplate.getDataSource().getConnection().toString().split(",")[2],
			 					jdbcTemplate.getDataSource().getConnection().toString().split(",")[0],
			 					jdbcTemplate.getDataSource().getConnection().toString().split(",")[1]);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}

	@Override
	public Status retrieveData() {
		// TODO Auto-generated method stub
		return s;
	}


}
