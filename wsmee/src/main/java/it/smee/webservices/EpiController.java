package it.smee.webservices;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import it.smee.buildings.dao.JdbcEdificiDAO;
import it.smee.buildings.dao.JdbcPoliciesDAO;
import it.smee.buildings.models.Edificio;
import it.smee.buildings.models.Policies;
import it.smee.epi.dao.JdbcModuloEpiDAO;
import it.smee.epi.models.*;
import it.smee.jboxlib.JBoxDAO;
import it.smee.kpi.dao.JdbcKpiDAO;

import java.util.List;
import java.util.Map;




@Controller
public class EpiController {
	
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");	
	
	private static final JBoxDAO indiciDAO = (JBoxDAO) context.getBean("epiDAO");
	private static final JBoxDAO epiDAO = (JBoxDAO) context.getBean("moduloepiDAO");
    
	public EpiController() {
		System.out.println("==== /EpiController ====");
	} 
	
	@RequestMapping(value = "/indici", method = RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody List< Map<String, Object> > indici(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,
    		@RequestParam(value="groupByParams", required=false) List<String> group_params)
	{
		System.out.println("==== /indici new request ====");
		
		indiciDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				indiciDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )	
			for(int i=0; i<groups.split(";").length; i++)
				indiciDAO.addAggregationFilter( groups.split(";")[i], group_params.get(i).split(";") );

		
		return indiciDAO.getData();
	}
	
	/*
	@RequestMapping(value = "/epi", method = RequestMethod.GET,headers="Accept=application/json")	
	public @ResponseBody List<Map<String, Object>> query(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups)
	{
		System.out.println("==== /epi ====");
		
		epiDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				epiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		return epiDAO.getData();
	}
	
	
	@RequestMapping(value = "/epi/update", method = RequestMethod.POST)
	public ResponseEntity<ModuloEpi> update(@RequestBody ModuloEpi modulo) {

		System.out.println("==== /epi/update ====");
	
		
		((JdbcModuloEpiDAO)epiDAO).AggiornaModuloEpi(modulo);
		return new ResponseEntity<ModuloEpi>(modulo, HttpStatus.OK);
	}
	*/

	@RequestMapping(value = "/epi/moduloepi", method = RequestMethod.GET)
	public @ResponseBody ModuloEpi query(@RequestParam(value="idImmobile", required=true) Integer idimmobile) {
		
		return ((JdbcModuloEpiDAO)epiDAO).getModuloEpi(idimmobile);
	}
	
	
	@RequestMapping(value = "/epi/moduloepi", method = RequestMethod.POST)
	public @ResponseBody ModuloEpi create(@RequestBody ModuloEpi modulo) {

		System.out.println("==== /epi POST ====");
			
		((JdbcModuloEpiDAO)epiDAO).AggiornaModuloEpi(modulo);
		
		return modulo;
	}
	
	
	@RequestMapping(value = "/epi/datigenerali", method = RequestMethod.GET)
	public @ResponseBody DatiGenerali query() {
		
		return ((JdbcModuloEpiDAO)epiDAO).getDatiGenerali();
	}
	
	@RequestMapping(value = "/epi/datigenerali", method = RequestMethod.POST)
	public @ResponseBody DatiGenerali create(@RequestBody DatiGenerali dati) {

		System.out.println("==== /epi/datigenerali ====");
			
		((JdbcModuloEpiDAO)epiDAO).AggiornaDatiGenerali(dati);
		
		return dati;
	}
	
	@RequestMapping(value = "/epi/getCOP", method = RequestMethod.GET,headers="Accept=application/json")
	public  @ResponseBody List<Map<String, Object>> getCOP(
			@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params) 
	{
		System.out.println("==== /epi/getCOP ====");
		epiDAO.resetFilters();
		
		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				epiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		return ((JdbcModuloEpiDAO)epiDAO).getCOP();
	}

	
	@RequestMapping(value = "/epi/indici", method = RequestMethod.GET,headers="Accept=application/json")
	 public  @ResponseBody List<Map<String, Object>> getEpiValue(
	   @RequestParam(value="filterBy", required=false) List<String> filters,
	      @RequestParam(value="filterParams", required=false) List<String> filter_params) 
	 {
	  System.out.println("==== /epi/indici ====");
	  epiDAO.resetFilters();
	  
	  if( (filters!=null) && (filter_params!=null))
	   for(int i=0; i<filters.size(); i++)    
		   epiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
	  
	  return ((JdbcModuloEpiDAO) epiDAO).getEpiValue();
	 }

}