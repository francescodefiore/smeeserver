package it.smee.buildings.models;

public class NumeroComponenti {
	
	private int numeroComp;
	private int idTipoImp;
	private String tipoImp;
	private Edificio edificio;
	
	public int getNumeroComp() {
		return numeroComp;
	}
	public void setNumeroComp(int numeroComp) {
		this.numeroComp = numeroComp;
	}
	public int getIdTipoImp() {
		return idTipoImp;
	}
	public void setIdTipoImp(int idTipoImp) {
		this.idTipoImp = idTipoImp;
	}
	public String getTipoImp() {
		return tipoImp;
	}
	public void setTipoImp(String tipoImp) {
		this.tipoImp = tipoImp;
	}
	public Edificio getEdificio() {
		return edificio;
	}
	public void setEdificio(Edificio edificio) {
		this.edificio = edificio;
	}

}
