package it.smee.kpi.dao;

import it.smee.epi.models.ComponentiTrasparenti;
import it.smee.jboxlib.JBoxDAO;
import it.smee.kpi.models.KPI;
import it.smee.kpi.models.PiccoPotenzaAssorbita;
import it.smee.util.ExecuteStoreProcedure;

import javax.sql.DataSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcKpiDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;
	private ExecuteStoreProcedure sp;
	private String spName = "provaSp";
	
	private String filter_string = "";
	
	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{		
			case "ZONA":	
				filter_string = filter_string.concat( zonaFilter(filter_params) );				
				break;
				
			case "CLUSTER":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );				
				break;	
				
			case "IMMOBILE":	
				filter_string = filter_string.concat( immobileFilter(filter_params) );				
				break;
				
			case "KPI":	
				filter_string = filter_string.concat( kpiFilter(filter_params) );				
				break;
				
			case "TIME":	
				filter_string = filter_string.concat( timeFilter(filter_params) );				
				break;
				
			case "CADENZA":	
				filter_string = filter_string.concat( cadenzaFilter(filter_params) );				
				break;
				
			case "ALERTBUILD":	
				filter_string = filter_string.concat( alertbuildFilter(filter_params) );				
				break;
				
			case "ALERTKPI":	
				filter_string = filter_string.concat( alertkpiFilter(filter_params) );				
				break;
				
			case "ALERTDATEKPI":	
				filter_string = filter_string.concat( alertdatekpiFilter(filter_params) );				
				break;
				
			case "ALERTPERIODOKPI":
				filter_string = filter_string.concat( alertperiodokpiFilter(filter_params) );				
				break;
			case "SIGLAPROV":	
				filter_string = filter_string.concat(siglaprovFilter(filter_params));
				break;
			case "SUPsmee":	
				filter_string = filter_string.concat(supsmeeFilter(filter_params));
				break;
		}
		
	}

	private String alertperiodokpiFilter(String[] alertperiodokpi) {
		int i=0;
		String s = "&& (a.periodoRiferimento like '%" + alertperiodokpi[i] + "%' "; 
		while(++i<alertperiodokpi.length)
			s = s.concat(" OR a.periodoRiferimento like  '%" + alertperiodokpi[i] + "%' ");
		s = s.concat(")");
		return s;
	}

	private String alertdatekpiFilter(String[] alertdatakpi) {
		int i=0;
		String s = "&& (a.data like '%" + alertdatakpi[i] + "%' "; 
		while(++i<alertdatakpi.length)
			s = s.concat(" OR a.data like  '%" + alertdatakpi[i] + "%' ");
		s = s.concat(")");
		return s;
	}

	private String alertkpiFilter(String[] alertkpi) {
		int i=0;
		String s = "&& (a.idKpi = '" + alertkpi[i] + "' "; 
		while(++i<alertkpi.length)
			s = s.concat(" OR a.idKpi = '" + alertkpi[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String alertbuildFilter(String[] alertbuild) {
		int i=0;
		String s = "&& (idImmobile = '" + alertbuild[i] + "' "; 
		while(++i<alertbuild.length)
			s = s.concat(" OR idImmobile = '" + alertbuild[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String cadenzaFilter(String[] cadenza) {
		int i=0;
		String s = "&& (tipoKpi = '" + cadenza[i] + "' "; 
		while(++i<cadenza.length)
			s = s.concat(" OR tipoKpi = '" + cadenza[i] + "' ");
		s = s.concat(")");
		return s;
	}
	
	private String timeFilter(String[] time) {
		int i=0;
		String s = "&& (periodoRiferimento = '" + time[i] + "' "; 
		while(++i<time.length)
			s = s.concat(" OR periodoRiferimento = '" + time[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String kpiFilter(String[] kpi) {
		int i=0;
		String s = "&& (refKpi = '" + kpi[i] + "' "; 
		while(++i<kpi.length)
			s = s.concat(" OR refKpi = '" + kpi[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String immobileFilter(String[] idImmobile) {
		int i=0;
		String s = "&& (idImmobili = '" + idImmobile[i] + "' "; 
		while(++i<idImmobile.length)
			s = s.concat(" OR idImmobili = '" + idImmobile[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String clusterFilter(String[] cluster) {
		int i=0;
		String s = "&& (clusterEnergy = '" + cluster[i] + "' "; 
		while(++i<cluster.length)
			s = s.concat(" OR clusterEnergy = '" + cluster[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String zonaFilter(String[] zona) {
		int i=0;
		String s = "&& (zona = '" + zona[i] + "' "; 
		while(++i<zona.length)
			s = s.concat(" OR zona = '" + zona[i] + "' ");
		s = s.concat(")");
		return s;
	}

	private String siglaprovFilter(String[] siglaprov) {
		int i=0;
		String s = "&& (siglaprov = '" + siglaprov[i] + "' "; 
		while(++i<siglaprov.length)
			s = s.concat(" OR siglaprov = '" + siglaprov[i] + "' ");
		s = s.concat(")");
		return s;
	}
	
	private String supsmeeFilter(String[] supsmee) {
		
		String s = "";
		if (supsmee.length==1) 
			if (supsmee[0].compareTo("ALL") == 0)
				s = ""; 
			else 
				s = "&& (superficieUtile >= " + supsmee[0] + ") "; 		
		else 
			s = "&& (superficieUtile >= " + supsmee[0] + " AND superficieUtile < " + supsmee[1] + ") "; 
		
		return s;
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		if(filter_name.compareTo("IMMOBILE") == 0)
			filter_string = filter_string.concat(immobileFilter(filter_params));		
		if(filter_name.compareTo("ZONA") == 0)
			filter_string = filter_string.concat(zonaFilter(filter_params));
		if(filter_name.compareTo("CLUSTER") == 0)
			filter_string = filter_string.concat(clusterFilter(filter_params));
		if(filter_name.compareTo("KPI") == 0)
			filter_string = filter_string.concat(kpiFilter(filter_params));		
		if(filter_name.compareTo("TIME") == 0)
			filter_string = filter_string.concat(timeFilter(filter_params));
		if(filter_name.compareTo("CADENZA") == 0)
			filter_string = filter_string.concat(cadenzaFilter(filter_params));
		if(filter_name.compareTo("ALERTBUILD") == 0)
			filter_string = filter_string.concat(alertbuildFilter(filter_params));
		if(filter_name.compareTo("ALERTKPI") == 0)
			filter_string = filter_string.concat(alertkpiFilter(filter_params));
		if(filter_name.compareTo("ALERTDATEKPI") == 0)
			filter_string = filter_string.concat(alertdatekpiFilter(filter_params));
		if(filter_name.compareTo("SIGLAPROV") == 0)
			filter_string = filter_string.concat(siglaprovFilter(filter_params));
		if(filter_name.compareTo("SUPsmee") == 0)
			filter_string = filter_string.concat(supsmeeFilter(filter_params));
	}

	@Override
	public void resetFilters() {
		filter_string = "";
	}

	@Override
	public List<Map<String, Object>> getData() {
		
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		sp = new ExecuteStoreProcedure(this.jdbcTemplate.getDataSource(),spName);
		list.add(sp.ExcecuteNoParameters());
		return list;
	}

	public List<Map<String, Object>> getTolleranza() {
		
		List< Map<String, Object> > tolleranze = null;
		String sql = "SELECT nomeBenchmark, descrizioneBenchmark, zb.idZoneBench as id, cluster, zona, tolleranza " +
			     	 "FROM smee_creem.smee_benchmark b left join smee_zone_benchmark zb on b.idBenchmark=zb.idBenchmark " +
			     	 "WHERE 1=1 "+ filter_string;
		try{
			tolleranze = this.jdbcTemplate.queryForList(sql);
				       				               				 
		}catch(Exception e)
		{
			 System.out.println("JdbcKPIDAO::getTolleranza()::Query non eseguita " + e);		
		}
		return tolleranze;
	}

	
	public void updateTolleranza(List<Map<String, Object>> listaTolleranza) {
		
		String sql = "";
		for (Map<String, Object> t : listaTolleranza) {
			sql ="UPDATE  smee_zone_benchmark SET tolleranza=? WHERE idZoneBench=? AND zona=? AND cluster=?";
			this.jdbcTemplate.update(sql, t.get("tolleranza"), t.get("id"), t.get("zona"), t.get("cluster"));   								
		}
		return;
	}

	
	
	public List<Map<String, Object>> getKpi() {
		List<Map<String, Object>> kpi = new ArrayList<Map<String, Object>>();
		String sql = "SELECT distinct k.*, ki.idKpiImmobili FROM (smee_kpi k join smee_kpi_immobili ki on k.idKpi=ki.idKpi) " +		
					 "WHERE 1=1 "+ filter_string;
		try{
			kpi = this.jdbcTemplate.queryForList(sql);		
		}
		catch(Exception e)
		{
			 System.out.println("JdbcKPIDAO::getKpi()::Query non eseguita " + e);		
		}
		return kpi;
	}

	
	
	public List<Map<String, Object>> getKpiValue() {
		List<Map<String, Object>> kpi_bench = new ArrayList<Map<String, Object>>();
		String sql = "Select  i.*, ki.idImmobili as idImmobile, kv.periodoRiferimento as data, k.nomeKpi as Kpi, k.tipoKpi as tipo , k.descrizione, k.unitaMisura, kv.value as valoreKpi,  round(bv.value,2) as valoreBench, CASE WHEN zb.tolleranza is null THEN 0  ELSE zb.tolleranza END AS tolleranza  "+
			  " from smee_kpi as k left join smee_kpi_immobili as ki on k.idKpi= ki.idKpi  "+
			  " left join smee_immobili as i on ki.idImmobili=i.idImmobile " +
			  " left join smee_kpi_value kv  on kv.idKpiImmobili = ki.idKpiImmobili   "+
			  " left join smee_kpi_zone_benchmark kzb  on kzb.idKpi = ki.idKpiImmobili  "+
			  " left join smee_zone_benchmark zb  on zb.idZoneBench = kzb.idZoneBench  "+
			  " left join smee_benchmark_value bv on bv.idZoneBenchmark = zb.idZoneBench  "+
			  " left join smee_benchmark b on b.idBenchmark=zb.idBenchmark where bv.data=kv.periodoRiferimento "+ filter_string + " order by  tipoKpi, data ";

		try{
			kpi_bench = this.jdbcTemplate.queryForList(sql);	
			System.out.println("JdbcKPIDAO::getKpiValue()::  " + sql);		
		}
		catch(Exception e)
		{
			 System.out.println("JdbcKPIDAO::getKpiValue()::Query non eseguita " + e);		
		}

		return kpi_bench;
	}
	
	public List<Map<String, Object>> getAlerts() {
		List<Map<String, Object>> alerts = new ArrayList<Map<String, Object>>();
		
		String sql = "select smee_allert.*, clusterEnergy as cluster, siglaprov as prov from smee_allert"+
				 " join smee_immobili using(idImmobile) join smee_comuni using(idComune)"+
			     " where 1=1  " + filter_string + "  order by periodoRiferimento DESC";		
			
		try{
			alerts = this.jdbcTemplate.queryForList(sql);	
			System.out.println(" JdbcKPIDAO::getAlerts()::  " + sql);		
		}
		catch(Exception e)
		{
			 System.out.println("JdbcKPIDAO::getAlerts()::Query non eseguita " + e);		
		}

		return alerts;
	}
	

	
	
	public List<Map<String, Object>> getKpiAlert() {
		List<Map<String, Object>> kpi_alert = new ArrayList<Map<String, Object>>();
		String sql = "SET lc_time_names = 'it_IT';";
		this.jdbcTemplate.execute(sql);
		sql = "SELECT distinct i.codice, clusterEnergy, siglaprov, tipoAllert, a.idAllert, a.allert,DATE_FORMAT(a.data,'%d-%m-%Y %h:%i %p') as data, "+
				"DATE_FORMAT(a.periodoRiferimento,'%M %Y ') as periodoRiferimento, k.descrizione, kv.value as KPI, bv.value as Bench, zb.tolleranza, k.unitaMisura "+
					"FROM smee_allert a inner join smee_kpi k on a.idKpi=k.idKpi inner join smee_immobili i on a.idImmobile = i.idImmobile inner join smee_comuni using (idComune) "+
					"inner join smee_kpi_zone_benchmark kzb on kzb.idKpiZoneBench=a.idKpiZoneBenchmark "+
					"inner join smee_kpi_value kv on kzb.idKpi=kv.idKpiImmobili "+
					"inner join smee_benchmark_value bv on kzb.idZoneBench=bv.idZoneBenchmark "+
					"inner join smee_zone_benchmark zb on zb.idZoneBench=bv.idZoneBenchmark "+
					"WHERE kv.periodoRiferimento = a.periodoRiferimento and bv.data = a.periodoRiferimento and bv.data = a.periodoRiferimento " + filter_string + " order by a.periodoRiferimento DESC;";
				
		try{
			kpi_alert = this.jdbcTemplate.queryForList(sql);	
			System.out.println(filter_string+" JdbcKPIDAO::getKpiAlert()::  " + sql);		
		}
		catch(Exception e)
		{
			 System.out.println("JdbcKPIDAO::getKpiAlert()::Query non eseguita " + e);		
		}

		return kpi_alert;
	}
	
	public List<Map<String, Object>> getAlertForKpi() {
		List<Map<String, Object>> kpi_alert = new ArrayList<Map<String, Object>>();
		String sql = "SET lc_time_names = 'it_IT';";
		this.jdbcTemplate.execute(sql);
		sql = "SELECT distinct i.codice, a.idAllert, a.allert,DATE_FORMAT(a.data,'%d-%m-%Y %h:%i %p') as data, "+
				"DATE_FORMAT(a.periodoRiferimento,'%M %Y ') as periodoRiferimento, k.descrizione, kv.value as KPI, bv.value as Bench, zb.tolleranza "+
				"FROM smee_allert a right join smee_kpi k on a.idKpi=k.idKpi  right join smee_immobili i on a.idImmobile = i.idImmobile "+
				"right join smee_kpi_zone_benchmark kzb on kzb.idKpiZoneBench=a.idKpiZoneBenchmark "+
				"right join smee_kpi_value kv on kzb.idKpi=kv.idKpiImmobili "+
				"right join smee_benchmark_value bv on kzb.idZoneBench=bv.idZoneBenchmark "+
				"right join smee_zone_benchmark zb on zb.idZoneBench=bv.idZoneBenchmark "+
				"WHERE kv.periodoRiferimento = a.periodoRiferimento and bv.data = a.periodoRiferimento and bv.data = a.periodoRiferimento " + filter_string ;//+ " group by a.idImmobile";
			
		try{
			kpi_alert = this.jdbcTemplate.queryForList(sql);	
			System.out.println("JdbcKPIDAO::getAlertForKpi()::  " + sql);	
		}
		catch(Exception e)
		{
			 System.out.println("JdbcKPIDAO::getKpiAlert()::Query non eseguita " + e);		
		}

		return kpi_alert;
	}
	
	public List<Map<String, Object>> getTipoKpi() 
	{
				List<Map<String, Object>> kpi = new ArrayList<Map<String, Object>>();
				String sql = "SELECT distinct k.* FROM (smee_kpi k join smee_kpi_immobili ki on k.idKpi=ki.idKpi); " ;
				
				try{
					kpi = this.jdbcTemplate.queryForList(sql);		
				}
				catch(Exception e)
				{
					 System.out.println("JdbcKPIDAO::getTipoKpi()::Query non eseguita " + e);		
				}
				return kpi;
	}

	

	
}
