'use strict';

CREEMapp.controller('BusDataMapController', ['$rootScope', '$scope', 'leafletData', 'BuildingsFactory', 'AlertFactory', 'CreemSettings',
		function ($rootScope, $scope, leafletData, BuildingsFactory, AlertFactory, CreemSettings) 
		{

			$scope.center = {};			
			$scope.immobili = [];
			$scope.buildings = {};
			$scope.markers = [];			
			$scope.measure = [];
			
			
			$scope.layers = {
				baselayers: {
					osm: {
						name: 'OpenStreetMap',
						type: 'xyz',
						url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
					   }
				},
				overlays: 
				{	
					measure: { name: "Measure", type:"markercluster", visible: true } 
				
				}						
				
				
								};



			var refreshData = function ()
			{				

				$scope.buildings = {};								
				
				$scope.markers = [];
				
				$scope.immobili = BuildingsFactory.getBuildings().query(function(){
							
							var len = $scope.immobili.length;
							
								
						
							if(CreemSettings.selectedbuildings.length > 0)
								$scope.buildings[CreemSettings.selectedbuildings[0].codice] = CreemSettings.selectedbuildings[0];
								
							
							// if(CreemSettings.selectedMeasures.length > 0)														
								// for (var i=0; i<len; i++) 
								// {									
									// if($scope.immobili[i].clusterEnergy == CreemSettings.selectedClusters[0])
										// $scope.buildings[$scope.immobili[i].codice] = $scope.immobili[i];
								// }
								
								
								
							// if(CreemSettings.selectedCities.length > 0)														
								// for (var i=0; i<len; i++) 
								// {									
									// if($scope.immobili[i].siglaprov == CreemSettings.selectedCities[0])
										// $scope.buildings[$scope.immobili[i].codice] = $scope.immobili[i];
								// }



								$scope.center = {
									lat: 37.4280017,
									lng: 13.7893994,
									zoom: 8
								};
								
								
								

								// var measure =  AlertFactory.getALLBuildingAlert().query(
											// function() 
											// {	
							
												
												// for(var i=0; i<measure.length; i++)
												// {														
														
														// if($scope.buildings[measure[i]["codice"]] != undefined)																												
														// {
														
															   // measure[i]["super"] = parseFloat(((parseFloat(measure[i]["KPI"]).toFixed(2) - parseFloat(measure[i]["Bench"]).toFixed(2))/parseFloat(measure[i]["Bench"]).toFixed(2))*100).toFixed(2);
															
																							
															
															// var awesomeMarkerIcon = {
																// type: 'awesomeMarker',
																// icon: 'exclamation-sign',
																// prefix: 'glyphicon',
																// markerColor: 'orange'
																
															// };
															// if(alert[i]["super"] > 100.0)
																// awesomeMarkerIcon.markerColor = "red";
															// else
																// awesomeMarkerIcon.markerColor = "orange";
															
															// $scope.markers.push({
																		// layer: "measure",
																		// lat: $scope.buildings[alert[i]["codice"]].latitudine,
																		// lng: $scope.buildings[alert[i]["codice"]].longitudine,
																		// message: "<b>" + measure[i]["codice"] + "</b> " + $scope.buildings[measure[i]["codice"]].indirizzo + " (" + $scope.buildings[measure[i]["codice"]].siglaprov +") <br><b>KPI:</b> " + measure[i]["descrizione"] + "<br> <b>Periodo:</b> " + measure[i]["periodoRiferimento"] + "<br> <b>Superamento benchmark:</b> " + measure[i]["super"] + "%",
																		// focus: false,
																		// icon: awesomeMarkerIcon,											
																		// draggable: false											
															// });
														// }
												
												// }																							

											// });
								
								
																
								
								
																						

				 });
				
			}
			
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
					
			if(CreemSettings.selectedbuildings.length > 0)
					refreshData();

				
		}
]);