package it.smee.epi.models;

public class DatiEdificio {
	
	private float superficielorda;
	private float superficienetta;
	private float volumelordo;
	
	
	private float volumenetto;
	private float altezza;
	
	private int numeropiani;
	private float superficielordapiano;
	private float superficienettapiano;
	
	
	private float superficiedisperdentelorda;
	
	private float potenzaLux_Interna;
	private float oremedieLux_Interna;
	private float potenzaLux_Esterna;
	private float oremedieLux_Esterna;
	
	
	private float quotaCaldaie;
	private float quotaPdC;
	
	
	private float etae_caldaia;
	private float etarg_caldaia;
	private float etad_caldaia;
	private float etagn_caldaia;
	private float etae_PdC;
	private float etarg_PdC;
	private float etad_PdC;
	private float etagn_PdC;
	private float etae_estivo;
	private float etarg_estivo;
	private float etad_estivo;
	private float etagn_estivo;
	
	private float eta_bench_C;
	private float eta_bench_PdC;
	private float eta_bench_estivo;
	private float potenza_PdC;
	private float potenza_Caldaia;
	private float potenza_Climatizzazione;
	private float potenza_media_PdC;
	private float potenza_media_Caldaia;
	private float potenza_media_Climatizzazione;

	private String tipologiadistribuzione_caldaie;
	private String tipologiadistribuzione_pdc;
	private String tipologiadistribuzione_estivo;
	private String tipologiascambio_pdc;
	private String tipologiascambio_estivo;
	
	private float apporto_interno;
	private float apporto_interno_est;
	private float ricambio_aria;
	private float ricambio_aria_est;
	private float eta_bench_PdC_energia_primaria;
	private float eta_bench_estivo_energia_primaria;
		
	
	public float getSuperficielorda() {
		return superficielorda;
	}

	public void setSuperficielorda(float superficielorda) {
		this.superficielorda = superficielorda;
	}

	public float getSuperficienetta() {
		return superficienetta;
	}

	public void setSuperficienetta(float superficienetta) {
		this.superficienetta = superficienetta;
	}

	public float getVolumelordo() {
		return volumelordo;
	}

	public void setVolumelordo(float volumelordo) {
		this.volumelordo = volumelordo;
	}
	public float getVolumenetto() {
		return volumenetto;
	}
	public void setVolumenetto(float volumenetto) {
		this.volumenetto = volumenetto;
	}
	public float getAltezza() {
		return altezza;
	}
	public void setAltezza(float altezza) {
		this.altezza = altezza;
	}

	public float getSuperficiedisperdentelorda() {
		return superficiedisperdentelorda;
	}
	public void setSuperficiedisperdentelorda(float superficiedisperdentelorda) {
		this.superficiedisperdentelorda = superficiedisperdentelorda;
	}
	
	
	public float getQuotaCaldaie() {
		return quotaCaldaie;
	}
	public void setQuotaCaldaie(float quotaCaldaie) {
		this.quotaCaldaie = quotaCaldaie;
	}
	public float getQuotaPdC() {
		return quotaPdC;
	}
	public void setQuotaPdC(float quotaPdC) {
		this.quotaPdC = quotaPdC;
	}
	

	public float getEtae_caldaia() {
		return etae_caldaia;
	}

	public void setEtae_caldaia(float etae_caldaia) {
		this.etae_caldaia = etae_caldaia;
	}

	public float getEtarg_caldaia() {
		return etarg_caldaia;
	}

	public void setEtarg_caldaia(float etarg_caldaia) {
		this.etarg_caldaia = etarg_caldaia;
	}

	public float getEtad_caldaia() {
		return etad_caldaia;
	}

	public void setEtad_caldaia(float etad_caldaia) {
		this.etad_caldaia = etad_caldaia;
	}

	public float getEtagn_caldaia() {
		return etagn_caldaia;
	}

	public void setEtagn_caldaia(float etagn_caldaia) {
		this.etagn_caldaia = etagn_caldaia;
	}

	public float getEtae_PdC() {
		return etae_PdC;
	}

	public void setEtae_PdC(float etae_PdC) {
		this.etae_PdC = etae_PdC;
	}

	public float getEtarg_PdC() {
		return etarg_PdC;
	}

	public void setEtarg_PdC(float etarg_PdC) {
		this.etarg_PdC = etarg_PdC;
	}

	public float getEtad_PdC() {
		return etad_PdC;
	}

	public void setEtad_PdC(float etad_PdC) {
		this.etad_PdC = etad_PdC;
	}

	public float getEtagn_PdC() {
		return etagn_PdC;
	}

	public void setEtagn_PdC(float etagn_PdC) {
		this.etagn_PdC = etagn_PdC;
	}

	public float getEtae_estivo() {
		return etae_estivo;
	}

	public void setEtae_estivo(float etae_estivo) {
		this.etae_estivo = etae_estivo;
	}

	public float getEtarg_estivo() {
		return etarg_estivo;
	}

	public void setEtarg_estivo(float etarg_estivo) {
		this.etarg_estivo = etarg_estivo;
	}

	public float getEtad_estivo() {
		return etad_estivo;
	}

	public void setEtad_estivo(float etad_estivo) {
		this.etad_estivo = etad_estivo;
	}

	public float getEtagn_estivo() {
		return etagn_estivo;
	}

	public void setEtagn_estivo(float etagn_estivo) {
		this.etagn_estivo = etagn_estivo;
	}

	public float getEta_bench_C() {
		return eta_bench_C;
	}

	public void setEta_bench_C(float eta_bench_C) {
		this.eta_bench_C = eta_bench_C;
	}

	public float getEta_bench_PdC() {
		return eta_bench_PdC;
	}

	public void setEta_bench_PdC(float eta_bench_PdC) {
		this.eta_bench_PdC = eta_bench_PdC;
	}

	public float getEta_bench_estivo() {
		return eta_bench_estivo;
	}

	public void setEta_bench_estivo(float eta_bench_estivo) {
		this.eta_bench_estivo = eta_bench_estivo;
	}


	public float getPotenza_PdC() {
		return potenza_PdC;
	}

	public void setPotenza_PdC(float potenza_PdC) {
		this.potenza_PdC = potenza_PdC;
	}

	public float getPotenza_Caldaia() {
		return potenza_Caldaia;
	}

	public void setPotenza_Caldaia(float potenza_Caldaia) {
		this.potenza_Caldaia = potenza_Caldaia;
	}

	public float getPotenza_Climatizzazione() {
		return potenza_Climatizzazione;
	}

	public void setPotenza_Climatizzazione(float potenza_Climatizzazione) {
		this.potenza_Climatizzazione = potenza_Climatizzazione;
	}

	public float getPotenza_media_PdC() {
		return potenza_media_PdC;
	}

	public void setPotenza_media_PdC(float potenza_media_PdC) {
		this.potenza_media_PdC = potenza_media_PdC;
	}

	public float getPotenza_media_Caldaia() {
		return potenza_media_Caldaia;
	}

	public void setPotenza_media_Caldaia(float potenza_media_Caldaia) {
		this.potenza_media_Caldaia = potenza_media_Caldaia;
	}

	public float getPotenza_media_Climatizzazione() {
		return potenza_media_Climatizzazione;
	}

	public void setPotenza_media_Climatizzazione(
			float potenza_media_Climatizzazione) {
		this.potenza_media_Climatizzazione = potenza_media_Climatizzazione;
	}

	

	public String getTipologiadistribuzione_caldaie() {
		return tipologiadistribuzione_caldaie;
	}

	public void setTipologiadistribuzione_caldaie(
			String tipologiadistribuzione_caldaie) {
		this.tipologiadistribuzione_caldaie = tipologiadistribuzione_caldaie;
	}

	public String getTipologiadistribuzione_pdc() {
		return tipologiadistribuzione_pdc;
	}

	public void setTipologiadistribuzione_pdc(String tipologiadistribuzione_pdc) {
		this.tipologiadistribuzione_pdc = tipologiadistribuzione_pdc;
	}

	public String getTipologiadistribuzione_estivo() {
		return tipologiadistribuzione_estivo;
	}

	public void setTipologiadistribuzione_estivo(
			String tipologiadistribuzione_estivo) {
		this.tipologiadistribuzione_estivo = tipologiadistribuzione_estivo;
	}

	public String getTipologiascambio_pdc() {
		return tipologiascambio_pdc;
	}

	public void setTipologiascambio_pdc(String tipologiascambio_pdc) {
		this.tipologiascambio_pdc = tipologiascambio_pdc;
	}

	public String getTipologiascambio_estivo() {
		return tipologiascambio_estivo;
	}

	public void setTipologiascambio_estivo(String tipologiascambio_estivo) {
		this.tipologiascambio_estivo = tipologiascambio_estivo;
	}

	public float getPotenzaLux_Interna() {
		return potenzaLux_Interna;
	}

	public void setPotenzaLux_Interna(float potenzaLux_Interna) {
		this.potenzaLux_Interna = potenzaLux_Interna;
	}

	public float getOremedieLux_Interna() {
		return oremedieLux_Interna;
	}

	public void setOremedieLux_Interna(float oremedieLux_Interna) {
		this.oremedieLux_Interna = oremedieLux_Interna;
	}

	public float getPotenzaLux_Esterna() {
		return potenzaLux_Esterna;
	}

	public void setPotenzaLux_Esterna(float potenzaLux_Esterna) {
		this.potenzaLux_Esterna = potenzaLux_Esterna;
	}

	public float getOremedieLux_Esterna() {
		return oremedieLux_Esterna;
	}

	public void setOremedieLux_Esterna(float oremedieLux_Esterna) {
		this.oremedieLux_Esterna = oremedieLux_Esterna;
	}

	public float getApporto_interno() {
		return apporto_interno;
	}

	public void setApporto_interno(float apporto_interno) {
		this.apporto_interno = apporto_interno;
	}

	public float getApporto_interno_est() {
		return apporto_interno_est;
	}

	public void setApporto_interno_est(float apporto_interno_est) {
		this.apporto_interno_est = apporto_interno_est;
	}

	public float getRicambio_aria() {
		return ricambio_aria;
	}

	public void setRicambio_aria(float ricambio_aria) {
		this.ricambio_aria = ricambio_aria;
	}

	public float getRicambio_aria_est() {
		return ricambio_aria_est;
	}

	public void setRicambio_aria_est(float ricambio_aria_est) {
		this.ricambio_aria_est = ricambio_aria_est;
	}

	public int getNumeropiani() {
		return numeropiani;
	}

	public void setNumeropiani(int numeropiani) {
		this.numeropiani = numeropiani;
	}

	public float getSuperficielordapiano() {
		return superficielordapiano;
	}

	public void setSuperficielordapiano(float superficielordapiano) {
		this.superficielordapiano = superficielordapiano;
	}

	public float getSuperficienettapiano() {
		return superficienettapiano;
	}

	public void setSuperficienettapiano(float superficienettapiano) {
		this.superficienettapiano = superficienettapiano;
	}
	public float getEta_bench_PdC_energia_primaria() {
		return eta_bench_PdC_energia_primaria;
	}

	public void setEta_bench_PdC_energia_primaria(
			float eta_bench_PdC_energia_primaria) {
		this.eta_bench_PdC_energia_primaria = eta_bench_PdC_energia_primaria;
	}

	public float getEta_bench_estivo_energia_primaria() {
		return eta_bench_estivo_energia_primaria;
	}

	public void setEta_bench_estivo_energia_primaria(
			float eta_bench_estivo_energia_primaria) {
		this.eta_bench_estivo_energia_primaria = eta_bench_estivo_energia_primaria;
	}

}
