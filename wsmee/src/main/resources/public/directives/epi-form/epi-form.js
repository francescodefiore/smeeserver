CREEMapp.directive('epiForm', 

function() 
{
	
	return {
	
    controller: ['$scope', 'CreemSettings','EPIFactory', 'EpiForm', 'ModalService',

		function($scope, CreemSettings, EPIFactory, EpiForm, ModalService)
		{
			var cloneObject = function (object) {
				return jQuery.extend(true, {}, object);
			};			

			$scope.currentTab = 'directives/epi-form/dati-edificio.html';

			$scope.onClickTab = function (tab) {
				$scope.currentTab = tab;
			};

			$scope.isActiveTab = function(tabUrl) {
				return tabUrl == $scope.currentTab;
			};

			$scope.moduloepi = {};
			$scope.datigenerali = {};
						
			$scope.eta_bench_genEstivo = 0;
			$scope.eta_bench_derEstivo = 0;						
			$scope.eta_g_caldaia = 0;
			$scope.eta_deri_caldaia = 0;							
			$scope.eta_g_pdc = 0;
			$scope.eta_deri_pdc = 0;
			
			$scope.quotaPdC = 0;
			$scope.quotaCaldaie = 100;
				

			$scope.onSubmit = function() {
			
					$scope.calcolaEtag();
					
					var modalOptions = {
						closeButtonText: 'Annulla',
						actionButtonText: 'OK',
						headerText: 'Attenzione',
						bodyText: 'Conferma Aggiornamento Dati Edificio?'
					};

					ModalService.showModal({}, modalOptions).then(function (result) {
							var epiform = new EpiForm($scope.moduloepi);
							console.log(epiform);
							epiform.$save();					
					});
					

			}
			
			var findIndexArray = function(array, value)
			{
				for(var i=0; i<array.length; i++)
					if(array[i].name == value)
						return i;
				return 0;
			}

			var refreshData = function()
			{
					$scope.building = CreemSettings.selectedbuildings[0];
					var moduloepi = EPIFactory.getEpiForm($scope.building.idImmobile).query(
						function(){
							$scope.moduloepi = moduloepi;
							$scope.moduloepi.idImmobile = $scope.building.idImmobile;
							
							$scope.tipodistribuzione_C.selectedOption = $scope.tipodistribuzione_C.availableOptions[findIndexArray($scope.tipodistribuzione_C.availableOptions, $scope.moduloepi.datiedificio.tipologiadistribuzione_caldaie)];
							$scope.tipodistribuzione_PdC.selectedOption = $scope.tipodistribuzione_PdC.availableOptions[findIndexArray($scope.tipodistribuzione_PdC.availableOptions, $scope.moduloepi.datiedificio.tipologiadistribuzione_pdc)];
							$scope.tiposcambio_PdC.selectedOption = $scope.tiposcambio_PdC.availableOptions[findIndexArray($scope.tiposcambio_PdC.availableOptions, $scope.moduloepi.datiedificio.tipologiascambio_pdc)];
							$scope.tipodistribuzione_estivo.selectedOption = $scope.tipodistribuzione_estivo.availableOptions[findIndexArray($scope.tipodistribuzione_estivo.availableOptions, $scope.moduloepi.datiedificio.tipologiadistribuzione_estivo)];
							$scope.tiposcambio_estivo.selectedOption = $scope.tiposcambio_estivo.availableOptions[findIndexArray($scope.tiposcambio_estivo.availableOptions, $scope.moduloepi.datiedificio.tipologiascambio_estivo)];
							
							
							var datigenerali = EPIFactory.getDatiGenerali().query(
								function(){
									$scope.datigenerali = datigenerali;
									$scope.calcolaEtag();
								}
							);
							
							
						}
					);
			}

			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );

			if(CreemSettings.selectedbuildings.length > 0) {
				refreshData();
			}
			
			


		    // Componenti Transparenti
			$scope.componentiTransparentiOptions = {
				tipotelaio: ['Metallo', 'Metallo Taglio Termici', 'Legno Duro', 'Legno Tenero', 'PVC'],
				tipovetro: ['Singolo', 'Doppio', 'Triplo', 'Doppio Basso Emissivo', 'Triplo Basso Emissivo'],
				tipogas: ['Aria', 'Argon', 'Krypton', 'SF6', 'Xenon'],
				esposizioneinfissi: ['NORD', 'NORD-EST', 'EST', 'SUD-EST', 'SUD', 'SUD-OVEST', 'OVEST', 'NORD-OVEST'],
				componentiTransparentiSelection : [],
				flagsi: ['SI', 'NO']
			};

			$scope.componentiTransparentiModel = {
				note: undefined,
				tipovetro: $scope.componentiTransparentiOptions.tipovetro[0],
				tipotelaio: $scope.componentiTransparentiOptions.tipotelaio[0],
				tipogas: $scope.componentiTransparentiOptions.tipogas[0],
				esposizioneinfissi: $scope.componentiTransparentiOptions.esposizioneinfissi[0],
				supinfissosi: undefined,
				trasmittanzausi: undefined,
				flagsi: $scope.componentiTransparentiOptions.flagsi[0]
			};
			
			$scope.materialeTelaioOptions = ['Metallo', 'Metallo Taglio Termici', 'Legno Duro', 'Legno Tenero', 'PVC'];
			$scope.tipoVetroOptions = ['Singolo', 'Doppio', 'Triplo', 'Doppio Basso Emissivo', 'Triplo Basso Emissivo'];
			$scope.gasIntercapedineOptions = ['Aria', 'Argon', 'Krypton', 'SF6', 'Xenon'];
			$scope.esposizioneOptions = ['NORD', 'NORD-EST', 'EST', 'SUD-EST', 'SUD', 'SUD-OVEST', 'OVEST', 'NORD-OVEST'];
			$scope.componentiTransparentiSelection = [];

			$scope.componentiTransparentiAdd= function () {
						
			$scope.moduloepi.componentitrasparenti.push({
					note: $scope.componentiTransparentiModel.note,
					tipovetro: $scope.componentiTransparentiModel.tipovetro,
					tipotelaio: $scope.componentiTransparentiModel.tipotelaio,
					tipogas: $scope.componentiTransparentiModel.tipogas,
					esposizioneinfissi: $scope.componentiTransparentiModel.esposizioneinfissi,
					supinfissosi: $scope.componentiTransparentiModel.supinfissosi,
					trasmittanzausi: $scope.componentiTransparentiModel.trasmittanzausi,
					flagsi: $scope.componentiTransparentiModel.flagsi
			});
				
				
			};

			//Superfici Opache
			$scope.superficiOpacheSelection = [];
			$scope.superficiOpacheOptions = {
				tiposuperficie: ['Parete', 'Pavimento', 'Copertura Piana', 'Copertura Inclinata'],
				esposizione: ['NORD', 'NORD-EST', 'EST', 'SUD-EST', 'SUD', 'SUD-OVEST', 'OVEST', 'NORD-OVEST'],
				ambienteconfinante: ['Esterno', 'Ambiente non riscaldato con aperture', 'Ambiente non riscaldato senza aperture']
			};

			$scope.superficiOpacheModel = {
				trasmittanzaeop: undefined,
				spessore: undefined,
				superficieseo: undefined,
				materiale: undefined,
				descrizione: undefined,
				fattorecorrezione: undefined,
				massa: undefined,
				calorespecifico: undefined,
				tiposuperficie: $scope.superficiOpacheOptions.tiposuperficie[0],
				esposizione: $scope.superficiOpacheOptions.esposizione[0],
				ambienteconfinante: $scope.superficiOpacheOptions.ambienteconfinante[0]
			};
			
			$scope.superficiOpacheAdd = function () {												
					$scope.moduloepi.supesterneopache.push(cloneObject($scope.superficiOpacheModel));				
			};
			
			$scope.tipodistribuzione_C = {
				selectedOption: {id: '0', name: 'Sistemi idronici'},
				availableOptions: [{id: '0', name: 'Sistemi idronici'},{ id: '1', name: 'Sistemi aeraulici'}],
			};
			
			$scope.tipodistribuzione_PdC = {
				selectedOption: {id: '0', name: 'Sistemi ad espansione diretta'},
				availableOptions: [{id: '0', name: 'Sistemi ad espansione diretta'},{id: '1', name: 'Sistemi idronici'},{id: '2', name: 'Sistemi aeraulici'}],
			};
			
			$scope.tiposcambio_PdC = {
				selectedOption: {id: '1', name: 'Aria/Aria'},
				availableOptions: [{id: '1', name: 'Aria/Aria'}, {id: '2', name: 'Aria/Acqua (Pt<=35kW)'}, {id: '3', name: 'Aria/Acqua (Pt>35kW)'}, {id: '4', name: 'Salamoia/Aria'}, {id: '5', name: 'Salamoia/Acqua'}, {id: '6', name:  'Acqua/Aria'}, {id: '7', name: 'Acqua/Acqua'}],				
			};
			
			$scope.tipodistribuzione_estivo = {
				selectedOption: {id: '0', name: 'Sistemi ad espansione diretta'},
				availableOptions: [{id: '0', name: 'Sistemi ad espansione diretta'},{id: '1', name: 'Sistemi idronici'},{id: '2', name: 'Sistemi aeraulici'}],
			};
			
			$scope.tiposcambio_estivo = {
				selectedOption: {id: '0', name: 'Aria/Aria'},				
				availableOptions: [{id: '0', name: 'Aria/Aria'}, {id: '1', name: 'Aria/Acqua (Pt<=35kW)'}, {id: '2', name: 'Aria/Acqua (Pt>35kW)'}],
			};
			
			
			
			$scope.calcolaEtag = function () {				
				var tipoSistema = $scope.tipodistribuzione_PdC.selectedOption.id;
				var tipoPdC = $scope.tiposcambio_PdC.selectedOption.id;				
				
				var dati_cop = EPIFactory.getEpiCOPForm($scope.tiposcambio_PdC.selectedOption.id, $scope.moduloepi.idImmobile).query(
					function(){	
							$scope.dati_cop = dati_cop;
							var zona = $scope.dati_cop[0].zona;
							
							$scope.moduloepi.datiedificio.tipologiascambio_estivo = $scope.tiposcambio_estivo.selectedOption.name;
							$scope.moduloepi.datiedificio.tipologiascambio_pdc = $scope.tiposcambio_PdC.selectedOption.name;
							$scope.moduloepi.datiedificio.tipologiadistribuzione_pdc = $scope.tipodistribuzione_PdC.selectedOption.name;
							$scope.moduloepi.datiedificio.tipologiadistribuzione_estivo = $scope.tipodistribuzione_estivo.selectedOption.name;
							$scope.moduloepi.datiedificio.tipologiadistribuzione_caldaie = $scope.tipodistribuzione_C.selectedOption.name;
							
							//Caldaie
							$scope.eta_g_caldaia = 90+2*Math.log10($scope.moduloepi.datiedificio.potenza_Caldaia);
							
							if ($scope.moduloepi.datiedificio.potenza_media_Caldaia<=400) {
								$scope.eta_deri_caldaia = (75+3*Math.log10($scope.moduloepi.datiedificio.potenza_Caldaia))/$scope.eta_g_caldaia;
							}
							else if ($scope.moduloepi.datiedificio.potenza_media_Caldaia>400 &&  $scope.moduloepi.datiedificio.potenza_media_Caldaia<1000) {
								$scope.eta_deri_caldaia = (75+3*Math.log10($scope.moduloepi.datiedificio.potenza_Caldaia))/95.2;
							}
							else{
								$scope.eta_deri_caldaia = 84/95.2;
							}
							var eta_bench_C = ($scope.eta_g_caldaia*$scope.eta_deri_caldaia * Math.pow(10, 2))/Math.pow(10, 2);
							
							//Pompe di Calore
							var eta_g_pdc = $scope.dati_cop[0].cop;
							if (zona == 'E' || zona == 'F') {
								eta_g_pdc = $scope.dati_cop[0].copEF;
							}
							var eta_deri_pdc = 0;
							var eta_bench_PdC = 0;
							if (tipoSistema == '0') {
								eta_bench_PdC = eta_g_pdc;
							}else {
								if ($scope.moduloepi.datiedificio.potenza_media_PdC<=400) {
									eta_deri_pdc = (75+3*Math.log10($scope.moduloepi.datiedificio.potenza_PdC))/eta_g_pdc;
								}
								else if ($scope.moduloepi.datiedificio.potenza_media_PdC>400 &&  $scope.moduloepi.datiedificio.potenza_media_PdC<1000) {
									eta_deri_pdc = (75+3*Math.log10($scope.moduloepi.datiedificio.potenza_PdC))/95.2;
								}
								else{
									eta_deri_pdc = 84/95.2;
								}
								eta_bench_PdC = (eta_g_pdc*eta_deri_pdc * Math.pow(10, 2))/Math.pow(10, 2);
							}
							
							//Climatizzazione estiva							
							var eta_deri_estivo = 0;
							var eta_g_estivo = $scope.dati_cop[0].eer;
													
							if ($scope.moduloepi.datiedificio.potenza_media_Climatizzazione<=400) {
								eta_deri_estivo = (75+3*Math.log10($scope.moduloepi.datiedificio.potenza_Climatizzazione))/eta_g_estivo;
							}
							else if ($scope.moduloepi.datiedificio.potenza_media_Climatizzazione>400 &&  $scope.moduloepi.datiedificio.potenza_media_Climatizzazione<1000) {
								eta_deri_estivo = (75+3*Math.log10($scope.moduloepi.datiedificio.potenza_Climatizzazione))/95.2;
							}
							else{
								eta_deri_estivo = 84/95.2;
							}							
							var eta_bench_estivo = (eta_g_estivo*eta_deri_estivo * Math.pow(10, 2))/Math.pow(10, 2);
							
																
							$scope.eta_bench_genEstivo = eta_g_estivo;
							$scope.eta_bench_derEstivo = eta_deri_estivo;
							
							$scope.eta_g_pdc = eta_g_pdc;
							$scope.eta_deri_pdc = eta_deri_pdc;
							
							$scope.moduloepi.datiedificio.eta_bench_C = (eta_bench_C* Math.pow(10, 2))/Math.pow(10, 2);
							$scope.moduloepi.datiedificio.eta_bench_PdC =  (eta_bench_PdC* Math.pow(10, 2))/Math.pow(10, 2);
							$scope.moduloepi.datiedificio.eta_bench_estivo =  (eta_bench_estivo* Math.pow(10, 2))/Math.pow(10, 2);

					}
				);
	
			};
			
			
		}],

		templateUrl: 'directives/epi-form/epi-form.html'
  };

});



	
	