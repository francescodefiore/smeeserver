CREEMapp.directive('buildingForm', 

function() 
{
	
	return {
	
    controller: ['$scope', 'CreemSettings','BuildingsFactory','BuildingForm', 'ModalService',

		function($scope, CreemSettings, BuildingsFactory, BuildingForm, ModalService)
		{
			var cloneObject = function (object) {
				return jQuery.extend(true, {}, object);
			};

			$scope.currentTab = 'directives/building-form/building-form.html';

			$scope.selectedbuilding = {};

			  $scope.onSubmit = function() {
					var modalOptions = {
						closeButtonText: 'Annulla',
						actionButtonText: 'OK',
						headerText: 'Attenzione',
						bodyText: 'Conferma Aggiornamento Dati Immobili?'
					};

					ModalService.showModal({}, modalOptions).then(function (result) {
							var buildingform = new BuildingForm($scope.selectedbuilding);							
							buildingform.$save();					
					});
					

			  }


			var refreshData = function()
			{
					$scope.selectedbuildingid = CreemSettings.selectedbuildings[0].idImmobile;
					var building = BuildingsFactory.getBuilding($scope.selectedbuildingid).query(
						function(){
							$scope.selectedbuilding = building;							
						}
					);

			}

			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );

			if(CreemSettings.selectedbuildings.length > 0) {
				refreshData();
			}


		}],

		templateUrl: 'directives/building-form/building-form.html'
  };

});



	
	