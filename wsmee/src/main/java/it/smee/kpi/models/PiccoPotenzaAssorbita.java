package it.smee.kpi.models;

/* 
 * Ref-F-001 - Generale - Valore picco potenza assorbita - KWmax 
*/
public class PiccoPotenzaAssorbita extends KPI
{
	private float potenza_picco;	
	
	@Override
	public float getValue() {
		return potenza_picco;
	}

	@Override
	public void setValue(float value) {
		this.potenza_picco = value;		
	}


	@Override
	public String getSQL_SELECT_column() {
		return "GREATEST( MAX(01_am), MAX(02_am), MAX(03_am), MAX(04_am), MAX(05_am), MAX(06_am), MAX(07_am), MAX(08_am), MAX(09_am), MAX(10_am), MAX(11_am), MAX(12_pm), MAX(01_pm), MAX(02_pm) , MAX(03_pm), MAX(04_pm), MAX(05_pm), MAX(06_pm), MAX(07_pm), MAX(08_pm), MAX(09_pm), MAX(10_pm), MAX(11_pm), MAX(12_am)) AS potenza_picco";
	}

	@Override
	public String getSQL_TABLE_from() {		
		return "datimultiorari";
	}
	
	
	
}