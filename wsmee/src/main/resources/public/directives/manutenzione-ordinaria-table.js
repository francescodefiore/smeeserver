CREEMapp.directive('manutenzioneOrdinariaTable', 
function() 
{
	return {    
			templateUrl: 'directives/manutenzione-ordinaria-table.html',
			
			controller: ['$scope', 'ManutenzioneFactory', 'ngTableParams', 'LoadingFactory', 'CreemSettings', 
			
			function($scope, ManutenzioneFactory, ngTableParams, LoadingFactory, CreemSettings) 
			{		
				var filter_immobile = "";
				var filter_cluster = "";
				var filter_prov = "";
				
				if(CreemSettings.selectedbuildings.length > 0)				
						filter_immobile = CreemSettings.selectedbuildings[0].codice;

				
				var interventi = [{"id":"Sostituzione", "title": "Sostituzione" }];
				var filter_codice = "";
				var filter_cluster = "";
				var filter_prov = "";
				var filter_tipologia = "";
				
				if(CreemSettings.selectedbuildings.length > 0)					
						filter_codice = CreemSettings.selectedbuildings[0].codice;
						
				
				// var m_data =  ManutenzioneFactory.getTipoIntervento().query(
						// function()
						// {
							// if (m_data.length > 0) {
								// interventi.push(  {"id": "" , "title": "Tutti" } );
								// for(var i=0; i<m_data.length; i++) {
									// interventi.push({"id":m_data[i].descrizione, "title": m_data[i].descrizione });
								// }
								
							// }
						// }
				// );
				
				
				var createTable = function(cod, cluster, prov, intervento)
				{
					var my_data = [];
					var j=0;
					for(var i=0; i<data.length; i++) {	
						if (cod!="" && data[i].codice==cod) {
							
							my_data[j]=data[i];
							j++;	
						}
						else if (cluster!="" && data[i].clusterEnergy==cluster) {
							my_data[j]=data[i];
							j++;	
						}
						else if (prov!="" && data[i].siglaprov==prov) {
							my_data[j]=data[i];
							j++;	
						}				
					}
					$scope.tableManutenzione = new ngTableParams({
							page: 1, // show first page
							count: 10, // count per page,							
							}, {
								data: my_data
					});							
				}
	
				var data =  ManutenzioneFactory.getALLBuildingsInterventi().query(
					function() 
					{	
						for(var i=0; i<data.length; i++) {						
							data[i]["idData"] = i;
							if (data[i]["tipologiaIntervento"]==10)
								data[i]["tipologiaIntervento"] = "Sostituzione";
								
								data[i]["statoAttuale"] = "Modello: " + data[i].modVecchioComp +
														  "\rPrestazione: " + data[i].prestazioniVecchioComp +
														  "\rNum. Cambio: " + data[i].numCompDaCambiare +
											              "\rConsumo Totale(W): " + data[i].consumoTotOld + 
											              "\rCosto (€): " + data[i].costoTotOld ;
											
								data[i]["statoProposto"] = "Modello: " + data[i].modNuovoComp + 
														   "\rPrestazione: " + data[i].prestazioniNuovoComp + 
														   "\rNum. Cambio: " + data[i].numComponentiDaComprare + 
														   "\rPotenza (W): " + data[i].potenza + 
														   "\rValore Economico (€): " + data[i].valSingoloComp +  
														   "\rConsumo Tot. (W): " + data[i].consumoTotComponenti +  
														   "\rCosto Tot. (€): " + data[i].costoComponenti ;
							
						}	
						createTable(filter_codice, filter_cluster, filter_prov, "");

					});

				$scope.cols = [
					{ field: "clusterEnergy", title: "Cluster"},
					{ field: "tipologiaIntervento", title: "Tipo Intervento", filter: { tipologiaIntervento: "select" }, filterData: interventi, show: true},				
					{ field: "ubicazione", title: "Ubicazione" },
					{ field: "modVecchioComp", title: "Modello In Uso", tooltip: "statoAttuale"},
					{ field: "modNuovoComp", title: "Modello Proposto", tooltip: "statoProposto"},
					{ field: "costoComponenti", title: "Costo Tot (€)"},
					{ field: "consumoTotComponenti", title: "Consumo Tot.(W)"},
					{ field: "numComponentiDaComprare", title: "N°Comp."}
				];

				
				$scope.$on('settingsUpdated', function (event, data) { 
					if(CreemSettings.selectedbuildings.length > 0)	{					
						createTable(CreemSettings.selectedbuildings[0].codice, "", "");
						$scope.cols = [
							{ field: "clusterEnergy", title: "Cluster"},
							{ field: "tipologiaIntervento", title: "Tipo Intervento", filter: { tipologiaIntervento: "select" }, filterData: interventi, show: true},				
							{ field: "ubicazione", title: "Ubicazione" },
							{ field: "modVecchioComp", title: "Modello In Uso", tooltip: "statoAttuale"},
							{ field: "modNuovoComp", title: "Modello Proposto", tooltip: "statoProposto"},
							{ field: "costoComponenti", title: "Costo Tot (€)"},
							{ field: "consumoTotComponenti", title: "Consumo Tot.(W)"},
							{ field: "numComponentiDaComprare", title: "N°Comp."}
						];	
					}
					if(CreemSettings.selectedClusters.length > 0) {
						createTable("", CreemSettings.selectedClusters[0], "");
						$scope.cols = [
							{ field: "codice", title: "Codice", filter: { codice: "text" }, show: true , sortable:"'codice'"},
							{ field: "tipologiaIntervento", title: "Tipo Intervento", filter: { tipologiaIntervento: "select" }, filterData: interventi, show: true},				
							{ field: "ubicazione", title: "Ubicazione" },
							{ field: "modVecchioComp", title: "Modello In Uso", tooltip: "statoAttuale"},
							{ field: "modNuovoComp", title: "Modello Proposto", tooltip: "statoProposto"},
							{ field: "costoComponenti", title: "Costo Tot (€)"},
							{ field: "consumoTotComponenti", title: "Consumo Tot.(W)"},
							{ field: "numComponentiDaComprare", title: "N°Comp."}
						];	
					}
					if(CreemSettings.selectedCities.length > 0)  {
						createTable("", "", CreemSettings.selectedCities[0]);
						$scope.cols = [
							{ field: "codice", title: "Codice", filter: { codice: "text" }, show: true , sortable:"'codice'"},
							{ field: "tipologiaIntervento", title: "Tipo Intervento", filter: { tipologiaIntervento: "select" }, filterData: interventi, show: true},				
							{ field: "ubicazione", title: "Ubicazione" },
							{ field: "modVecchioComp", title: "Modello In Uso", tooltip: "statoAttuale"},
							{ field: "modNuovoComp", title: "Modello Proposto", tooltip: "statoProposto"},
							{ field: "costoComponenti", title: "Costo (€)"},
							{ field: "consumoTotComponenti", title: "Consumo Tot.(W)"},
							{ field: "numComponentiDaComprare", title: "N°Comp."}
						];	
					}
				} );
       

		}],

  };
 
});
