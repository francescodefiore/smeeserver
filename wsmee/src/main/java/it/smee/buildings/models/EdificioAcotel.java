package it.smee.buildings.models;

public class EdificioAcotel {
	
	private int idImmobile;
	private String codice;
	
	public int getIdImmobile() {
		return idImmobile;
	}
	public void setIdImmobile(int idImmobile) {
		this.idImmobile = idImmobile;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	

}
