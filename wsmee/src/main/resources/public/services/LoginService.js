'use strict';

CREEMapp.service('LoginService', ['$rootScope','$location', function ($rootScope, $location) {
    var state = {
        isLogged: false,
        username: undefined
    };
    this.signIn = function (credentials) {
        if ( !credentials.username || !credentials.password ) {
            throw 'Nessuna password o username specificati.';
        }
        if ( (credentials.username === 'creem') && credentials.password === 'softeco') {
            state.isLogged = true;
            state.username = credentials.username;
            $rootScope.$broadcast('loginSuccessfulEvent');
            $location.path('/dashboard');
        }
        else {
            throw 'Password or username errati';
        }
    };

    this.signOut = function () {
        state.isLogged = false;
		state.username = undefined;
        $rootScope.$broadcast('logoutSuccessfulEvent');
		$location.path('/login');		
    };

    this.isLoggedIn = function () {
        return state.isLogged;
    };

    this.getUsername = function() {
        return state.username;
    }
}]);