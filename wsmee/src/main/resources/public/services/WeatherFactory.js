
CREEMapp.factory('WeatherFactory', ['RESTservAddr','$resource','LoadingFactory','$http', '$q',

    function(RESTservAddr, $resource, LoadingFactory, $http, $q) {
        var WeatherFactory = {};
        var weatherData = {};

        var SoftecoCode = 'baee12df81a5602b';
        var tmpCode = 'df5ebeab6222da53';
        var lastRequestTime = new Date();
        var requestNumber = 0;

        var isRequestPossible = function() {
            if (lastRequestTime.getMinutes() !== new Date().getMinutes()
            || lastRequestTime.getHours() !== new Date().getHours()
            || lastRequestTime.getDate() !== new Date().getDate()
            || lastRequestTime.getMonth() !== new Date().getMonth()) {
                lastRequestTime = new Date();
                requestNumber = 1;
            }
            else {
                if (requestNumber === 10) return false;
                else requestNumber++;
            }
            return true;
        }

        var parseDate = function(str) {
            var mdy = str.split('-');
            return new Date(mdy[0], mdy[1]-1, mdy[2]);
        }

        var dayDiff = function(dateFrom, dateTo) {
            return (dateTo-dateFrom)/(1000*60*60*24);
        }

        WeatherFactory.getForecast = function(city) {
            $http.jsonp('http://api.wunderground.com/api/' + tmpCode + '/hourly10day/q/IT/' + city + '.json?callback=JSON_CALLBACK')
                .success(function(data, status, headers, config) {
                    weatherData = data;
                })
        }

        WeatherFactory.getHistoryDay = function(city, date, index) {
            return $http.jsonp('http://api.wunderground.com/api/' + tmpCode + '/history_' + date + '/q/IT/' + city + '.json?callback=JSON_CALLBACK')
                 .then(function(data, status, headers, config) {
                     return [index, data.data.history.observations];
                 })
        }

        WeatherFactory.getHourlyHistoricData = function(city, dateFrom, dateTo) {
            var deferred = $q.defer();
            dateFrom = parseDate(dateFrom);
            dateTo = parseDate(dateTo);
            var dateDiff = dayDiff(dateFrom, dateTo) + 1;
            var periodData = {};

            for (var i=0; i<dateDiff; i++) {
                if (isRequestPossible()) {
                    var dateFromInt = parseInt(dateFrom.getFullYear())*10000 + (parseInt(dateFrom.getMonth())+1)*100 + parseInt(dateFrom.getDate());
                    var myDataPromise = WeatherFactory.getHistoryDay(city, dateFromInt, i);
                    myDataPromise.then(function(result) {
                        periodData[result[0]] = result[1];
                        if (Object.keys(periodData).length===dateDiff || Object.keys(periodData).length===10) {
                            deferred.resolve(periodData);
                        }
                    });
                    dateFrom.setDate(dateFrom.getDate() + 1);
                }
            }
            return deferred.promise;
        }

        return WeatherFactory;
    }
]);