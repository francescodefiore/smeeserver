package it.smee.budget.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import it.smee.jboxlib.JBoxDAO;


public class JdbcDashboardBudgetDAO implements JBoxDAO{
	
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void print() {
		System.out.println("--- JdbcDashboardBudgetDAO");
	}

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void resetFilters() {	}

	@Override
	public List< Map<String, Object> > getData() {
		
		List<Map<String, Object>> Dashboard = new ArrayList<Map<String, Object>>();
		
		
		List<Map<String, Object>> budget = null;
		List<Map<String, Object>> consuntivi = null;
		
		String sql = "";
		
			

		try {
				
				sql = "SELECT clusterEnergy, anno, " + 
					  "SUM(gen) as gen, SUM(feb) as feb, SUM(mar) as mar, SUM(apr) as apr, SUM(mag) as mag, SUM(giu) as giu, SUM(lug) as lug, SUM(ago) as ago, SUM(sett) as 'set', SUM(ott) as ott, SUM(nov) as nov, SUM(dic) as dic " + 
					  " FROM smee_budget left join smee_immobili on (smee_budget.pod = smee_immobili.pod) GROUP BY clusterEnergy, anno";
				
				System.out.println("query " + sql);
				
				budget = this.jdbcTemplate.queryForList(sql);				

				sql = "SELECT clusterEnergy, anno, " + 
						  "SUM(gen) as gen, SUM(feb) as feb, SUM(mar) as mar, SUM(apr) as apr, SUM(mag) as mag, SUM(giu) as giu, SUM(lug) as lug, SUM(ago) as ago, SUM(sett) as 'set', SUM(ott) as ott, SUM(nov) as nov, SUM(dic) as dic " + 
						  " FROM smee_consuntivi left join smee_immobili on (smee_consuntivi.pod = smee_immobili.pod) GROUP BY clusterEnergy, anno";
					
				System.out.println("query " + sql);
					
				consuntivi = this.jdbcTemplate.queryForList(sql);
							
	
		}catch(Exception e)
		{
			System.out.println("getBuildingDashboard::Error found! "+ e);
		}
		
		Map<String, Object> dd = new HashMap<String, Object>();
		dd.put("budget", budget);
		dd.put("consuntivi", consuntivi);
				
		Dashboard.add(dd);
		
		return Dashboard;
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}

}

