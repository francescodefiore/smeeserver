package it.smee.buildings.dao;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

import it.smee.buildings.models.Policies;
import it.smee.jboxlib.JBoxDAO;


public class JdbcPoliciesDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;
	private String filter_string = "";
	private String name;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	
	@Override	
	public List< Map<String, Object> > getData() {
		
		List< Map<String, Object> > immobili = null;
		
		String sql = "SELECT * " +
			     	 "FROM smee_policies_immobili JOIN smee_immobili ON smee_policies_immobili.idImmobile=smee_immobili.idImmobile " +
			     	 "WHERE 1=1 "+ filter_string;
			     
	
	
		System.out.println("query " + sql);
		
		
		try{
				immobili = this.jdbcTemplate.queryForList(sql);
				       				               				 
		}catch(Exception e)
		{
			 System.out.println("JdbcEdificiDAO::getData()::Query non eseguita");
			 System.out.println(e);			
		}
		
		return immobili;
	}

	
	
	@Override
	public void resetFilters() {
		filter_string = "";
		
	}
	
	private String codeFilter(String[] pod)
	{			
		int i=0;
		String s = "&& codice = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR codice = '" + pod[i] + "'");
		
		return s;
	}


	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {		
		
		if(filter_name.compareTo("CODE") == 0)
			filter_string = filter_string.concat( codeFilter(filter_params));		
	}

	@Override
	public void print() {
		System.out.println("--- JdbcEdificiDAO");
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) 
	{
		// TODO Auto-generated method stub	
	}


	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}


	@Override
	public void setName(String name) {
		this.name = name;		
	}
	
	
	public void AggiornaPolicies(Policies policies) {
		System.out.println("==== AggiornaPolicies new request ====");
	
		
		String idImmobile = policies.getIdImmobile();
		String luxMediaOreLavoro = policies.getLuxMediaOreLavoro();
		String luxAreetransito = policies.getLuxAreetransito();
		String orarioLunVen = policies.getOrarioLunVen();
		String orarioSab = policies.getOrarioSab();
		String TempEstivaAreaLavoro = policies.getTempEstivaAreaLavoro();
		String TempEstivaChiusura = policies.getTempEstivaChiusura();
		String TempInvernoAreaLavoro = policies.getTempInvernoAreaLavoro();
		String TempInvernoChiusura = policies.getTempInvernoChiusura();
		String oraAccensioneRiscaldamento = policies.getOraAccensioneRiscaldamento();
		String oraSpegnimentoRiscaldamento = policies.getOraSpegnimentoRiscaldamento(); 
		String oraAccensioneRaffrescamento = policies.getOraAccensioneRaffrescamento();
		String oraSpegnimentoRaffrescamento = policies.getOraSpegnimentoRaffrescamento();

		try{
			
			String sql;
		
			sql = "UPDATE smee_policies_immobili "+
					      "SET luxMediaOreLavoro = '"+ luxMediaOreLavoro +"', luxAreetransito = '"+ luxAreetransito  +"', "+
					      " orarioLunVen = '"+ orarioLunVen+ "', orarioSab = '" + orarioSab + "',  " +
					      " TempEstivaAreaLavoro = '" + TempEstivaAreaLavoro + "', TempEstivaChiusura = '" + TempEstivaChiusura + "', " +
					      " TempInvernoAreaLavoro = '" + TempInvernoAreaLavoro + "', TempInvernoChiusura = '" + TempInvernoChiusura + "', " +
					      " oraAccensioneRiscaldamento = '" + oraAccensioneRiscaldamento + "', oraSpegnimentoRiscaldamento = '" + oraSpegnimentoRiscaldamento + "', " +
					      " oraAccensioneRaffrescamento = '" + oraAccensioneRaffrescamento + "', oraSpegnimentoRaffrescamento = '" + oraSpegnimentoRaffrescamento + "' " +
						  " where idImmobile='" + idImmobile + "'";
				
				this.jdbcTemplate.update(sql);
				System.out.println("==== "+sql+" ====");	
		}catch(Exception e){
			
			System.out.println(e);	
			
		}
		
	}
				

}