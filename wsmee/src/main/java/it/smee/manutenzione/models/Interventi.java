package it.smee.manutenzione.models;

public class Interventi {
	private int idImmobile;
	private int id;
	private String codice;
	private String denominazione;
	private String clusterEnergy;
	private String siglaprov;
	private String provincia;
	private String tipologiaIntervento;
	private String ubicazione;
	private String modVecchioComp;	
	private String prestazioniVecchioComp;	
	private String numCompDaCambiare;
	private String consumoTotOld;
	private String costoTotOld;
	private String modNuovoComp;	
	private String prestazioniNuovoComp;	
	private String potenza;
	private String valSingoloComp;
	private String numComponentiDaComprare;
	private String consumoTotComponenti;
	private String costoComponenti;
	
	public int getIdImmobile() {
		return idImmobile;
	}
	public void setIdImmobile(int idImmobile) {
		this.idImmobile = idImmobile;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getDenominazione() {
		return denominazione;
	}
	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}
	public String getClusterEnergy() {
		return clusterEnergy;
	}
	public void setClusterEnergy(String clusterEnergy) {
		this.clusterEnergy = clusterEnergy;
	}
	public String getUbicazione() {
		return ubicazione;
	}
	public void setUbicazione(String ubicazione) {
		this.ubicazione = ubicazione;
	}
	public String getNumCompDaCambiare() {
		return numCompDaCambiare;
	}
	public void setNumCompDaCambiare(String numCompDaCambiare) {
		this.numCompDaCambiare = numCompDaCambiare;
	}
	public String getConsumoTotOld() {
		return consumoTotOld;
	}
	public void setConsumoTotOld(String consumoTotOld) {
		this.consumoTotOld = consumoTotOld;
	}
	public String getCostoTotOld() {
		return costoTotOld;
	}
	public void setCostoTotOld(String costoTotOld) {
		this.costoTotOld = costoTotOld;
	}
	public String getPotenza() {
		return potenza;
	}
	public void setPotenza(String potenza) {
		this.potenza = potenza;
	}
	public String getValSingoloComp() {
		return valSingoloComp;
	}
	public void setValSingoloComp(String valSingoloComp) {
		this.valSingoloComp = valSingoloComp;
	}
	public String getNumComponentiDaComprare() {
		return numComponentiDaComprare;
	}
	public void setNumComponentiDaComprare(String numComponentiDaComprare) {
		this.numComponentiDaComprare = numComponentiDaComprare;
	}
	public String getConsumoTotComponenti() {
		return consumoTotComponenti;
	}
	public void setConsumoTotComponenti(String consumoTotComponenti) {
		this.consumoTotComponenti = consumoTotComponenti;
	}
	public String getCostoComponenti() {
		return costoComponenti;
	}
	public void setCostoComponenti(String costoComponenti) {
		this.costoComponenti = costoComponenti;
	}
	public String getModVecchioComp() {
		return modVecchioComp;
	}
	public void setModVecchioComp(String modVecchioComp) {
		this.modVecchioComp = modVecchioComp;
	}
	public String getPrestazioniVecchioComp() {
		return prestazioniVecchioComp;
	}
	public void setPrestazioniVecchioComp(String prestazioniVecchioComp) {
		this.prestazioniVecchioComp = prestazioniVecchioComp;
	}
	public String getModNuovoComp() {
		return modNuovoComp;
	}
	public void setModNuovoComp(String modNuovoComp) {
		this.modNuovoComp = modNuovoComp;
	}
	public String getPrestazioniNuovoComp() {
		return prestazioniNuovoComp;
	}
	public void setPrestazioniNuovoComp(String prestazioniNuovoComp) {
		this.prestazioniNuovoComp = prestazioniNuovoComp;
	}
	public String getSiglaprov() {
		return siglaprov;
	}
	public void setSiglaprov(String siglaprov) {
		this.siglaprov = siglaprov;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getTipologiaIntervento() {
		return tipologiaIntervento;
	}
	public void setTipologiaIntervento(String tipologiaIntervento) {
		this.tipologiaIntervento = tipologiaIntervento;
	}
}
