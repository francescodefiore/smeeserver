package it.smee.daticampo.models;

public class Schede {
	
	private int index;
	private String ip;
	private String ubicazione;
	private String scheda;
	private String ipS;
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getUbicazione() {
		return ubicazione;
	}
	public void setUbicazione(String ubicazione) {
		this.ubicazione = ubicazione;
	}
	public String getScheda() {
		return scheda;
	}
	public void setScheda(String scheda) {
		this.scheda = scheda;
	}
	
	public String getIpS() {
		return ipS;
	}
	public void setIpS(String ipS) {
		this.ipS = ipS;
	}

}
