'use strict';

CREEMapp

	// Angular Routing configuration
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider.otherwise({redirectTo: '/login'});
	
	}])
	
	
	.config(['$translateProvider', function($translateProvider){
		// Register a loader for the static files
		// So, the module will search missing translation tables under the specified urls.
		// Those urls are [prefix][langKey][suffix].
		$translateProvider.useStaticFilesLoader({
		  prefix: 'l10n/',
		  suffix: '.js'
		});
		// Tell the module what language to use by default
		$translateProvider.preferredLanguage('it');
		// Tell the module to store the language in the local storage
		$translateProvider.useLocalStorage();		
		$translateProvider.useSanitizeValueStrategy('sanitize');
	}])
	
	.constant('RESTservAddr', "http://localhost:8080")
	

	.constant('JQ_CONFIG', {
		chosen:         [   '/bower_components/chosen/chosen.jquery.min.js',
							  '/bower_components/bootstrap-chosen/bootstrap-chosen.css']
						  
		}
	);
