package it.smee.jboxlib;

/* 
 * A JBox defines a generic node of a tree
 *     
 * [Component] class of [Composite Pattern]
 */
public interface JBox
{ 		
	public void print();
	public String getName();
	public void setName(String name);
	
	public void addConditionalFilter(String filter_name, String[] filter_params);
	public void addAggregationFilter(String filter_name, String[] filter_params);
	public void resetFilters();
}