package it.smee.consumption.dao;

import it.smee.jboxlib.*;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.System;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Arrays;
import java.math.BigDecimal;
import java.util.ArrayList;


public class JdbcCoalConsumptionDAO implements JBoxDAO {

    private JdbcTemplate jdbcTemplate;
    private String filter_string = "";
    private String name;
    private List< Map<String, Object> > hourly = null;
    private List< Map<String, Object> > monthly = null;

    public Double getDayConsumption(int i) {
        return ((BigDecimal) hourly.get(i).get("01_am")).doubleValue() + ((BigDecimal) hourly.get(i).get("02_am")).doubleValue() + ((BigDecimal) hourly.get(i).get("03_am")).doubleValue()
                + ((BigDecimal) hourly.get(i).get("04_am")).doubleValue() + ((BigDecimal) hourly.get(i).get("05_am")).doubleValue() + ((BigDecimal) hourly.get(i).get("06_am")).doubleValue()
                + ((BigDecimal) hourly.get(i).get("07_am")).doubleValue() + ((BigDecimal) hourly.get(i).get("08_am")).doubleValue() + ((BigDecimal) hourly.get(i).get("09_am")).doubleValue()
                + ((BigDecimal) hourly.get(i).get("10_am")).doubleValue() + ((BigDecimal) hourly.get(i).get("11_am")).doubleValue() + ((BigDecimal) hourly.get(i).get("12_am")).doubleValue()
                + ((BigDecimal) hourly.get(i).get("01_pm")).doubleValue() + ((BigDecimal) hourly.get(i).get("02_pm")).doubleValue() + ((BigDecimal) hourly.get(i).get("03_pm")).doubleValue()
                + ((BigDecimal) hourly.get(i).get("04_pm")).doubleValue() + ((BigDecimal) hourly.get(i).get("05_pm")).doubleValue() + ((BigDecimal) hourly.get(i).get("06_pm")).doubleValue()
                + ((BigDecimal) hourly.get(i).get("07_pm")).doubleValue() + ((BigDecimal) hourly.get(i).get("08_pm")).doubleValue() + ((BigDecimal) hourly.get(i).get("09_pm")).doubleValue()
                + ((BigDecimal) hourly.get(i).get("10_pm")).doubleValue() + ((BigDecimal) hourly.get(i).get("11_pm")).doubleValue() + ((BigDecimal) hourly.get(i).get("12_pm")).doubleValue();
    }

    @Override
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public List< Map<String, Object> > getData() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        Date timeDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);
        Date dateDate = cal.getTime();

        String date, time;
        date = dateFormat.format(dateDate);
        time = timeFormat.format(timeDate);

        String sql = "SELECT * FROM smee_datimultiorari WHERE data <> '0000-00-00' " + filter_string;
        String sql2 = "SELECT * FROM smee_consuntivi WHERE 1=1 " + filter_string;

        System.out.println("query1 " + sql);
        System.out.println("query2 " + sql2);
        try {
            hourly = this.jdbcTemplate.queryForList(sql);
            monthly = this.jdbcTemplate.queryForList(sql2);

        } catch(Exception e) {
            System.out.println("JdbcCoalConsumptionDAO::getData()::Query non eseguita");
            System.out.println(e);
        }

        Double tillToday = 0.0, tillMonth = 0.0, tillYear = 0.0, today = 0.0, month = 0.0, year = 0.0;
        Date dt = new Date();
        int hours = dt.getHours(), days = dt.getDate();
        int len = hourly.size(), len2 = monthly.size();
        int todayIndex = 0, thisYearIndex = 0;
        String s1 = "_am", s2 = "_pm";

        Map<Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "gen");map.put(2, "feb");map.put(3, "mar");map.put(4, "apr");map.put(5, "mag");map.put(6, "giu");
        map.put(7, "lug");map.put(8, "ago");map.put(9, "sett");map.put(10, "ott");map.put(11, "nov");map.put(12, "dic");

        //tillMonth
        for (int i=0; i<len; i++) {
            if (((Date) hourly.get(i).get("data")).getMonth() == dt.getMonth()
                    && ((Date) hourly.get(i).get("data")).getYear() == (dt.getYear()-1)) {
                if (((Date) hourly.get(i).get("data")).getDate() < dt.getDate()) {
                    tillMonth += getDayConsumption(i);
                }
                else if (((Date) hourly.get(i).get("data")).getDate() == dt.getDate()) {
                    todayIndex = i;
                }
            }
        }
        if (len > 0) {
            //tillDay
            for (int i = 1; i <= hours; i++) {
                if (i < 13) {
                    tillToday += ((BigDecimal) hourly.get(todayIndex).get(String.format("%02d", i) + s1)).doubleValue();
                } else {
                    tillToday += ((BigDecimal) hourly.get(todayIndex).get(String.format("%02d", i - 12) + s2)).doubleValue();
                }
            }
            tillMonth += tillToday;
        }

        //tillYear
        for (int i=0; i<len2; i++) {
            if ((Long) monthly.get(i).get("anno") == (dt.getYear()+1899)) {
                thisYearIndex = i;
                for (int j=1; j<=12; j++) {
                    if (dt.getMonth() >= j) {
                        tillYear += ((BigDecimal) monthly.get(i).get(map.get(j))).doubleValue();
                    }
                }
            }
        }
        tillYear += tillMonth;

        if (len > 0) {
            today = getDayConsumption(todayIndex);
        }

        if (len2 > 0) {
            month = ((BigDecimal) monthly.get(thisYearIndex).get(map.get(dt.getMonth() + 1))).doubleValue();
            year = ((BigDecimal) monthly.get(thisYearIndex).get("gen")).doubleValue() + ((BigDecimal) monthly.get(thisYearIndex).get("feb")).doubleValue()
                    + ((BigDecimal) monthly.get(thisYearIndex).get("mar")).doubleValue() + ((BigDecimal) monthly.get(thisYearIndex).get("apr")).doubleValue()
                    + ((BigDecimal) monthly.get(thisYearIndex).get("mag")).doubleValue() + ((BigDecimal) monthly.get(thisYearIndex).get("giu")).doubleValue()
                    + ((BigDecimal) monthly.get(thisYearIndex).get("lug")).doubleValue() + ((BigDecimal) monthly.get(thisYearIndex).get("ago")).doubleValue()
                    + ((BigDecimal) monthly.get(thisYearIndex).get("sett")).doubleValue() + ((BigDecimal) monthly.get(thisYearIndex).get("ott")).doubleValue()
                    + ((BigDecimal) monthly.get(thisYearIndex).get("nov")).doubleValue() + ((BigDecimal) monthly.get(thisYearIndex).get("dic")).doubleValue();
        }

        Map<String, Object> consumption = new HashMap<String, Object>();
        List<Map<String, Object> > result = new ArrayList<Map<String, Object> >();

        //Double Co2Factor = 0.527;
        Double Co2Factor = 0.41;
        Double Co2ConsumedPerYear = 200.0;
        Double treesPerYear = year*Co2Factor / Co2ConsumedPerYear;

        consumption.put("tillToday", tillToday*Co2Factor);
        consumption.put("tillMonth", tillMonth*Co2Factor);
        consumption.put("tillYear", tillYear*Co2Factor);
        consumption.put("today", today*Co2Factor);
        consumption.put("month", month*Co2Factor);
        consumption.put("year", year*Co2Factor);
        consumption.put("trees", treesPerYear);

        result.add(consumption);

        return result;
    }



    @Override
    public void resetFilters() {
        filter_string = "";

    }

    private String podFilter(String[] pod)
    {
        int i=0;
        String s = "&& pod = '" + pod[i] + "'";
        while(++i<pod.length)
            s = s.concat(" OR pod = '" + pod[i] + "'");

        return s;
    }

    private String provFilter(String[] prov)
    {
        int i=0;
        String s = "&& idComune IN (SELECT idComune from smee_comuni WHERE provincia = '" + prov[i] + "'";
        while(++i<prov.length)
            s = s.concat(" OR provincia = '" + prov[i] + "'");
        s = s.concat(")");

        return s;
    }

    private String creemFilter(String[] creem)
    {
        String s = "&& sitoCreem = '" + creem[0] + "'";
        return s;
    }


    @Override
    public void addConditionalFilter(String filter_name, String[] filter_params) {

        if(filter_name.compareTo("POD") == 0)
            filter_string = filter_string.concat( podFilter(filter_params));
        if(filter_name.compareTo("PROV") == 0)
            filter_string = filter_string.concat( provFilter(filter_params));
        if(filter_name.compareTo("CREEM") == 0)
            filter_string = filter_string.concat( creemFilter(filter_params));
    }

    @Override
    public void print() {
        System.out.println("--- JdbcCoalConsumptionDAO");
    }

    @Override
    public void addAggregationFilter(String filter_name, String[] filter_params)
    {
    }


    @Override
    public String getName() {
        return name;
    }



    @Override
    public void setName(String name) {
        this.name = name;
    }


    public void AggiornaImmobile() {

    }


    private int getIdComune(String comune, String prov) {
        int idComune = -1;
        String sql;
        try{
            sql = "SELECT  idComune " +
                    "FROM smee_comuni " +
                    "where nome='" + comune + "' AND siglaprov='" + prov + "'";

            idComune = this.jdbcTemplate.queryForObject(sql, Integer.class);
        }catch(Exception e){
            System.out.println(e);
        }


        return idComune;

    }



}