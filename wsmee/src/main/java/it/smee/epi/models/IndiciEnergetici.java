package it.smee.epi.models;

import java.util.ArrayList;

public class IndiciEnergetici {
	
	private ArrayList<IndiceEnergetico> indici;

	
	public ArrayList<IndiceEnergetico> getIndiciEnergetici() {
		return indici;
	}
	
	public void setIndiciEnergetici(ArrayList<IndiceEnergetico> indici) {
		this.indici = indici;
	}

}
