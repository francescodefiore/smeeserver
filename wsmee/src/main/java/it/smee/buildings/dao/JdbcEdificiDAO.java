package it.smee.buildings.dao;

import it.smee.buildings.models.Componente;
import it.smee.buildings.models.Edificio;
import it.smee.buildings.models.EdificioAcotel;
import it.smee.buildings.models.Impianto;
import it.smee.buildings.models.NumeroComponenti;
import it.smee.epi.models.DatiEdificio;
import it.smee.epi.models.DatiGenerali;
import it.smee.epi.models.Irradianza;
import it.smee.epi.models.ModuloEpi;
import it.smee.jboxlib.*;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public class JdbcEdificiDAO implements JBoxDAO {

	private JdbcTemplate jdbcTemplate;
	private String filter_string = "";
	private String name;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	
	@Override	
	public List< Map<String, Object> > getData() {
		
		List< Map<String, Object> > immobili = null;
		
		String sql = "SELECT * " +
			     	 "FROM (smee_immobili LEFT JOIN smee_comuni ON smee_immobili.idComune=smee_comuni.idComune) LEFT JOIN smee_policies_immobili ON smee_policies_immobili.idImmobile=smee_immobili.idImmobile,  smee_epi_riscaldamento_ore o  " +
			     	 "WHERE 1=1 AND o.zona=smee_comuni.zona "+ filter_string;
			     
	
	
		System.out.println("query " + sql);
		
		
		try{
				immobili = this.jdbcTemplate.queryForList(sql);
				       				               				 
		}catch(Exception e)
		{
			 System.out.println("JdbcEdificiDAO::getData()::Query non eseguita");
			 System.out.println(e);			
		}
		
		return immobili;
	}

	
	
	@Override
	public void resetFilters() {
		filter_string = "";
		
	}
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = "&& pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		
		return s;
	}
	
	private String idFilter(String[] id)
	{			
		int i=0;
		String s = "&& smee_immobili.idImmobile = '" + id[i] + "'"; 
		while(++i<id.length)
			s = s.concat(" OR smee_immobili.idImmobile = '" + id[i] + "'");
		
		return s;
	}
	
	private String provFilter(String[] prov)
	{			
		int i=0;
		String s = "&& idComune IN (SELECT idComune from smee_comuni WHERE provincia = '" + prov[i] + "'"; 
		while(++i<prov.length)
			s = s.concat(" OR provincia = '" + prov[i] + "'");
		s = s.concat(")");
		
		return s;
	}
	
	private String creemFilter(String[] creem)
	{					
		String s = "&& sitoCreem = '" + creem[0] + "'";
		return s;
	}


	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {		
		
		if(filter_name.compareTo("POD") == 0)
			filter_string = filter_string.concat( podFilter(filter_params));		
		if(filter_name.compareTo("PROV") == 0)
			filter_string = filter_string.concat( provFilter(filter_params));
		if(filter_name.compareTo("CREEM") == 0)
			filter_string = filter_string.concat( creemFilter(filter_params));
		if(filter_name.compareTo("ID") == 0)
			filter_string = filter_string.concat( idFilter(filter_params));
	}

	@Override
	public void print() {
		System.out.println("--- JdbcEdificiDAO");
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) 
	{
		// TODO Auto-generated method stub	
	}


	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	
	
	@Override
	public void setName(String name) {
		this.name = name;		
	}
		
	public List<Impianto> getImpianti(Integer idImmobile)
	{
		List<Impianto> impianti = null;
		
		String sql = "SELECT * from smee_impianti join smee_tipi_impianti using(idTipoImp) WHERE idImmobile=" + idImmobile;

		
		impianti  = this.jdbcTemplate.query(sql,
				new BeanPropertyRowMapper<Impianto>(Impianto.class));
		
		System.out.println("EPIEdificio::getImpianti " + sql);
		
		for(Impianto i : impianti)
			i.setComponenti(this.getComponenti(i.getIdImpianti()));
		
		
		return impianti;
	
	}
	
	public List<Componente> getComponenti(Integer idImpianto)
	{
		List<Componente> componenti = null;
		
		String sql = "SELECT * FROM smee_impianti_componente join smee_impianti using(idImpianti) join smee_componente on smee_componente.idComp=smee_impianti_componente.idComponente where idImpianti='"+ idImpianto +"';";
		
		System.out.println("EPIEdificio::getComponenti " + sql);
		
		componenti = this.jdbcTemplate.query(sql,  new BeanPropertyRowMapper<Componente>(Componente.class));
		
		return componenti;
		
		
	}
	
	public Edificio getEdificio(Integer idimmobile)
	{
			
			String sql = "";
			Edificio edificio = new Edificio();
			try {
						sql = "SELECT smee_immobili.idImmobile, pod, codice, denominazione, indirizzoPod, indirizzo, latitudine, longitudine, areaImmobiliare, potenzaInstallata, tipologiaFornitura, frazionario, classificazione, tipoTurno, funzioniBusiness, clusterEnergy, occupazioneImmobile, destinazioneDuso, nome, siglaprov, provincia, zona, orarioLunVen, orarioSab  FROM smee_immobili JOIN smee_comuni USING (idComune) LEFT JOIN smee_policies_immobili ON smee_policies_immobili.idImmobile=smee_immobili.idImmobile WHERE smee_immobili.idImmobile= "+ idimmobile;

						
						edificio = this.jdbcTemplate.queryForObject(
								sql,  null, new RowMapper<Edificio>() {
						            public Edificio mapRow(ResultSet rs, int rowNum) throws SQLException {
						            	Edificio edificio = new Edificio();
						            	edificio.setIdImmobile(rs.getInt("idImmobile"));
						            	edificio.setPod(rs.getString("pod"));
						            	edificio.setCodice(rs.getString("codice"));
						            	edificio.setDenominazione(rs.getString("denominazione"));
						            	edificio.setIndirizzo(rs.getString("indirizzo"));
						            	edificio.setIndirizzoPod(rs.getString("indirizzoPod"));
						            	edificio.setLatitudine(rs.getDouble("latitudine"));
						            	edificio.setLongitudine(rs.getDouble("longitudine"));
						            	edificio.setAreaImmobiliare(rs.getString("areaImmobiliare"));
						            	edificio.setPotenzaInstallata(rs.getInt("potenzaInstallata"));
						            	edificio.setTipologiaFornitura(rs.getString("tipologiaFornitura"));
						            	edificio.setFrazionario(rs.getString("frazionario"));
						            	edificio.setClassificazione(rs.getString("classificazione"));
						            	edificio.setTipoTurno(rs.getString("tipoTurno"));
						            	edificio.setFunzioniBusiness(rs.getString("funzioniBusiness"));
						            	edificio.setClusterEnergy(rs.getString("clusterEnergy"));
						            	edificio.setOccupazioneImmobile(rs.getString("occupazioneImmobile"));
						            	edificio.setDestinazioneDuso(rs.getString("destinazioneDuso"));
						            	edificio.setNome(rs.getString("nome"));
						            	edificio.setSiglaprov(rs.getString("siglaprov"));
						            	edificio.setProvincia(rs.getString("provincia"));
						            	edificio.setZona(rs.getString("zona"));
						            	edificio.setTipoTurno(rs.getString("tipoturno"));
						            	edificio.setOrarioLunVen(rs.getString("orarioLunVen"));
						            	edificio.setOrarioSab(rs.getString("orarioSab"));
						            	
						                return edificio;
						            }});
						
					
			}
			catch (Exception e) {
			    System.out.println("EPIEdificio::Query non eseguita su modulo_epi " + e );
			    
			}
			
			return edificio;
		
	}


	public void AggiornaImmobile(Edificio building) {
		
		System.out.println("==== AggiornaImmobile new request ====");
		
		int idImmobile = (building.getIdImmobile());		
		String denominazione = building.getDenominazione();
		String indirizzo = building.getIndirizzo();
		String indirizzoPod = building.getIndirizzoPod();		
		double latitudine = building.getLatitudine();
		double longitudine = building.getLongitudine();
		String classificazione = building.getClassificazione();
		String destinazioneDuso = building.getDestinazioneDuso();
		String areaImmobiliare = building.getAreaImmobiliare();
		int potenzaInstallata = building.getPotenzaInstallata();
		String tipologiaFornitura = building.getTipologiaFornitura();
		String frazionario = building.getFrazionario();
		String tipoTurno = building.getTipoTurno();
		String funzioniBusiness = building.getFunzioniBusiness();
		String occupazioneImmobile = building.getOccupazioneImmobile();
		

		try{
			
			String sql;

		
			sql = "UPDATE smee_immobili "+
					      "SET denominazione = '"+ denominazione +"', indirizzo = '"+ indirizzo  +"', indirizzoPod = '"+ indirizzoPod +"', " + 					     
					      " latitudine = "+ latitudine+ ", longitudine = " + longitudine + ",  " +
					      " tipologiaFornitura = '" + tipologiaFornitura + "', " +
					      " classificazione = '" + classificazione + "', " + 
					      " destinazioneDuso = '" + destinazioneDuso + "', areaImmobiliare = '" + areaImmobiliare + "', " +
					      " potenzaInstallata = " + potenzaInstallata + ", frazionario = '" + frazionario + "', " +
					      " tipoTurno = '" + tipoTurno + "', " + " funzioniBusiness = '" + funzioniBusiness + "', " +
					      " occupazioneImmobile = '" + occupazioneImmobile + "' " +
					      " where idImmobile='" + idImmobile + "'";
				
				this.jdbcTemplate.update(sql);
				System.out.println("==== "+sql+" ====");	
				
		}catch(Exception e){
			
			System.out.println(e);	
			
		}
	}
	
	
	public List<EdificioAcotel> getImmobiliAcotel()
	{
		List<EdificioAcotel> edificioAcotel = null;
		
		String sql = "SELECT idImmobile,codiceImmobile as codice FROM smee_immobili_acotel order by codiceImmobile;";
		
		System.out.println("EPIEdificio::getComponenti " + sql);
		
		edificioAcotel = this.jdbcTemplate.query(sql,  new BeanPropertyRowMapper<EdificioAcotel>(EdificioAcotel.class));
		
		return edificioAcotel;
		
		
	}
	
	public List<NumeroComponenti> getNumeroComponenti(Integer idImmobile)
	{
		List<NumeroComponenti> impianti = null;
		
		String sql = "SELECT ifnull(comp.numeroComp,0) numeroComp,ifnull(comp.idTipoImp,0) idTipoImp,ifnull(comp.tipoImp,0) tipoImp " +
					 "FROM smee_immobili im " +
					 "left join " +
					 "( " +
					 "SELECT count(*) numeroComp,i.idTipoImp,t.tipoImp,i.IdImmobile " +
					 "from smee_impianti i " +
					 "inner join smee_impianti_componente c " +
					 "on i.idimpianti = c.idImpianti " +
					 "inner join smee_tipi_impianti t " +
					 "on t.idTipoImp = i.idTipoImp " +
					 "where i.IdImmobile = " + idImmobile + " " +
					 "group by i.IdImmobile, i.idTipoImp " +
					 ")comp " +
					 "on im.idImmobile = comp.IdImmobile " +
					 "where im.IdImmobile = " + idImmobile;
		
		impianti  = this.jdbcTemplate.query(sql,
				new BeanPropertyRowMapper<NumeroComponenti>(NumeroComponenti.class));
		
		System.out.println("EPIEdificio::getNumeroComponenti " + sql);
		
		Edificio edificio = this.getEdificio(idImmobile);
		
		for(NumeroComponenti c : impianti)
			c.setEdificio(edificio);
		
		
		return impianti;
	
	}
	
	

}