package it.smee.epi.dao;

import it.smee.epi.models.*;
import it.smee.jboxlib.JBoxDAO;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;


public class JdbcEpiDAO implements JBoxDAO{
	private JdbcTemplate jdbcTemplate;
	private String filter_string = "";
	private String filter_columns = "*";
	private String aggregation_string = "";
	private String ordering_string = "";
	
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


	@Override
	public void print() {
		System.out.println("--- JdbcEpiDAO");		
	}
	
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = " && (idImmobile IN (SELECT idImmobile FROM smee_immobili WHERE pod ='" + pod[0] + "'";	
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		s = s.concat(")");
		return s;
	}
	
	private String provFilter(String[] prov)
	{	
		int i=0;
		String s = " && idComune IN (SELECT idComune FROM smee_comuni WHERE siglaprov ='" + prov[0] + "'";	
		while(++i<prov.length)
			s = s.concat(" OR siglaprov = '" + prov[i] + "'");
		s = s.concat(")");
		return s;
	}
	
	private String clusterFilter(String[] cluster)
	{		
		int i=0;
		String s = " && pod IN (SELECT pod FROM smee_immobili WHERE clusterEnergy ='" + cluster[0] + "'";
		while(++i<cluster.length)
			s = s.concat(" OR clusterEnergy = '" + cluster[i] +"'");
		s = s.concat(")");
		return s;
	}
	

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{
			case "PROV":
				filter_string = filter_string.concat( provFilter(filter_params) );
				break;
				
			case "CLUSTER":	
				filter_string = filter_string.concat( clusterFilter(filter_params) );				
				break;
				
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
	
		}
		
	}



	@Override
	public void resetFilters() {
		filter_string = "";
		filter_columns = "*";
		aggregation_string = "";
		ordering_string = "";
		
	}



	@Override
	public List< Map<String, Object > > getData() {
		
		ArrayList< Map<String, Object> > Indici = new ArrayList<Map<String, Object>>();
		
		
		String sql = "SELECT " + filter_columns + " " +
			     "FROM smee_epi_prestazioni_energetiche " +
			     "WHERE 1=1 "+ filter_string + " " +
			     aggregation_string + " " +
			     ordering_string + " ";
	
		System.out.println("query " + sql);
		List< Map<String, Object> > results = null;
		
		
		try {
			
			IndiceEnergetico m_index;
			float index = 0.0f;
			
			results = this.jdbcTemplate.queryForList(sql);
			
			Map<String, Object> c = new HashMap<String, Object>();
			for (Map<String, Object> m : results)
			{
					/*
				index = ((Float) m.get("coefficientescambiotermico")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Hti");
				m_index.setNome("Coefficiente Invernale di Scambio Termico per Trasmissione");
				m_index.setValore(index);
				m_index.setUnitamisura("W/K");
				c.put("coefficientescambiotermico", m_index);
					
				index = ((Float)m.get("benchmarkscambiotermico")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Htbenchi");
				m_index.setNome("Benchmarch Coefficiente Invernale di Scambio Termico per Trasmissione");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh");
				c.put("benchmarkscambiotermico", m_index);
				
				index = ((Float) m.get("coefficientescambioventilazione")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Hvi");
				m_index.setNome("Coefficiente Invernale di Scambio Termico per Ventilazione");
				m_index.setValore(index);
				m_index.setUnitamisura("W/K");
				c.put("coefficientescambioventilazione", m_index);
				
				index = ((Float) m.get("apportigratuitiinterni")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Qii");
				m_index.setNome("Apporti Gratuiti Interni Invernali");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh");
				c.put("apportigratuitiinterni", m_index);
				
				index = ((Float) m.get("apportisolari")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Qsi");
				m_index.setNome("Apporti Solari Invernali");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh");
				c.put("apportisolari", m_index);
				
				index = ((Float) m.get("fabbisognoenergiatermica")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Qhi");
				m_index.setNome("Fabbisogno Netto Energia Termica Invernale");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh");
				c.put("fabbisognoenergiatermica", m_index);
				
				index = ((Float) m.get("rendimentoglobale_caldaie")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("ETAg - Caldaie");
				m_index.setNome("Rendimento Globale Di Climatizzazione Invernale con Caldaie");
				m_index.setValore(index);
				m_index.setUnitamisura("%");
				c.put("rendimentoglobale_caldaie", m_index);
				
				index = ((Float) m.get("rendimentoglobale_PdC")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("ETAg - PdC");
				m_index.setNome("Rendimento Globale Di Climatizzazione Invernale con Pompe di Calore");
				m_index.setValore(index);
				m_index.setUnitamisura("%");
				c.put("rendimentoglobale_PdC", m_index);
				
				index = ((Float) m.get("episuperficieutile")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPIsn");
				m_index.setNome("Indice Prestazione Energetica Invernale Riferita alla Superficie Utile");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/m&sup2;*anno");
				c.put("episuperficieutile", m_index);
				
				index = ((Float) m.get("episuperficieutile_GG")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPIsnGGi");
				m_index.setNome("Indice Prestazione Energetica Invernale Riferita alla Superficie Utile Normalizzato alla Condizione Climatica (GG)");
				m_index.setValore(index);
				m_index.setUnitamisura("W/m&sup2;K");
				c.put("episuperficieutile_GG", m_index);
				
				index = ((Float) m.get("episuperficielorda")).floatValue();
				m_index = new IndiceEnergetico();				
				m_index.setSimbolo("EPIsl");
				m_index.setNome("Indice Prestazione Energetica Invernale Riferita alla Superficie Lorda");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/m&sup2;*anno");
				c.put("episuperficielorda", m_index);
				
				index = ((Float) m.get("episuperficielorda_GG")).floatValue();
				m_index = new IndiceEnergetico();				
				m_index.setSimbolo("EPIslGGi");
				m_index.setNome("Indice Prestazione Energetica Invernale Riferita alla Superficie Lorda Normalizzato alla Condizione Climatica (GG)");
				m_index.setValore(index);
				m_index.setUnitamisura("W/m&sup2;K");
				c.put("episuperficielorda_GG", m_index);
				
				index = ((Float) m.get("epivolumelordo")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPIvl");
				m_index.setNome("Indice Prestazione Energetica Invernale Riferita al Volume Lordo");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/m&sup2;*anno");
				c.put("epivolumelordo", m_index);
				
				index = ((Float) m.get("epivolumelordo_GG")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPIvlGGi");
				m_index.setNome("Indice Prestazione Energetica Invernale Riferita al Volume Lordo Normalizzato alla Condizione Climatica (GG)");
				m_index.setValore(index);
				m_index.setUnitamisura("W/m&sup2;K");
				c.put("epivolumelordo_GG", m_index);
				
				index = ((Float) m.get("coefficientescambiotermico_estivo")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Hte");
				m_index.setNome("Coefficiente Estivo di Scambio Termico per Trasmissione");
				m_index.setValore(index);
				m_index.setUnitamisura("W/K");
				c.put("coefficientescambiotermico_estivo", m_index);
				
				index = ((Float) m.get("coefficientescambioventilazione_estivo")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Hve");
				m_index.setNome("Coefficiente Estivo Scambio Termico per Ventilazione");
				m_index.setValore(index);
				m_index.setUnitamisura("W/K");
				c.put("coefficientescambioventilazione_estivo", m_index);
				
				index = ((Float) m.get("apportigratuitiinterni_estivo")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Qie");
				m_index.setNome("Carico Estivo per Apporti Interni");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh");
				c.put("apportigratuitiinterni_estivo", m_index);
				
				index = ((Float) m.get("apportisolari_estivo")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Qse");
				m_index.setNome("Apporti Solari Estivi");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh");
				c.put("apportisolari_estivo", m_index);
				
				index = ((Float) m.get("fabbisognoenergiatermica_estivo")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("Qhe");
				m_index.setNome("Fabbisogno Netto Energia per Raffrescamento Edificio");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh");
				c.put("fabbisognoenergiatermica_estivo", m_index);
				
				index = ((Float) m.get("rendimentoglobale_estivo")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("ETAg - Caldaie");
				m_index.setNome("Rendimento Globale Impianto di Climatizzazione");
				m_index.setValore(index);
				m_index.setUnitamisura("%");
				c.put("rendimentoglobale_estivo", m_index);
				
				index = ((Float) m.get("epesuperficieutile")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPEsn");
				m_index.setNome("Indice Prestazione Energetica Estiva Riferita alla Superficie Utile");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/m&sup2;*anno");
				c.put("epesuperficieutile", m_index);
				
				index = ((Float) m.get("epEsuperficieutile_GGe")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPEsnGGe");
				m_index.setNome("Indice Prestazione Energetica Estiva Riferita alla Superficie Utile Normalizzato alla Condizione Climatica (GG)");
				m_index.setValore(index);
				m_index.setUnitamisura("W/m&sup2;K");
				c.put("epEsuperficieutile_GGe", m_index);
				
				index = ((Float) m.get("epesuperficielorda")).floatValue();
				m_index = new IndiceEnergetico();				
				m_index.setSimbolo("EPEsl");
				m_index.setNome("Indice Prestazione Energetica Estiva Riferita alla Superficie Lorda");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/m&sup2;*anno");
				c.put("epesuperficielorda", m_index);
				
				index = ((Float) m.get("epesuperficielorda_GGe")).floatValue();
				m_index = new IndiceEnergetico();				
				m_index.setSimbolo("EPEslGGe");
				m_index.setNome("Indice Prestazione Energetica Estiva Riferita alla Superficie Lorda Normalizzato alla Condizione Climatica (GG)");
				m_index.setValore(index);
				m_index.setUnitamisura("W/m&sup2;K");
				c.put("epesuperficielorda_GGe", m_index);
				
				index = ((Float) m.get("epevolumelordo")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPEvl");
				m_index.setNome("Indice Prestazione Energetica Estiva Riferita al Volume Lordo");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/m&sup2;*anno");
				c.put("epevolumelordo", m_index);
				
				index = ((Float) m.get("epevolumelordo_GGe")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPEvlGGe");
				m_index.setNome("Indice Prestazione Energetica Estiva Riferita al Volume Lordo Normalizzato alla Condizione Climatica (GG)");
				m_index.setValore(index);
				m_index.setUnitamisura("W/m&sup2;K");
				c.put("epevolumelordo_GGe", m_index);
				
				index = ((Float) m.get("consumo_illesterna")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("kWhIllEst");
				m_index.setNome("Consumo Energia per Illuminazione Esterna");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/anno");
				c.put("consumo_illesterna", m_index);
				
				index = ((Float) m.get("consumo_illinterna")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("kWhIllInt");
				m_index.setNome("Consumo Energia per Illuminazione Interna");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/anno");
				c.put("consumo_illinterna", m_index);
				
				index = ((Float) m.get("epillinterna")).floatValue();
				m_index = new IndiceEnergetico();
				m_index.setSimbolo("EPill");
				m_index.setNome("Indice Prestazione Energetica Illuminazione Interna");
				m_index.setValore(index);
				m_index.setUnitamisura("kWh/anno");
				c.put("epillinterna", m_index);
							*/		
				Indici.add(c);
				
				break; // estrai solo una riga da questa tabella

			}
			

		
			} 		
			catch (Exception e) {
				    System.out.println(" IndiciEdificio::Query non eseguita su prestazioni_energetiche" + e);
				    
			}
		
		
		return Indici;

	}


	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}
	
	

	

}
