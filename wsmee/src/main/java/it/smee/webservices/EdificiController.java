package it.smee.webservices;

import it.smee.buildings.dao.JdbcEdificiDAO;
import it.smee.buildings.dao.JdbcPoliciesDAO;
import it.smee.buildings.models.Componente;
import it.smee.buildings.models.Edificio;
import it.smee.buildings.models.EdificioAcotel;
import it.smee.buildings.models.Impianto;
import it.smee.buildings.models.NumeroComponenti;
import it.smee.buildings.models.Policies;
import it.smee.epi.dao.JdbcModuloEpiDAO;
import it.smee.epi.models.ModuloEpi;
import it.smee.jboxlib.JBoxDAO;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Controller
public class EdificiController {
	
	private static final ApplicationContext context =	new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO edificiDAO = (JBoxDAO) context.getBean("edificiDAO");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO policiesDAO = (JBoxDAO) context.getBean("policiesDAO");
	
	
	@RequestMapping(value = "/immobili", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> edifici( 
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups)
    {  
	
		edificiDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				edificiDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )			
			for(int i=0; i<groups.split(";").length; i++)
				edificiDAO.addAggregationFilter( groups.split(";")[i], filter_params.get(i).split(";") );


		return edificiDAO.getData();		
    }
    
	
	@RequestMapping(value = "/immobile", method = RequestMethod.GET)
	public @ResponseBody Edificio query(@RequestParam(value="idImmobile", required=true) Integer idimmobile) {
		
		return ((JdbcEdificiDAO)edificiDAO).getEdificio(idimmobile);
	}
	
	@RequestMapping(value = "/immobile", method = RequestMethod.POST)
	public @ResponseBody Edificio updateimmobile(@RequestBody Edificio building) {

		System.out.println("==== /immobile POST ====");
		
		((JdbcEdificiDAO)edificiDAO).AggiornaImmobile(building);
		return building;
	}
	
	@RequestMapping(value = "/impianti", method = RequestMethod.GET)
	public @ResponseBody List<Impianto> getImpianti(@RequestParam(value="idImmobile", required=true) Integer idimmobile) {
		
		return ((JdbcEdificiDAO)edificiDAO).getImpianti(idimmobile);
	}
	
	@RequestMapping(value = "/impianti/componenti", method = RequestMethod.GET)
	public @ResponseBody List<Componente> getComponenti(@RequestParam(value="idImpianto", required=true) Integer idimpianto) {
		
		return ((JdbcEdificiDAO)edificiDAO).getComponenti(idimpianto);
	}
	
	
	@RequestMapping(value = "/policies", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> policies( 
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params)
    {  
	
		policiesDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				policiesDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		return policiesDAO.getData();		
    }
	
	@RequestMapping(value = "/policies/update", method = RequestMethod.POST)
	public ResponseEntity<Policies> update(@RequestBody Policies policies) {

		System.out.println("==== /policies/update ====");
	
		
		((JdbcPoliciesDAO)policiesDAO).AggiornaPolicies(policies);
		return new ResponseEntity<Policies>(policies, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/immobili/acotel", method = RequestMethod.GET)
	public @ResponseBody List<EdificioAcotel> getImmobiliAcotel() {
		
		return ((JdbcEdificiDAO)edificiDAO).getImmobiliAcotel();
	}
	
	@RequestMapping(value = "/impianti/numerocomponenti", method = RequestMethod.GET)
	public @ResponseBody List<NumeroComponenti> getNumeroComponenti(@RequestParam(value="idImmobile", required=true) Integer idimmobile) {
		
		return ((JdbcEdificiDAO)edificiDAO).getNumeroComponenti(idimmobile);
	}
	
	

}
