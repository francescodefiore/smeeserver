package it.smee.webservices;


import it.smee.jboxlib.JBoxDAO;
import it.smee.power.dao.JdbcMultiorariaDAO;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;


@Controller
public class MultiorarieController {
	
	private static final ApplicationContext context =	new ClassPathXmlApplicationContext("Spring-Module.xml");
	
	@SuppressWarnings("unchecked")
	private static final JBoxDAO multiorarieDAO = (JBoxDAO) context.getBean("multiorarieDAO");
	
	public MultiorarieController() 
	{
		System.out.println("==== /ControllerMultiorarie ====");
	}
	
	@RequestMapping(value = "/multiorarie", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> multi( 
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,    
    		@RequestParam(value="groupByParams", required=false) List<String> group_params)
    {  
	
		multiorarieDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				multiorarieDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )	
			for(int i=0; i<groups.split(";").length; i++)
				multiorarieDAO.addAggregationFilter( groups.split(";")[i], group_params.get(i).split(";") );
		

		return multiorarieDAO.getData();	
    }
	
	@RequestMapping(value="/consumi/upload", method=RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    public void handleFileUpload(@RequestParam("file") MultipartFile file){
		System.out.println("==== /consumi/upload new request ====");
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Calendar c = Calendar.getInstance();
                String directory = "./imported/consumi";
                boolean success=(new File(directory)).mkdirs();		           
	            
	            	String name = directory + "/"+c.getTimeInMillis()+"_consumi.csv";
	            	BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name)));
	            	stream.write(bytes);
	            	stream.close();
	            	((JdbcMultiorariaDAO)multiorarieDAO).FileImport(name);
	            
                
            } catch (Exception e) { }
        }
    }
	
	
	@RequestMapping(value = "/multiorarie/podf1f2f3", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> podf1f2f3(
      		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params)
    {  
		multiorarieDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				multiorarieDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

		return ((JdbcMultiorariaDAO) multiorarieDAO).getPodF1F2F3();
    }
	
	@RequestMapping(value = "/multiorarie/podf1f2f3/anno", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> podf1f2f3Anno(
      		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="year", required=false) String year)
    {  
		multiorarieDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				multiorarieDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

		return ((JdbcMultiorariaDAO) multiorarieDAO).getPodF1F2F3Anno(year);
    }
	
	
	@RequestMapping(value = "/multiorarie/clusterf1f2f3", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> clusterf1f2f3(
      		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params)
    {  
		multiorarieDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				multiorarieDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

		return ((JdbcMultiorariaDAO) multiorarieDAO).getClusterF1F2F3();
    }
	
	@RequestMapping(value = "/multiorarie/clusterf1f2f3/anno", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> clusterf1f2f3Anno(
      		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="year", required=false) String year)
    {  
		multiorarieDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				multiorarieDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

		return ((JdbcMultiorariaDAO) multiorarieDAO).getClusterF1F2F3Anno(year);
    }
	
	@RequestMapping(value = "/multiorarie/provincef1f2f3", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> provincef1f2f3(  		
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params)
    {  
		multiorarieDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				multiorarieDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

		return ((JdbcMultiorariaDAO) multiorarieDAO).getProvinceF1F2F3();
    }
	
	@RequestMapping(value = "/multiorarie/provincef1f2f3/anno", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> provincef1f2f3Anno(  		
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="year", required=false) String year)
    {  
		multiorarieDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				multiorarieDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));

		return ((JdbcMultiorariaDAO) multiorarieDAO).getProvinceF1F2F3Anno(year);
    }
	
	
	@RequestMapping(value = "/multiorarie/acotel", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody List< Map<String, Object>> dataAcotel( 
    		@RequestParam(value="filterBy", required=false) List<String> filters,
    		@RequestParam(value="filterParams", required=false) List<String> filter_params,
    		@RequestParam(value="groupBy", required=false) String groups,    
    		@RequestParam(value="groupByParams", required=false) List<String> group_params)
    {  
	
		multiorarieDAO.resetFilters();

		if( (filters!=null) && (filter_params!=null))
			for(int i=0; i<filters.size(); i++)				
				multiorarieDAO.addConditionalFilter(filters.get(i), filter_params.get(i).split(";"));
		
		if( groups!=null )	
			for(int i=0; i<groups.split(";").length; i++)
				multiorarieDAO.addAggregationFilter( groups.split(";")[i], group_params.get(i).split(";") );
		

		return ((JdbcMultiorariaDAO) multiorarieDAO).getDatiAcotel();	
    }

    
}