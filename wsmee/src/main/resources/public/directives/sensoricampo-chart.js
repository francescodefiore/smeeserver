CREEMapp.directive('sensoriChart', 

function() 
{

	return {
    scope: {},
    controller: ['$rootScope', '$scope', 'CreemSettings', 'BusDataFactory', 
	
		function($rootScope, $scope, CreemSettings, BusDataFactory)
		{			
			var seriesCounter = 0;
			$scope.data = [];
			$scope.sensordata = {};
						
			
			var createChart = function(){
				var today = moment().format("dddd, MMMM Do YYYY");
				
				var histdaticampochart = new Highcharts.Chart({
					chart: {
						renderTo: 'histdaticampo',
						type: 'spline',
						zoomType: 'x'
					},
					title: {
						text: 'Sensori di campo - ' + today
					},
					
					credits: {
						enabled: false
					},
					
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: { // don't display the dummy year
							month: '%e. %b',
							year: '%b'
						},
						title: {
							text: 'Date'
						}
					},
					yAxis: [{ // Primary yAxis
								labels: {
									format: '{value}°C',
									style: {
										color: Highcharts.getOptions().colors[0]
									}
								},
								title: {
									text: 'Temperatura'
								},
								min: 0
							},
							{ 
								labels: {
									format: '{value}uv',
									align: "right",
									style: {
										color: Highcharts.getOptions().colors[1]
									}
								},
								title: {
									text: 'Luminosità'
								},
								opposite: true
							},
							{ 
								labels: {
									enabled: false									
								},
								title: {
									enabled: false									
								}
							}
							
							
							],

					plotOptions: {
					
						spline: {
							marker: {
								enabled: false
							}
						}
					},

					series: $scope.data
				});
			}
			
			

			var getdata = function (building) {
				//if (CreemSettings.selectedMeasures == 'ThermalMeter')
					$scope.sensordata[building.pod] = BusDataFactory.getSensorData(building.idImmobile).query(
						function() {
							if($scope.sensordata[building.pod].length) {
								$scope.data.push({
									name: "Temperatura",
									data: prepareTemperatureData($scope.sensordata[building.pod]),
									yAxis: 0,
									type: 'spline'
								});																
								$scope.data.push({
									name: "Luminosità",
									data: prepareLuminosityData($scope.sensordata[building.pod]),
									yAxis: 1,
									dashStyle: 'shortdot',
									type: 'spline'
								});							
								
								$scope.data.push({
									name: "Presenza",
									data: preparePresenceData($scope.sensordata[building.pod]),
									yAxis: 2,
									type: 'column'
								});
								createChart();
								
							}
						}
					);
				
			}
			
			var prepareTemperatureData = function (response)
			{				
				var tot = 0;
				var chartData = [];

				for (var i = 0; i < response.length; i++) {
						var newDate = new Date(response[i]['timestamp']);
							
							if(response[i]['nomeFenomeno'] == "Temperatura")
								chartData.push([
									Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), newDate.getHours(), newDate.getMinutes(), newDate.getSeconds(), newDate.getMilliseconds()),
									parseInt(response[i].valore)
								]);

				}				
				return chartData;
			}
			
			
			var preparePresenceData = function (response)
			{				
				var tot = 0;
				var chartData = [];

				for (var i = 0; i < response.length; i++) {
						var newDate = new Date(response[i]['timestamp']);
							
							if(response[i]['nomeFenomeno'] == "Presenza")
								chartData.push([
									Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), newDate.getHours(), newDate.getMinutes(), newDate.getSeconds(), 0),
									parseInt(response[i].valore)
								]);

				}				
				return chartData;
			}
			
			
			var prepareUmidityData = function (response)
			{				
				var tot = 0;
				var chartData = [];

				for (var i = 0; i < response.length; i++) {
						var newDate = new Date(response[i]['timestamp']);
							
							if(response[i]['nomeFenomeno'] == "Umidita")
								chartData.push([
									Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), newDate.getHours(), newDate.getMinutes(), newDate.getSeconds(), 0),
									parseInt(response[i].valore)
								]);

				}				
				return chartData;
			}
			
			
			var prepareLuminosityData = function (response)
			{				
				var tot = 0;
				var chartData = [];

				for (var i = 0; i < response.length; i++) {
						var newDate = new Date(response[i]['timestamp']);
							
							if(response[i]['nomeFenomeno'] == "Luminosita")
								chartData.push([
									Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), newDate.getHours(), newDate.getMinutes(), newDate.getSeconds(), 0),
									parseInt(response[i].valore)
								]);

				}				
				return chartData;
			}
			
			
			
			var refreshData = function ()
			{										
					$scope.sensordata = {}
					$scope.data = [];
					getdata(CreemSettings.selectedbuildings[0]);					
					
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0)
				refreshData();


			
		}],
		
		templateUrl: 'directives/sensoricampo-chart.html'
  };
  
});
	
	