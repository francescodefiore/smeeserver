package it.smee.buildings.models;

public class Policies{
	
	private String idImmobile;
	private String orarioLunVen;
	private String orarioSab;
	private String TempEstivaAreaLavoro;
	private String TempEstivaChiusura;
	private String TempInvernoAreaLavoro;
	private String TempInvernoChiusura;
	private String oraAccensioneRiscaldamento;
	private String oraSpegnimentoRiscaldamento;
	private String oraAccensioneRaffrescamento;
	private String oraSpegnimentoRaffrescamento;
	private String luxMediaOreLavoro;
	private String luxAreetransito;

	
	public Policies(){}


	public String getLuxMediaOreLavoro() {
		return luxMediaOreLavoro;
	}


	public void setLuxMediaOreLavoro(String luxMediaOreLavoro) {
		this.luxMediaOreLavoro = luxMediaOreLavoro;
	}


	public String getLuxAreetransito() {
		return luxAreetransito;
	}


	public void setLuxAreetransito(String luxAreetransito) {
		this.luxAreetransito = luxAreetransito;
	}


	public String getIdImmobile() {
		return idImmobile;
	}


	public void setIdImmobile(String idImmobile) {
		this.idImmobile = idImmobile;
	}


	public String getOrarioLunVen() {
		return orarioLunVen;
	}


	public void setOrarioLunVen(String orarioLunVen) {
		this.orarioLunVen = orarioLunVen;
	}


	public String getOrarioSab() {
		return orarioSab;
	}


	public void setOrarioSab(String orarioSab) {
		this.orarioSab = orarioSab;
	}


	public String getTempEstivaAreaLavoro() {
		return TempEstivaAreaLavoro;
	}


	public void setTempEstivaAreaLavoro(String tempEstivaAreaLavoro) {
		TempEstivaAreaLavoro = tempEstivaAreaLavoro;
	}


	public String getTempEstivaChiusura() {
		return TempEstivaChiusura;
	}


	public void setTempEstivaChiusura(String tempEstivaChiusura) {
		TempEstivaChiusura = tempEstivaChiusura;
	}


	public String getTempInvernoAreaLavoro() {
		return TempInvernoAreaLavoro;
	}


	public void setTempInvernoAreaLavoro(String tempInvernoAreaLavoro) {
		TempInvernoAreaLavoro = tempInvernoAreaLavoro;
	}


	public String getTempInvernoChiusura() {
		return TempInvernoChiusura;
	}


	public void setTempInvernoChiusura(String tempInvernoChiusura) {
		TempInvernoChiusura = tempInvernoChiusura;
	}


	public String getOraAccensioneRiscaldamento() {
		return oraAccensioneRiscaldamento;
	}


	public void setOraAccensioneRiscaldamento(String oraAccensioneRiscaldamento) {
		this.oraAccensioneRiscaldamento = oraAccensioneRiscaldamento;
	}


	public String getOraSpegnimentoRiscaldamento() {
		return oraSpegnimentoRiscaldamento;
	}


	public void setOraSpegnimentoRiscaldamento(
			String oraSpegnimentoRiscaldamento) {
		this.oraSpegnimentoRiscaldamento = oraSpegnimentoRiscaldamento;
	}


	public String getOraAccensioneRaffrescamento() {
		return oraAccensioneRaffrescamento;
	}


	public void setOraAccensioneRaffrescamento(
			String oraAccensioneRaffrescamento) {
		this.oraAccensioneRaffrescamento = oraAccensioneRaffrescamento;
	}


	public String getOraSpegnimentoRaffrescamento() {
		return oraSpegnimentoRaffrescamento;
	}


	public void setOraSpegnimentoRaffrescamento(String oraSpegnimentoRaffrescamento) {
		this.oraSpegnimentoRaffrescamento = oraSpegnimentoRaffrescamento;
	}
	


}