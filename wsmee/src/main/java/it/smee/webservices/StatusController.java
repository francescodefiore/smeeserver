package it.smee.webservices;

import it.smee.status.dao.StatusDAO;
import it.smee.status.models.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
public class StatusController {
	private static final ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
	private static StatusDAO statusDAO = (StatusDAO) context.getBean("statusDAO");
    
	public StatusController() {
		System.out.println("==== /StatusController ====");
	} 
	
	
	//WebService status
	@RequestMapping(value = "/status", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody Status info_status()
    {
		System.out.println("==== /status new request ====");   
		//return statusDAO.retrieveInfoStatusService();
		return statusDAO.retrieveData();
		
    }
	/*
	@RequestMapping(value = "/jbox", method = RequestMethod.GET,headers="Accept=application/json")
    public @ResponseBody JBoxComponent status_jbox()
    {
		System.out.println("==== /jbox new request ====");   
		
		JBoxComponent C = new JBox("MainBox");		
		JBoxComponent box1 = new JBox("JBox1");
		JBoxComponent box2 = new JBox("JBox2");
		C.add(box1);
		C.add(box2);
				
		box1.add( (JBoxComponent) statusDAO );
		


		return C;
		
    }
    */
}
