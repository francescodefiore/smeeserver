package it.smee.epi.dao;

import it.smee.epi.models.*;
import it.smee.jboxlib.JBoxDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;


public class JdbcDashboardDAO implements JBoxDAO{
	
	private JdbcTemplate jdbcTemplate;
	private String filter_string = "";
	private String filter_columns = "*";
	private String aggregation_string = "";
	private String ordering_string = "";
	
	public void setDataSource(DataSource dataSource) {
		 this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void print() {
		System.out.println("--- JdbcDashboardDAO");
		
	}

	@Override
	public void addConditionalFilter(String filter_name, String[] filter_params) {
		switch(filter_name)
		{		
			case "POD":	
				filter_string = filter_string.concat( podFilter(filter_params) );				
				break;
	
		}		
	}


	@Override
	public void resetFilters() {
		filter_string = "";
		filter_columns = "*";
		aggregation_string = "";
		ordering_string = "";
	}
	
	
	private String podFilter(String[] pod)
	{			
		int i=0;
		String s = "&& pod = '" + pod[i] + "'"; 
		while(++i<pod.length)
			s = s.concat(" OR pod = '" + pod[i] + "'");
		
		return s;
	}

	@Override
	public List< Map<String, Object> > getData() {
		
		List< Map<String, Object> > Dashboard = new ArrayList<Map<String, Object>>();
		DashboardImmobile dashboard = null;
		
		String sql = "";
		
		List<String> warnings = new ArrayList<String>();
		List<String> errors = new ArrayList<String>();
		String last_update = "01/01/1979";
		
		sql = "SELECT  smee_immobili.*, nome as Comune, siglaprov as provincia " +
		     	 "FROM smee_immobili JOIN smee_comuni ON smee_immobili.idComune=smee_comuni.idComune " +
		     	 "WHERE 1=1 "+ filter_string;


		System.out.println("query " + sql);
		List< Map<String,Object> > immobili = new ArrayList<Map<String, Object>>();
		
		try{
			immobili = this.jdbcTemplate.queryForList(sql);
				       				               				 
		}catch(Exception e)
		{
			 System.out.println("JdbcEdificiDAO::getData()::Query non eseguita");
			 System.out.println(e);			
		}


		try {
				
				for (Map<String, Object> m : immobili)
				{
		    
				    dashboard = new DashboardImmobile((String)m.get("pod"), (String)m.get("denominazione"), (String)m.get("indirizzo"), (String)m.get("Comune"), (String)m.get("provincia"), (String)m.get("clusterEnergy"), (Double)m.get("latitudine"),  (Double)m.get("longitudine"), warnings, errors, last_update);
				    //dashboard.setIndici(this.IndiciEdificio((String)m.get("pod")));	
				    dashboard.setIndici(this.IndiciEdificio()); 
				    dashboard.setTipoturno((String)m.get("tipoTurno"));
				}
							
		
		
		
		
				float epiglobale = 0.0f;
	
				epiglobale = dashboard.getIndici().get(7).getValore();
				if (epiglobale < 18.125f)
					dashboard.setClasse_energetica_globale("A+");
				else if(epiglobale < 27.25f)
					dashboard.setClasse_energetica_globale("A");
				else if(epiglobale < 39.376f)
					dashboard.setClasse_energetica_globale("B");
				else if(epiglobale < 54.501f)
					dashboard.setClasse_energetica_globale("C");
				else if(epiglobale < 66.626f)
					dashboard.setClasse_energetica_globale("D");
				else if(epiglobale < 87.876f)
					dashboard.setClasse_energetica_globale("E");
				else if(epiglobale < 121.252f)
					dashboard.setClasse_energetica_globale("F");
				else if(epiglobale >= 121.252f)
					dashboard.setClasse_energetica_globale("G");
				
				
				dashboard.setPrestazione_riscaldamento(46.601f);
				dashboard.setPrestazione_raffrescamento(30.11f);
				dashboard.setPrestazione_acquacalda(20.451f);
				dashboard.setPrestazione_energetica_globale(epiglobale);
				dashboard.setCarbonConsumptionToday(68.11f);
				dashboard.setQualita_involucro("IV");
				
				
		}catch(Exception e)
		{
			System.out.println("getBuildingDashboard::Error found! "+ e);
		}
		Map<String, Object> dd = new HashMap<String, Object>();
		dd.put("dashboard", dashboard);
		
		Dashboard.add(dd);
		
		return Dashboard;
	}
	
	private List<IndiceEnergetico> IndiciEdificio() {
		
		
		ArrayList< IndiceEnergetico > Indici = new ArrayList<IndiceEnergetico>(); 		

		String sql = "SELECT " + filter_columns + " " +
			     "FROM smee_epi_prestazioni_energetiche join smee_immobili ON smee_immobili.idImmobile=smee_epi_prestazioni_energetiche.idImmobile " +
			     "WHERE 1=1 "+ filter_string + " " +
			     aggregation_string + " " +
			     ordering_string + " "; 
		
		/*
		String sql = "SELECT " + filter_columns + " " +
			     "FROM softeco_prestazioni_energetiche " +
			     "WHERE 1=1 AND pod='IT001E91346483'";
		*/
		System.out.println("query " + sql);
		
		
		try {
			
			IndiceEnergetico m_index;
			float index = 0.0f;

			
			List< Map<String, Object> > results = this.jdbcTemplate.queryForList(sql);
			for (Map m : results)
			{
					index = ((Float) m.get("coefficientescambiotermico")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Hti");
					m_index.setNome("Coefficiente Invernale di Scambio Termico per Trasmissione");
					m_index.setValore(index);
					m_index.setUnitamisura("W/K");
					Indici.add(m_index);
					
					index = ((Float)m.get("benchmarkscambiotermico")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Htbenchi");
					m_index.setNome("Benchmarch Coefficiente Invernale di Scambio Termico per Trasmissione");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh");
					Indici.add(m_index);
					
					index = ((Float) m.get("coefficientescambioventilazione")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Hvi");
					m_index.setNome("Coefficiente Invernale di Scambio Termico per Ventilazione");
					m_index.setValore(index);
					m_index.setUnitamisura("W/K");
					Indici.add(m_index);
					
					index = ((Float) m.get("apportigratuitiinterni")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Qii");
					m_index.setNome("Apporti Gratuiti Interni Invernali");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh");
					Indici.add(m_index);
					
					index = ((Float) m.get("apportisolari")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Qsi");
					m_index.setNome("Apporti Solari Invernali");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh");
					Indici.add(m_index);
					
					index = ((Float) m.get("fabbisognoenergiatermica")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Qhi");
					m_index.setNome("Fabbisogno Netto Energia Termica Invernale");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh");
					Indici.add(m_index);
					
					index = ((Float) m.get("rendimentoglobale")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("ETAg - Caldaie");
					m_index.setNome("Rendimento Globale Di Climatizzazione Invernale con Caldaie");
					m_index.setValore(index);
					m_index.setUnitamisura("%");
					Indici.add(m_index);
					
					index = ((Float) m.get("rendimentoglobale_PdC")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("ETAg - PdC");
					m_index.setNome("Rendimento Globale Di Climatizzazione Invernale con Pompe di Calore");
					m_index.setValore(index);
					m_index.setUnitamisura("%");
					Indici.add(m_index);
					
					index = ((Float) m.get("episuperficieutile")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPIsn");
					m_index.setNome("Indice Prestazione Energetica Invernale Riferita alla Superficie Utile");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/m²*anno");
					Indici.add(m_index);
					
					index = ((Float) m.get("episuperficieutile_GG")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPIsnGGi");
					m_index.setNome("Indice Prestazione Energetica Invernale Riferita alla Superficie Utile Normalizzato alla Condizione Climatica (GG)");
					m_index.setValore(index);
					m_index.setUnitamisura("W/m²K");
					Indici.add(m_index);
					
					index = ((Float) m.get("episuperficielorda")).floatValue();
					m_index = new IndiceEnergetico();				
					m_index.setSimbolo("EPIsl");
					m_index.setNome("Indice Prestazione Energetica Invernale Riferita alla Superficie Lorda");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/m²*anno");
					Indici.add(m_index);
					
					index = ((Float) m.get("episuperficielorda_GG")).floatValue();
					m_index = new IndiceEnergetico();				
					m_index.setSimbolo("EPIslGGi");
					m_index.setNome("Indice Prestazione Energetica Invernale Riferita alla Superficie Lorda Normalizzato alla Condizione Climatica (GG)");
					m_index.setValore(index);
					m_index.setUnitamisura("W/m²K");
					Indici.add(m_index);
					
					index = ((Float) m.get("epivolumelordo")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPIvl");
					m_index.setNome("Indice Prestazione Energetica Invernale Riferita al Volume Lordo");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/m²*anno");
					Indici.add(m_index);
					
					index = ((Float) m.get("epivolumelordo_GG")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPIvlGGi");
					m_index.setNome("Indice Prestazione Energetica Invernale Riferita al Volume Lordo Normalizzato alla Condizione Climatica (GG)");
					m_index.setValore(index);
					m_index.setUnitamisura("W/m²K");
					Indici.add(m_index);
					
					index = ((Float) m.get("coefficientescambiotermico_estivo")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Hte");
					m_index.setNome("Coefficiente Estivo di Scambio Termico per Trasmissione");
					m_index.setValore(index);
					m_index.setUnitamisura("W/K");
					Indici.add(m_index);
					
					index = ((Float) m.get("coefficientescambioventilazione_estivo")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Hve");
					m_index.setNome("Coefficiente Estivo Scambio Termico per Ventilazione");
					m_index.setValore(index);
					m_index.setUnitamisura("W/K");
					Indici.add(m_index);
					
					index = ((Float) m.get("apportigratuitiinterni_estivo")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Qie");
					m_index.setNome("Carico Estivo per Apporti Interni");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh");
					Indici.add(m_index);
					
					index = ((Float) m.get("apportisolari_estivo")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Qse");
					m_index.setNome("Apporti Solari Estivi");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh");
					Indici.add(m_index);
					
					index = ((Float) m.get("fabbisognoenergiatermica_estivo")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("Qhe");
					m_index.setNome("Fabbisogno Netto Energia per Raffrescamento Edificio");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh");
					Indici.add(m_index);
					
					index = ((Float) m.get("rendimentoglobale_estivo")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("ETAg - Caldaie");
					m_index.setNome("Rendimento Globale Impianto di Climatizzazione");
					m_index.setValore(index);
					m_index.setUnitamisura("%");
					Indici.add(m_index);
					
					index = ((Float) m.get("epesuperficieutile")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPEsn");
					m_index.setNome("Indice Prestazione Energetica Estiva Riferita alla Superficie Utile");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/m²*anno");
					Indici.add(m_index);
					
					index = ((Float) m.get("epEsuperficieutile_GGe")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPEsnGGe");
					m_index.setNome("Indice Prestazione Energetica Estiva Riferita alla Superficie Utile Normalizzato alla Condizione Climatica (GG)");
					m_index.setValore(index);
					m_index.setUnitamisura("W/m²K");
					Indici.add(m_index);
					
					index = ((Float) m.get("epesuperficielorda")).floatValue();
					m_index = new IndiceEnergetico();				
					m_index.setSimbolo("EPEsl");
					m_index.setNome("Indice Prestazione Energetica Estiva Riferita alla Superficie Lorda");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/m²*anno");
					Indici.add(m_index);
					
					index = ((Float) m.get("epesuperficielorda_GGe")).floatValue();
					m_index = new IndiceEnergetico();				
					m_index.setSimbolo("EPEslGGe");
					m_index.setNome("Indice Prestazione Energetica Estiva Riferita alla Superficie Lorda Normalizzato alla Condizione Climatica (GG)");
					m_index.setValore(index);
					m_index.setUnitamisura("W/m²K");
					Indici.add(m_index);
					
					index = ((Float) m.get("epevolumelordo")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPEvl");
					m_index.setNome("Indice Prestazione Energetica Estiva Riferita al Volume Lordo");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/m²*anno");
					Indici.add(m_index);
					
					index = ((Float) m.get("epevolumelordo_GGe")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPEvlGGe");
					m_index.setNome("Indice Prestazione Energetica Estiva Riferita al Volume Lordo Normalizzato alla Condizione Climatica (GG)");
					m_index.setValore(index);
					m_index.setUnitamisura("W/m²K");
					Indici.add(m_index);
					
					index = ((Float) m.get("consumo_illesterna")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("kWhIllEst");
					m_index.setNome("Consumo Energia per Illuminazione Esterna");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/anno");
					Indici.add(m_index);
					
					index = ((Float) m.get("consumo_illinterna")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("kWhIllInt");
					m_index.setNome("Consumo Energia per Illuminazione Interna");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/anno");
					Indici.add(m_index);
					
					index = ((Float) m.get("epillinterna")).floatValue();
					m_index = new IndiceEnergetico();
					m_index.setSimbolo("EPill");
					m_index.setNome("Indice Prestazione Energetica Illuminazione Interna");
					m_index.setValore(index);
					m_index.setUnitamisura("kWh/anno");
					Indici.add(m_index);
									
					
					break; // estrai solo una riga da questa tabella

			}

		
			} 		
			catch (Exception e) {
				    System.out.println(" IndiciEdificio::Query non eseguita su prestazioni_energetiche" + e);
				    
			}

		return Indici;
		
	}

	@Override
	public void addAggregationFilter(String filter_name, String[] filter_params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}
	
	
}