package it.smee.epi.models;

import it.smee.buildings.models.Edificio;
import it.smee.epi.models.IndiceEnergetico;

import java.util.List;

public class DashboardImmobile extends Edificio{
	
	private List<String> warnings;
	private List<String> errors;
	private String last_update;
	private String qualita_involucro;
	private Float power_usage_now;
	private Float power_consumption_today;
	private Float power_consumption_daily_avg;
	private Float carbon_consumption_today; 
	private List<IndiceEnergetico> indici;
	private String classe_energetica_globale;
	private Float prestazione_energetica_globale;
	private Float prestazione_riscaldamento;
	private Float prestazione_raffrescamento;
	private Float prestazione_acquacalda;
	private String tipoturno;
	
	
	public DashboardImmobile(String pod, String name, String indirizzo, String comune, String provincia, String tipologia, double latitude, double longitude, List<String> msg_warnings, List<String> msg_errors, String last_date)
	{
		super();
				
		this.setPod(pod);
		this.setDenominazione(name);
		this.setIndirizzo(indirizzo);
		this.setNome(comune);
		this.setProvincia(provincia);
		this.setClusterEnergy(tipologia);
		this.setLatitudine(latitude);
		this.setLongitudine(longitude);
		
		this.warnings = msg_warnings;
		this.errors = msg_errors;
		this.last_update = last_date;
		
		this.power_consumption_today = 0.0f;
		this.power_consumption_daily_avg = 0.0f;
		this.power_usage_now = 0.0f;
		this.carbon_consumption_today = 0.0f;
		
	}

	public List<String> getWarnings(){
		return warnings;
	}
	
	public List<String> getErrors() {
		return errors;
	}
	
	public String getLast_update() {
		return last_update;
	}
	
	public Float getPowerConsumptionToday()
	{
		return power_consumption_today;
	}
	
	public Float getPowerUsageNow()
	{
		return power_usage_now;
	}
	
	public Float getPowerConsumptionDailyAvg()
	{
		return power_consumption_daily_avg;
	}
	
	public Float getCarbonConsumptionToday()
	{
		return carbon_consumption_today;
	}
	
	
	public void setPowerConsumptionToday(Float power_consumption_today)
	{
		this.power_consumption_today = power_consumption_today;
	}
	
	public void setPowerUsageNow(Float power_usage_now)
	{
		this.power_usage_now = power_usage_now;
	}
	
	public void setPowerConsumptionDailyAvg(Float power_consumption_daily_avg)
	{
		this.power_consumption_daily_avg = power_consumption_daily_avg;
	}
	
	public void setCarbonConsumptionToday(Float carbon_consumption_today)
	{
		this.carbon_consumption_today = carbon_consumption_today;
	}

	public List<IndiceEnergetico> getIndici() {
		return indici;
	}

	public void setIndici(List<IndiceEnergetico> indici) {
		this.indici = indici;
	}

	public String getClasse_energetica_globale() {
		return classe_energetica_globale;
	}

	public void setClasse_energetica_globale(String classe_energetica_globale) {
		this.classe_energetica_globale = classe_energetica_globale;
	}

	public Float getPrestazione_energetica_globale() {
		return prestazione_energetica_globale;
	}

	public void setPrestazione_energetica_globale(
			Float prestazione_energetica_globale) {
		this.prestazione_energetica_globale = prestazione_energetica_globale;
	}

	public Float getPrestazione_riscaldamento() {
		return prestazione_riscaldamento;
	}

	public void setPrestazione_riscaldamento(Float prestazione_riscaldamento) {
		this.prestazione_riscaldamento = prestazione_riscaldamento;
	}

	public Float getPrestazione_raffrescamento() {
		return prestazione_raffrescamento;
	}

	public void setPrestazione_raffrescamento(Float prestazione_raffrescamento) {
		this.prestazione_raffrescamento = prestazione_raffrescamento;
	}

	public Float getPrestazione_acquacalda() {
		return prestazione_acquacalda;
	}

	public void setPrestazione_acquacalda(Float prestazione_acquacalda) {
		this.prestazione_acquacalda = prestazione_acquacalda;
	}

	public String getQualita_involucro() {
		return qualita_involucro;
	}

	public void setQualita_involucro(String qualita_involucro) {
		this.qualita_involucro = qualita_involucro;
	}


	public String getTipoturno() {
		return tipoturno;
	}

	public void setTipoturno(String tipoturno) {
		this.tipoturno = tipoturno;
	}
}