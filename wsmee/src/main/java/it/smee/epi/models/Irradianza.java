package it.smee.epi.models;

public class Irradianza {
	
	private float nordovest;
	private float nord;
	private float nordest;
	private float ovest;
	private float est;
	private float sudovest;
	private float sud;
	private float sudest;
	
	
	public float getNordovest() {
		return nordovest;
	}
	public void setNordovest(float nordovest) {
		this.nordovest = nordovest;
	}
	public float getNord() {
		return nord;
	}
	public void setNord(float nord) {
		this.nord = nord;
	}
	public float getNordest() {
		return nordest;
	}
	public void setNordest(float nordest) {
		this.nordest = nordest;
	}
	public float getOvest() {
		return ovest;
	}
	public void setOvest(float ovest) {
		this.ovest = ovest;
	}
	public float getEst() {
		return est;
	}
	public void setEst(float est) {
		this.est = est;
	}
	public float getSudovest() {
		return sudovest;
	}
	public void setSudovest(float sudovest) {
		this.sudovest = sudovest;
	}
	public float getSud() {
		return sud;
	}
	public void setSud(float sud) {
		this.sud = sud;
	}
	public float getSudest() {
		return sudest;
	}
	public void setSudest(float sudest) {
		this.sudest = sudest;
	}
	
	
	
	
	
	
}
