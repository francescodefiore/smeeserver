CREEMapp.directive('alertList', function() {

	  return {		
		templateUrl: 'directives/alert-list.html',
		scope: {alerttype: '@'},
				
		controller: ['$scope', 'AlertFactory', 'CreemSettings', function($scope, AlertFactory, CreemSettings) {

			var getdata = function (par, type) 
			{
				var alertData = [];
				var length_alert = "";
				var num_alert = 0;
				var buiding_selected = CreemSettings.selectedbuildings[0];
				if(type == "IMMOBILE") {					
					var m_data = AlertFactory.getBuildingAlerts(par).query(
					function(){		
						for (var i = 0; i < m_data.length; i++) {
							var sup = parseFloat(((parseFloat(m_data[i].KPI).toFixed(2) - parseFloat(m_data[i].Bench).toFixed(2))/parseFloat(m_data[i].Bench).toFixed(2))*100).toFixed(2);
							var tooltip = "Codice: " + buiding_selected.codice + "\nCluster: " + buiding_selected.clusterEnergy + "\r" + buiding_selected.indirizzo + "\r" + buiding_selected.nome + " (" + buiding_selected.siglaprov + ")";
							var year = (m_data[i].periodoRiferimento).split("-");
							if($scope.alerttype == "budget" && (m_data[i].tipoAllert == 3 || m_data[i].tipoAllert == 4) && (year[0]=='2014'))
								alertData.push({"tipoAllert":tooltip, "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "epi" && m_data[i].tipoAllert == 2)
								alertData.push({"KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "kpi" && m_data[i].tipoAllert == 1)
								alertData.push({"KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "baseload" && m_data[i].tipoAllert == 5)
								alertData.push({"KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
						}
						$scope.alerts = alertData;
						$scope.length_alert = alertData.length;
						if ($scope.length_alert > 100) {
							$scope.length_alert = "+100";
						}
					});
				}
				else if(type == "PROV") {					
					var m_data = AlertFactory.getForProvinciaAlert(par).query(
					function(){
						for (var i = 0; i < m_data.length; i++) {
							var tooltip = "Codice: " + buiding_selected.codice + "\nCluster: " + buiding_selected.clusterEnergy + "\r" + buiding_selected.indirizzo + "\r" + buiding_selected.nome + " (" + buiding_selected.siglaprov + ")";
							var sup = parseFloat(((parseFloat(m_data[i].KPI).toFixed(2) - parseFloat(m_data[i].Bench).toFixed(2))/parseFloat(m_data[i].Bench).toFixed(2))*100).toFixed(2);
							var year = (m_data[i].periodoRiferimento).split("-");
							if($scope.alerttype == "budget" && (m_data[i].tipoAllert == 3 || m_data[i].tipoAllert == 4) && (year[0]=='2014'))
								alertData.push({"tipoAllert":tooltip,  "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "epi" && m_data[i].tipoAllert == 2)
								alertData.push({"nome":info, "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "kpi" && m_data[i].tipoAllert == 1)
								alertData.push({"nome":info, "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "baseload" && m_data[i].tipoAllert == 5)
								alertData.push({"nome":info, "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
						}
						$scope.alerts = alertData;
						$scope.length_alert = alertData.length;
						if ($scope.length_alert > 100) {
							$scope.length_alert = "+100";
						}
					});
				}
				else if(type == "CLUSTER") {					
					var m_data = AlertFactory.getForClusterAlert(par).query(
					function(){
						for (var i = 0; i < m_data.length; i++) {
							var tooltip = "Codice: " + buiding_selected.codice + "\nCluster: " + buiding_selected.clusterEnergy + "\r" + buiding_selected.indirizzo + "\r" + buiding_selected.nome + " (" + buiding_selected.siglaprov + ")";
							var sup = parseFloat(((parseFloat(m_data[i].KPI).toFixed(2) - parseFloat(m_data[i].Bench).toFixed(2))/parseFloat(m_data[i].Bench).toFixed(2))*100).toFixed(2);
							var year = (m_data[i].periodoRiferimento).split("-");
							if($scope.alerttype == "budget" && (m_data[i].tipoAllert == 3 || m_data[i].tipoAllert == 4) && (year[0]=='2014'))
								alertData.push({"tipoAllert":tooltip, "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "epi" && m_data[i].tipoAllert == 2)	
								alertData.push({"nome":info, "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "kpi" && m_data[i].tipoAllert == 1)
								alertData.push({"nome":info, "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});
							if($scope.alerttype == "baseload" && m_data[i].tipoAllert == 5)
								alertData.push({"nome":info, "KPI": m_data[i].KPI, "descrizione":m_data[i].allert, "Bench":m_data[i].Bench, "unitaMisura":m_data[i].unitaMisura, "periodoRiferimento":m_data[i].periodoRiferimento, "super":sup});

						}
						$scope.alerts = alertData;
						$scope.length_alert = alertData.length;
						if ($scope.length_alert > 100) {
							$scope.length_alert = "+100";
						}
					});					
				}
				
				
				
			}
			
			
			var refreshData = function ()
			{										
					$scope.alerts = [];
				
					if(CreemSettings.selectedbuildings.length > 0)
						getdata(CreemSettings.selectedbuildings[0].idImmobile, "IMMOBILE");
					else if(CreemSettings.selectedCities.length > 0)
						getdata(CreemSettings.selectedCities[0], "PROV");
					else if(CreemSettings.selectedClusters.length > 0)
						getdata(CreemSettings.selectedClusters[0], "CLUSTER");
					
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();

		}]

	  }
	});
	


	
	