package it.smee.epi.models;

public class DatiGenerali {
	

	private float ricambioariaInverno_bench;
	private float ricambioariaEstate_bench;	
	private float coefficienteFx;
	private float fattoreFm;
	private float costoEnergiaElettrica;
	private float costoGasolio;
	private float costoMetano;
	private float rendimentoNazionale;

	public float getCoefficienteFx() {
		return coefficienteFx;
	}
	public void setCoefficienteFx(float coefficienteFx) {
		this.coefficienteFx = coefficienteFx;
	}
	public float getFattoreFm() {
		return fattoreFm;
	}
	public void setFattoreFm(float fattoreFm) {
		this.fattoreFm = fattoreFm;
	}
	public float getCostoEnergiaElettrica() {
		return costoEnergiaElettrica;
	}
	public void setCostoEnergiaElettrica(float costoEnergiaElettrica) {
		this.costoEnergiaElettrica = costoEnergiaElettrica;
	}
	public float getCostoGasolio() {
		return costoGasolio;
	}
	public void setCostoGasolio(float costoGasolio) {
		this.costoGasolio = costoGasolio;
	}
	public float getCostoMetano() {
		return costoMetano;
	}
	public void setCostoMetano(float costoMetano) {
		this.costoMetano = costoMetano;
	}
	public float getRendimentoNazionale() {
		return rendimentoNazionale;
	}
	public void setRendimentoNazionale(float rendimentoNazionale) {
		this.rendimentoNazionale = rendimentoNazionale;
	}
	public float getRicambioariaInverno_bench() {
		return ricambioariaInverno_bench;
	}
	public void setRicambioariaInverno_bench(float ricambioariaInverno_bench) {
		this.ricambioariaInverno_bench = ricambioariaInverno_bench;
	}
	public float getRicambioariaEstate_bench() {
		return ricambioariaEstate_bench;
	}
	public void setRicambioariaEstate_bench(float ricambioariaEstate_bench) {
		this.ricambioariaEstate_bench = ricambioariaEstate_bench;
	}
	
}
