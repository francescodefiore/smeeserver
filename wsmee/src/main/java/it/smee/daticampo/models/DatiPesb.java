package it.smee.daticampo.models;

import java.sql.Date;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
public class DatiPesb {
	
	private double valore;
	//private Date data;
	private String data; 
	
	public double getValore() {
		return valore;
	}
	public void setValore(double valore) {
		this.valore = valore;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

}
