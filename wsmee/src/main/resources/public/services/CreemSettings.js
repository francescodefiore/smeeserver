CREEMapp.factory("CreemSettings", ['$rootScope', function ($rootScope) {
    
	var settingsObject;

	
    settingsObject = {
	
        selectedbuildings: [],

        selectedClusters: [],
		
		selectedCities: [],

		selectedWeather: '',
		
		selectedMeasures: '',
		
		selectedSuperficieDss: '',
		
        selectedDates: { from: moment().subtract(7, 'days').subtract(1,'years').format("YYYY-MM-DD"), to: moment().subtract(1,'years').format("YYYY-MM-DD") },
		
		selectedDatesCampo: { from: moment().subtract(7, 'days').format("YYYY-MM-DD"), to: moment().format("YYYY-MM-DD") },
		
		reset: function()
		{
			this.selectedbuildings = [];
            this.selectedClusters = [];         
			this.selectedCities = [];
			this.selectedMeasures= '';
			this.selectedSuperficieDss = '';
			this.selectedDates.from = moment().subtract(7, 'days').subtract(1,'years').format("YYYY-MM-DD");
			this.selectedDates.to = moment().subtract(1,'years').format("YYYY-MM-DD");
			//this.selectedDatesCampo.from = moment().subtract(7, 'days').format("YYYY-MM-DD");
			//this.selectedDatesCampo.to = moment().format("YYYY-MM-DD");
		},
		
        setSettings: function (conf) 
		{
			this.reset();
            this.selectedbuildings = conf.buildings;
            this.selectedClusters = conf.clusters;         
            this.selectedDates = conf.dates;
			this.selectedDatesCampo = conf.datescampo;
			this.selectedWeather = conf.weather;
			this.selectedMeasures= conf.measures;
			this.selectedSuperficieDss = conf.selectedSuperficieDss;
			this.notifyObservers();
			
        },
		
		setBuilding: function(building)
		{
			this.reset();
			this.selectedbuildings.push(building);			
			this.notifyObservers();
		},
		
		setCity: function(city)
		{
			this.reset();
			this.selectedCities[0] = city;			
			this.notifyObservers();
		},
		
		setCluster: function(cluster)
		{
			this.reset();		
			this.selectedClusters[0] = cluster;			
			this.notifyObservers();
		},
		
		setMeasure: function(measure)
		{
			this.reset();		
			this.selectedMeasures = measure;			
			this.notifyObservers();
		},
		
		setSuperficieDss: function(supdss)
		{
			this.reset();		
			this.selectedSuperficieDss = supdss;			
			this.notifyObservers();
		},
		
		observerCallbacks: [],
			
		notifyObservers: function(){
			
			$rootScope.$broadcast('settingsUpdated', {});
				
		}

    };
	
	//settingsObject.reset();
	
    return settingsObject;
}]);