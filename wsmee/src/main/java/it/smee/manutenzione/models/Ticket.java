package it.smee.manutenzione.models;

public class Ticket {
	private String idIntervento;
	private String cdIntervento;
	private String idImmobile;
	private String idImpianto;
	private String codSocieta;
	private String denominazioneUp;
	private String codiceImmobile;
	private String idCategoria;
	private String descrizione;
	private String fornitore;
	private String buildingManager;
	private String utenteInserimento;
	private String dataRichiesta;
	private String dataInizioIntervento;
	private String dataLimite;
	private String dataFineIntervento;
	private String durata;
	private String priorita;
	private String contatoreScarti;
	private String eseguito;
	private String dataCardine;
	private String numEsecutori;
	private String pianificato;
	private String altezza;
	private String carico;
	private String sistemaEmittente;
	private String kgCO2Risparmiati;
	private String idSedeCentrale;
	
	
	public String getIdIntervento() {
		return idIntervento;
	}
	public void setIdIntervento(String idIntervento) {
		this.idIntervento = idIntervento;
	}
	public String getCdIntervento() {
		return cdIntervento;
	}
	public void setCdIntervento(String cdIntervento) {
		this.cdIntervento = cdIntervento;
	}
	public String getIdImpianto() {
		return idImpianto;
	}
	public void setIdImpianto(String idImpianto) {
		this.idImpianto = idImpianto;
	}
	public String getIdImmobile() {
		return idImmobile;
	}
	public void setIdImmobile(String idImmobile) {
		this.idImmobile = idImmobile;
	}
	public String getCodSocieta() {
		return codSocieta;
	}
	public void setCodSocieta(String codSocieta) {
		this.codSocieta = codSocieta;
	}
	public String getDenominazioneUp() {
		return denominazioneUp;
	}
	public void setDenominazioneUp(String denominazioneUp) {
		this.denominazioneUp = denominazioneUp;
	}
	public String getCodiceImmobile() {
		return codiceImmobile;
	}
	public void setCodiceImmobile(String codiceImmobile) {
		this.codiceImmobile = codiceImmobile;
	}
	public String getIdCategoria() {
		return idCategoria;
	}
	public void setIdCategoria(String idCategoria) {
		this.idCategoria = idCategoria;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getFornitore() {
		return fornitore;
	}
	public void setFornitore(String fornitore) {
		this.fornitore = fornitore;
	}
	public String getBuildingManager() {
		return buildingManager;
	}
	public void setBuildingManager(String buildingManager) {
		this.buildingManager = buildingManager;
	}
	public String getUtenteInserimento() {
		return utenteInserimento;
	}
	public void setUtenteInserimento(String utenteInserimento) {
		this.utenteInserimento = utenteInserimento;
	}
	public String getDataRichiesta() {
		return dataRichiesta;
	}
	public void setDataRichiesta(String dataRichiesta) {
		this.dataRichiesta = dataRichiesta;
	}
	public String getDataInizioIntervento() {
		return dataInizioIntervento;
	}
	public void setDataInizioIntervento(String dataInizioIntervento) {
		this.dataInizioIntervento = dataInizioIntervento;
	}
	public String getDataLimite() {
		return dataLimite;
	}
	public void setDataLimite(String dataLimite) {
		this.dataLimite = dataLimite;
	}
	public String getDataFineIntervento() {
		return dataFineIntervento;
	}
	public void setDataFineIntervento(String dataFineIntervento) {
		this.dataFineIntervento = dataFineIntervento;
	}
	public String getDurata() {
		return durata;
	}
	public void setDurata(String durata) {
		this.durata = durata;
	}
	public String getPriorita() {
		return priorita;
	}
	public void setPriorita(String priorita) {
		this.priorita = priorita;
	}
	public String getContatoreScarti() {
		return contatoreScarti;
	}
	public void setContatoreScarti(String contatoreScarti) {
		this.contatoreScarti = contatoreScarti;
	}
	public String getEseguito() {
		return eseguito;
	}
	public void setEseguito(String eseguito) {
		this.eseguito = eseguito;
	}
	public String getDataCardine() {
		return dataCardine;
	}
	public void setDataCardine(String dataCardine) {
		this.dataCardine = dataCardine;
	}
	public String getPianificato() {
		return pianificato;
	}
	public void setPianificato(String pianificato) {
		this.pianificato = pianificato;
	}
	public String getNumEsecutori() {
		return numEsecutori;
	}
	public void setNumEsecutori(String numEsecutori) {
		this.numEsecutori = numEsecutori;
	}
	public String getAltezza() {
		return altezza;
	}
	public void setAltezza(String altezza) {
		this.altezza = altezza;
	}
	public String getCarico() {
		return carico;
	}
	public void setCarico(String carico) {
		this.carico = carico;
	}
	public String getSistemaEmittente() {
		return sistemaEmittente;
	}
	public void setSistemaEmittente(String sistemaEmittente) {
		this.sistemaEmittente = sistemaEmittente;
	}
	public String getKgCO2Risparmiati() {
		return kgCO2Risparmiati;
	}
	public void setKgCO2Risparmiati(String kgCO2Risparmiati) {
		this.kgCO2Risparmiati = kgCO2Risparmiati;
	}
	public String getIdSedeCentrale() {
		return idSedeCentrale;
	}
	public void setIdSedeCentrale(String idSedeCentrale) {
		this.idSedeCentrale = idSedeCentrale;
	}


}
