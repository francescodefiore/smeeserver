CREEMapp.directive('buildingPlants', 

function() 
{
	
	return {
	
    controller: ['$scope', 'CreemSettings','PlantsFactory', 		
		
		function($scope, CreemSettings, PlantsFactory)
		{			
			$scope.components = {};
			$scope.impianti_selezionati = [];
			$scope.currentTab = 'directives/impianti/riscaldamento.html';

			$scope.onClickTab = function (tab) {
				$scope.currentTab = tab;
			};

			$scope.isActiveTab = function(tabUrl) {
				return tabUrl == $scope.currentTab;
			};

			$scope.moduloepi = {};
			$scope.datigenerali = {};
			
			$scope.models = {
				  changeInfo: [],
				  searchText: '',
				  selectedPlants: [],
				  
				  impiantiRaffrescamento: [],
				  impiantiRiscaldamento: [],
				  impiantiIlluminazione: [],
				  
				  state: {
					sortKey: 'ubicazione',
					sortDirection: 'DEC'
				  }
				};

				$scope.impiantiTableColumnDefinition = [
				  {
					columnHeaderDisplayName: 'Ubicazione',
					displayProperty: 'ubicazione',
					sortKey: 'ubicazione',
					width: '50%',
					columnSearchProperty: 'ubicazione',
					visible: true
				  },
				  {
					columnHeaderTemplate: '<span>Data installazione</span>',
					template: '<strong>{{ item.dataInstallazione }}</strong>',
					sortKey: 'dataInstallazione',
					width: '25%',
					columnSearchProperty: 'dataInstallazione'
				  },
				  {
					columnHeaderTemplate: '<span> Tipologia</span>',
					displayProperty: 'tipoImp',
					sortKey: 'tipoImp',
					width: '25%',
					columnSearchProperty: 'tipoImp'
				  }
				];

				

				$scope.rowExpanded = function (impianto) {
					$scope.impianti_selezionati.push(impianto);										
				};

				$scope.checkRowSelected = function (item, index) {
				  var found = false;
				  
				  $scope.impianti_selezionati.forEach(function (selectedItem) {
					if (item.idImpianti === selectedItem.idImpianti) {
					  found = true;
					}
				  });
				  
				  return found ? 'info row-' + index : 'row-' + index;
				};

			$scope.onSubmit = function() {
					

			}
			
			

			var refreshData = function ()
			{						
					$scope.models.impiantiIlluminazione = [];										
					$scope.models.impiantiRiscaldamento = [];
					$scope.models.impiantiRaffrescamento = [];		
										

					var plants = PlantsFactory.getPlants(CreemSettings.selectedbuildings[0].idImmobile).query( 
							function(){ 

									for(var i=0; i<plants.length; i++)
									{									
										if( plants[i].tipoImp == "Illuminazione Interna" || plants[i].tipoImp == "Illuminazione Esterna")
											$scope.models.impiantiIlluminazione.push(plants[i]);											
										if( plants[i].tipoImp == "Riscaldamento")									
											$scope.models.impiantiRiscaldamento.push(plants[i]);
										if( plants[i].tipoImp == "Raffrescamento")
											$scope.models.impiantiRaffrescamento.push(plants[i]);
										
										$scope.components[plants[i].idImpianti]=plants[i].componenti;
									}									
									
			});
					
			}
			
			$scope.$on('settingsUpdated', function (event, data) { refreshData(); } );
			
			if(CreemSettings.selectedbuildings.length > 0 || CreemSettings.selectedCities.length > 0 || CreemSettings.selectedClusters.length > 0)
				refreshData();
			
		}],

		templateUrl: 'directives/impianti/impianti.html'
	}
  });



	
	