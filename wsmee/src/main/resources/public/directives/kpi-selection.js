CREEMapp.directive('kpiSelection', function() {

	  return {		
		
		templateUrl: 'directives/kpi-selection.html',
				
		controller: ['$scope', 'KPIFactory', 'CreemSettings', function($scope, KPIFactory, CreemSettings) {
							
				$scope.selected_kpi;																		
				$scope.kpi = KPIFactory.getTipoKPI(CreemSettings.selectedbuildings[0].idImmobile).query(
					function(){
						if(CreemSettings.selectedKpi.length == 0)													
							CreemSettings.setKpi($scope.kpi[0]);
						$scope.selected_kpi = CreemSettings.selectedKpi[0];	
					}
				);

				$scope.setKpi = function() 
				{													
					CreemSettings.setKpi($scope.selected_kpi.idKpi);
				}
									
			}
		],
		
		link: function(scope, element)
	  { 		
			
			$(element).find('#kpi_list').val(scope.selected_kpi);										
			scope.$watch('kpi', function(newValue, oldValue) {																	
					if (newValue.length != 0)																				
					{
						$(element).find('#kpi_list').chosen();
					}
					
			}, true);	
						
		}
				
		
		
	  }
	});

	