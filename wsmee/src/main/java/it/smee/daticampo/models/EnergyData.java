package it.smee.daticampo.models;

public class EnergyData
{
	private String timestamp;
	private String ipFisico;
	private float valore;
	
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getIpFisico() {
		return ipFisico;
	}
	public void setIpFisico(String ipFisico) {
		this.ipFisico = ipFisico;
	}
	public float getValore() {
		return valore;
	}
	public void setValore(float valore) {
		this.valore = valore;
	}
}